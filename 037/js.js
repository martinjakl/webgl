// Perspective correct texture mapping
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in float a_brightness;
out float v_brightness;

void main() {
	gl_Position = a_position;
	v_brightness = a_brightness;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in float v_brightness;
out vec4 outColor;
void main() {
	outColor = vec4(fract(v_brightness * 10.), 0, 0, 1);
}
`;

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const mult = 20;
	const positions = [
		-.8, -.8, 0, 1,
		.8 * mult, -.8 * mult, 0, mult,
		-.8, -.2, 0, 1,
		-.8, -.2, 0, 1,
		.8 * mult, -.8 * mult, 0, mult,
		.8 * mult, -.2 * mult, 0, mult,

		-.8, .2, 0, 1,
		.8, .2, 0, 1,
		-.8, .8, 0, 1,
		-.8, .8, 0, 1,
		.8, .2, 0, 1,
		.8, .8, 0, 1,
	];
	const brightness = [0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1];
	const vao = gl.createVAO(program, {position: "a_position", brightness: "a_brightness"}, {position: new pcInfo(new Float32Array(positions), 12, 4, false, 0, 0), brightness: new pcInfo(new Float32Array(brightness), 12, 1, false, 0, 0)});

	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		gl.drawVAO(vao, {});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
