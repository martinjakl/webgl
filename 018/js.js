// 3D textures
"use strict";
const fVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_matrix;
out vec2 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = a_texcoord;
}
`;

const fFragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	const textCtx = document.createElement("canvas").getContext("2d");
	if (gl.gl && textCtx) {
		const fProgram = gl.createProgramShaders(fVertexShaderSource, fFragmentShaderSource);
		const fvao = gl.createVAO(fProgram, {position: "a_position", texcoord2: "a_texcoord"}, Primitive.createF());
		const fTex = gl.loadTexture("../img/f-texture.png");

		const fieldOfViewRadians = m3.deg2rad(60);
		let modelXRotationRadians = 0;
		let modelYRotationRadians = 0;
		let then = 0;

		requestAnimationFrame(drawScene);

		function drawScene(now) {
			now *= 0.001;
			const deltaTime = now - then;
			then = now;

			glUtils.resizeCanvas(gl.canvas);

			modelXRotationRadians += 1.2 * deltaTime;
			modelYRotationRadians += 0.7 * deltaTime;

			gl.initFrame();

			const aspect = gl.clientWidth / gl.clientHeight;
			const zNear = 1;
			const zFar = 2000;
			const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

			const cameraPosition = [0, 0, 200];
			const target = [0, 0, 0];
			const up = [0, 1, 0];
			const cameraMatrix = m4.lookAt(cameraPosition, target, up);
			const viewMatrix = m4.inverse(cameraMatrix);
			const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

			const matrix = m4.xRotate(viewProjectionMatrix, modelXRotationRadians);
			m4.yRotate(matrix, modelYRotationRadians, matrix);
			gl.drawVAO(fvao, {u_matrix: matrix, u_texture: fTex});

			requestAnimationFrame(drawScene);
		}
	} else {
		console.log("No GL");
	}
}

document.addEventListener('DOMContentLoaded', start);
