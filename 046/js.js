// Tetris
/*
 * Copyright 2018 Martin Jakl (martin.jakl@macsnet.cz).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
"use strict";
const specularVertexShader = `#version 300 es
in vec4 a_position;
in vec3 a_normal;
in vec2 a_texcoord;
in vec2 a_mappos;
uniform vec3 u_lightWorldPosition;
uniform vec3 u_viewWorldPosition;
uniform mat4 u_world;
uniform mat4 u_worldViewProjection;
uniform mat4 u_worldInverseTranspose;
uniform sampler2D u_cubemap;
uniform float u_cubeSize;
out vec3 v_normal;
out vec3 v_surfaceToLight;
out vec3 v_surfaceToView;
out vec2 v_texcoord;
out vec4 v_color;

vec4 getColor(int c) {
	vec4 result = vec4(0, 0, 0, 0);
	switch (c) {
		case 1:
			result = vec4(0, 1, 1, 1);
			break;
		case 2:
			result = vec4(0, 0, 1, 1);
			break;
		case 3:
			result = vec4(1, 0.5, 0, 1);
			break;
		case 4:
			result = vec4(1, 1, 0, 1);
			break;
		case 5:
			result = vec4(0, 1, 0, 1);
			break;
		case 6:
			result = vec4(1, 0, 1, 1);
			break;
		case 7:
			result = vec4(1, 0, 0, 1);
			break;
	}
	return result;
}

void main() {
	vec4 pos = a_position + vec4(a_mappos, 0, 0) * u_cubeSize;
	gl_Position = u_worldViewProjection * pos;
	v_normal = mat3(u_worldInverseTranspose) * a_normal;
	vec3 surfaceWorldPosition = (u_world * pos).xyz;
	v_surfaceToLight = u_lightWorldPosition - surfaceWorldPosition;
	v_surfaceToView = u_viewWorldPosition - surfaceWorldPosition;
	v_texcoord = a_texcoord;
	v_color = getColor(int((texelFetch(u_cubemap, ivec2(a_mappos - vec2(1, 1)), 0).r) * 8.0));
}
`;

const specularShader = `#version 300 es
#if GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
in vec3 v_normal;
in vec3 v_surfaceToLight;
in vec3 v_surfaceToView;
in vec2 v_texcoord;
in vec4 v_color;
uniform float u_shininess;
uniform sampler2D u_texture;
out vec4 outColor;

void main() {
	if (v_color == vec4(0, 0, 0, 0)) {
		discard;
	}
	vec3 normal = normalize(v_normal);
	vec3 surfaceToLightDirection = normalize(v_surfaceToLight);
	vec3 surfaceToViewDirection = normalize(v_surfaceToView);
	vec3 halfVector = normalize(surfaceToLightDirection + surfaceToViewDirection);

	float light = dot(normal, surfaceToLightDirection);
	float specular = 0.0;
	if (light > 0.0) {
		specular = pow(dot(normal, halfVector), u_shininess) / 1.2;
	}
	outColor = v_color * texture(u_texture, v_texcoord);
	outColor.rgb *= light;
	outColor.rgb += specular;
}
`;

const plateVertexShader = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
in vec3 a_normal;
in vec3 a_tangent;
uniform mat4 u_worldViewProjection;
uniform vec3 u_lightWorldPosition;
uniform vec3 u_viewWorldPosition;
uniform mat4 u_world;
uniform mat4 u_worldInverseTranspose;
uniform sampler2D u_dispmap;
out vec2 v_texcoord;
out vec3 v_surfaceToLight;
out vec3 v_surfaceToView;
out vec3 v_normal;
out vec3 v_tangent;

void main() {
	vec2 tex = a_texcoord * vec2(0.5, 1);
	float disp = texture(u_dispmap, tex).r;
	vec4 pos = a_position + disp * vec4(a_normal, 0);
	gl_Position = u_worldViewProjection * pos;
	v_texcoord = tex;
	v_normal = mat3(u_worldInverseTranspose) * a_normal;
	v_tangent = mat3(u_worldInverseTranspose) * a_tangent;
	vec3 surfaceWorldPosition = (u_world * pos).xyz;
	v_surfaceToLight = u_lightWorldPosition - surfaceWorldPosition;
	v_surfaceToView = u_viewWorldPosition - surfaceWorldPosition;
}
`;

const plateFragmentShader = `#version 300 es
#if GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
in vec2 v_texcoord;
in vec3 v_surfaceToLight;
in vec3 v_surfaceToView;
in vec3 v_normal;
in vec3 v_tangent;
uniform sampler2D u_coltexture;
uniform sampler2D u_normtexture;
uniform sampler2D u_shinemap;
uniform sampler2D u_reflmap;
out vec4 outColor;

vec3 bumpedNormal() {
	vec3 normal = normalize(v_normal);
	vec3 tangent = normalize(v_tangent);
	tangent = normalize(tangent - dot(tangent, normal) * normal);
	vec3 bitangent = cross(tangent, normal);
	vec3 bumpNormal = texture(u_normtexture, v_texcoord).xyz;
	bumpNormal = 2.0 * bumpNormal - vec3(1, 1, 1);
	mat3 TBN = mat3(tangent, bitangent, normal);
	vec3 newNormal = normalize(TBN * bumpNormal);
	return newNormal;
}

void main() {
	vec3 normal = bumpedNormal();
	vec3 surfaceToLightDirection = normalize(v_surfaceToLight);
	vec3 surfaceToViewDirection = normalize(v_surfaceToView);
	vec3 halfVector = normalize(surfaceToLightDirection + surfaceToViewDirection);

	/*vec3 amb = texture(u_reflmap, v_texcoord).r * vec3(0.24725, 0.1995, 0.0745);

	float diffuseTerm = clamp(dot(normal, surfaceToLightDirection), 0.0, 1.0);
	vec3 dif = texture(u_shinemap, v_texcoord).r * vec3(0.75164, 0.60648, 0.22648) * diffuseTerm;

	float specularTerm = 0.0;
	if (dot(normal, surfaceToLightDirection) > 0.0) {
		specularTerm = pow(dot(normal, halfVector), 0.4 * 128.0);
	}
	vec3 spec = texture(u_shinemap, v_texcoord).r * vec3(0.628281, 0.555802, 0.366065) * specularTerm;

	outColor = vec4(texture(u_coltexture, v_texcoord).rgb * (amb + dif + spec), 1);*/
	float light = dot(normal, surfaceToLightDirection);
	float specular = 0.0;
	if (light > 0.0) {
		specular = pow(dot(normal, halfVector), 64.0) / 1.2;
	}
	outColor = texture(u_coltexture, v_texcoord);
	outColor.rgb *= light;
	outColor.rgb += specular;
}
`;

const shapeDef = {
	I: {size: 4,
		shape: [0, 0, 0, 0,
			1, 1, 1, 1,
			0, 0, 0, 0,
			0, 0, 0, 0],
	},
	J: {size: 3,
		shape: [2, 0, 0,
			2, 2, 2,
			0, 0, 0]
	},
	L: {size: 3,
		shape: [0, 0, 3,
			3, 3, 3,
			0, 0, 0]
	},
	O: {size: 2,
		shape: [4, 4,
			4, 4]
	},
	S: {size: 3,
		shape: [0, 5, 5,
			5, 5, 0,
			0, 0, 0]
	},
	T: {size: 3,
		shape: [0, 6, 0,
			6, 6, 6,
			0, 0, 0]
	},
	Z: {size: 3,
		shape: [7, 7, 0,
			0, 7, 7,
			0, 0, 0]
	}
};

const wellSize = [10, 20];
let shapes = [];

function draw(fpsNode, levelNode, scoreNode, gl, canvas) {
	const program = gl.createProgramShaders(specularVertexShader, specularShader);
	const plateProgram = gl.createProgramShaders(plateVertexShader, plateFragmentShader);
	const well = new Uint8Array(wellSize[0] * wellSize[1] * 2);
	const wellinner = new Uint8Array(wellSize[0] * wellSize[1] * 2);
	for (let i = 0; i < wellSize[1]; ++i) {
		for (let j = 0; j < wellSize[0]; ++j) {
			well[(i * wellSize[0] + j) * 2] = j + 1;
			if (i < wellSize[1] / 2) {
				well[(i * wellSize[0] + j) * 2 + 1] = wellSize[1] - i;
			} else {
				well[(i * wellSize[0] + j) * 2 + 1] = i - (wellSize[1] / 2 - 1);
			}
			wellinner[(i * wellSize[0] + j) * 2] = j + 1;
			wellinner[(i * wellSize[0] + j) * 2 + 1] = i + 1;
		}
	}
	const wellPC = new pcInfo(well, well.length / 2, 2, false, 0, 0);
	const wellinnerPC = new pcInfo(wellinner, wellinner.length / 2, 2, false, 0, 0);

	let colorMap = new Uint8Array(wellSize[0] * wellSize[1]);
	let colorMapTexture;

	function getNextShape() {
		if (shapes.length == 0) {
			shapes = ['I', 'J', 'L', 'O', 'S', 'T', 'Z'];
		}
		const ind = Math.floor(Math.random() * shapes.length);
		const res = shapes[ind];
		shapes.splice(ind, 1);
		return res;
	}

	function blendShape(shape, size, pos, toMap) {
		let cm;
		if (shape) {
			if (toMap) {
				cm = colorMap;
			} else {
				cm = new Uint8Array(colorMap);
			}
			for (let i = 0; i < size; ++i) {
				if (pos[1] - i >= 0 && pos[1] - i < wellSize[1]) {
					for (let j = 0; j < size; ++j) {
						if (j + pos[0] < wellSize[0] && shape[i * size + j]) {
							cm[(pos[1] - i) * wellSize[0] + j + pos[0]] = shape[i * size + j] * 32;
						}
					}
				}
			}
		} else {
			cm = colorMap;
		}
		return cm;
	}

	function rotShape(shape, size) {
		const res = [];
		for (let i = 0; i < size; ++i) {
			for (let j = 0; j < size; ++j) {
				res[i * size + j] = shape[(size - j - 1) * size + i];
			}
		}
		return res;
	}

	function colorMap2Texture(cm) {
		if (colorMapTexture) {
			gl.deleteTexture(colorMapTexture);
		}
		colorMapTexture = gl.createTextureFromData(wellSize[0], wellSize[1], gl.PixelFormat.RED, cm, gl.PixelFormat.R8);
	}

	function validPos(pos, shape, size) {
		let res = true;
		for (let i = 0; i < size; ++i) {
			for (let j = 0; j < size; ++j) {
				if (shape[i * size + j]) {
					if (j + pos[0] < 0 || j + pos[0] >= wellSize[0] || colorMap[(pos[1] - i) * wellSize[0] + j + pos[0]] || pos[1] - i < 0) {
						res = false;
						break;
					}
				}
			}
			if (!res) {
				break;
			}
		}
		return res;
	}

	function removeFullLines() {
		let removedLines = 0;
		for (let i = 0; i < wellSize[1];) {
			let notfull = false;
			for (let j = 0; j < wellSize[0]; ++j) {
				if (!colorMap[i * wellSize[0] + j]) {
					notfull = true;
					break;
				}
			}
			if (notfull) {
				++i;
			} else {
				++removedLines;
				if (i < wellSize[1] - 1) {
					colorMap.set(colorMap.slice((i + 1) * wellSize[0]), i * wellSize[0]);
				}
				for (let j = 0; j < wellSize[0]; ++j) {
					colorMap[(wellSize[1] - 1) * wellSize[0] + j] = 0;
				}
			}
		}
		return removedLines;
	}

	function getSpeed(level) {
		return Math.pow(0.8 - ((level - 1) * 0.007), level - 1);
	}

	colorMap2Texture(colorMap);

	const metalColorTex = gl.loadTexture("../img/[2K]MetalPlates03/MetalPlates03_col.jpg", gl.TextureWrap.REPEAT, gl.TextureWrap.REPEAT);
	const metalNormalTex = gl.loadTexture("../img/[2K]MetalPlates03/MetalPlates03_nrm.jpg", gl.TextureWrap.REPEAT, gl.TextureWrap.REPEAT);
	const metalMetTex = null;//gl.loadTexture("../img/[2K]MetalPlates03/MetalPlates03_met.jpg", gl.TextureWrap.REPEAT, gl.TextureWrap.REPEAT);
	const reflMetTex = null;//gl.loadTexture("../img/[2K]MetalPlates03/MetalPlates03_rgh.jpg", gl.TextureWrap.REPEAT, gl.TextureWrap.REPEAT);
	const dispMetTex = gl.loadTexture("../img/[2K]MetalPlates03/MetalPlates03_disp.jpg", gl.TextureWrap.REPEAT, gl.TextureWrap.REPEAT);

	let vao;
	let vaoinner;
	let cubeSize;
	let plateVao;
	const plateSize = 100;
	let resPlate;
	Primitive.loadJSON('../mesh/cube.json', function(prim) {
		cubeSize = prim.extents.max[0] - prim.extents.min[0];
		resPlate = 1 / (plateSize / ((wellSize[0] + 0.5) * cubeSize));
		plateVao = gl.createVAO(plateProgram, {position: 'a_position', texcoord: 'a_texcoord', normal: 'a_normal', tangent: 'a_tangent'}, Primitive.addTangent(Primitive.createRectangle(plateSize, plateSize * 2), 'texcoord', 'position', 'indices'), 'indices');
		const priminner = {};
		priminner.extents = prim.extents;
		priminner.position = prim.position;
		priminner.texcoord = prim.texcoord;
		const normalsinner = [];
		for (let i = 0; i < prim.normal.data.length / 3; ++i) {
			let v = prim.normal.data.slice(i * 3, i * 3 + 3);
			v = m4.multiplyVector(v, -1);
			normalsinner.push(...v);
		}
		priminner.normal = new pcInfo(new Float32Array(normalsinner), normalsinner.length / 3, 3, false, 0, 0);
		const indinner = [];
		for (let i = 0; i < prim.indices.data.length / 3; ++i) {
			indinner.push(prim.indices.data[i * 3 + 0], prim.indices.data[i * 3 + 2], prim.indices.data[i * 3 + 1]);
		}
		priminner.indices = new pcInfo(new Uint16Array(indinner), indinner.length, 1, false, 0, 0);
		prim.wellpos = wellPC;
		priminner.wellpos = wellinnerPC;
		vao = gl.createVAO(program, {position: "a_position", normal: "a_normal", texcoord: "a_texcoord", wellpos: "a_mappos"}, prim, 'indices', undefined, {wellpos: 1}, 'wellpos');
		vaoinner = gl.createVAO(program, {position: "a_position", normal: "a_normal", texcoord: "a_texcoord", wellpos: "a_mappos"}, priminner, 'indices', undefined, {wellpos: 1}, 'wellpos');
	});
	const texture = gl.loadTexture('../img/cube.png');

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [30, 20, 40];
	const target = [10, 20, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	const lightPos = [-20, 40, 80];
	const viewWorldPos = cameraMatrix.slice(12, 15);
	let then = 0;
	let time = 0;
	let shape;
	let pos = [0, 0];
	let size = 0;
	let score = 0;
	let level = 1;
	let linesToNext = 5;
	let speed = getSpeed(level);
	let speedup = false;
	let preventtouch = false;

	function gameOver() {
		shape = undefined;
		colorMap = new Uint8Array(wellSize[0] * wellSize[1]);
		score = 0;
		level = 1;
		linesToNext = 5;
		speed = getSpeed(level);
		speedup = false;
	}

	function changeScore(lines) {
		let res = 0;
		switch (lines) {
			case 1:
				res = 1;
				break;
			case 2:
				res = 3;
				break;
			case 3:
				res = 5;
				break;
			case 4:
				res = 8;
				break;
		}
		score += res;
		linesToNext -= res;
		if (linesToNext <= 0) {
			linesToNext = ++level * 5 + linesToNext;
		}
	}

	const controls = new Controls(canvas, {
		ksp: function(v) {if (v == 0) {gameOver()}},
		kup: function(v) {if (!v && !speedup) {let s = rotShape(shape, size); if (validPos(pos, s, size)) shape = s;}},
		kri: function(v) {if (v && !speedup) if (validPos([pos[0] + 1, pos[1]], shape, size)) ++pos[0];},
		kle: function(v) {if (v && !speedup) if (validPos([pos[0] - 1, pos[1]], shape, size)) --pos[0];},
		kdo: function(v) {if (!v && !speedup) {speed /= 10; speedup = true;}},
		preventdeftouch: function() {return preventtouch;},
		mlc: function(x, y) {if (y > gl.clientHeight * .8) {preventtouch = true; gameOver();} else {if (!speedup) {let s = rotShape(shape, size); if (validPos(pos, s, size)) shape = s;}}},
		tm: function(t) {
			let update = false;
			if (!speedup) {
				if (t.currY - t.lastY > 30) {
					speed /= 10;
					speedup = true;
					update = true;
				} else {
					if (Math.abs(t.currX - t.lastX) > 8) {
						update = true;
						if (t.currX - t.lastX > 0) {
							if (validPos([pos[0] + 1, pos[1]], shape, size)) {
								++pos[0];
							}
						} else {
							if (validPos([pos[0] - 1, pos[1]], shape, size)) {
								--pos[0];
							}
						}
					}
				}
			}
			return update;
		},
	}, false);

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);
		scoreNode.nodeValue = score;
		levelNode.nodeValue = level;

		time += deltaTime;
		function getShape() {
			const s = getNextShape();
			shape = shapeDef[s].shape;
			size = shapeDef[s].size;
			pos = [wellSize[0] / 2 - Math.ceil(size / 2), wellSize[1]];
			if (!validPos(pos, shape, size)) {
				gameOver();
			}
		}
		if (!shape) {
			getShape();
			time = 0;
		} else {
			if (time > speed) {
				if (validPos([pos[0], pos[1] - 1], shape, size)) {
					--pos[1];
				} else {
					blendShape(shape, size, pos, true);
					const lines = removeFullLines();
					changeScore(lines);
					getShape();
					speed = getSpeed(level);
					speedup = false;
				}
				time = 0;
			}
		}
		colorMap2Texture(blendShape(shape, size, pos, false));

		glUtils.resizeCanvas(gl.canvas);

		controls.updatePads();

		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const matrix = m4.translation((wellSize[0] + 0.5) * cubeSize, 0, -cubeSize);
		m4.yRotate(matrix, Math.PI, matrix);
		m4.scale(matrix, resPlate, resPlate, 1, matrix);
		const worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, matrix);
		const viewInverseTranspose = m4.transpose(m4.inverse(matrix));
		gl.initFrame(undefined, [0, 0, 0, 1]);
		gl.blendOff();
		gl.drawVAO(plateVao, {u_worldViewProjection: worldViewProjectionMatrix, u_world: matrix, u_worldInverseTranspose: viewInverseTranspose, u_lightWorldPosition: lightPos, u_viewWorldPosition: viewWorldPos, u_shinemap: metalMetTex, u_coltexture: metalColorTex, u_normtexture: metalNormalTex, u_dispmap: dispMetTex, u_reflmap: reflMetTex});

		m4.identity(matrix);
		m4.multiply(viewProjectionMatrix, matrix, worldViewProjectionMatrix);
		m4.transpose(m4.inverse(matrix), viewInverseTranspose);
		const uniforms = {u_worldViewProjection: worldViewProjectionMatrix, u_world: matrix, u_worldInverseTranspose: viewInverseTranspose, u_lightWorldPosition: lightPos, u_viewWorldPosition: viewWorldPos, u_shininess: 30, u_texture: texture, u_cubemap: colorMapTexture, u_cubeSize: cubeSize};
		gl.blendOn();
		gl.drawVAO(vaoinner, uniforms);
		gl.drawVAO(vao, uniforms);

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		const scoreElement = document.getElementById("score");
		const scoreNode = document.createTextNode("");
		scoreElement.appendChild(scoreNode);
		const levelElement = document.getElementById("level");
		const levelNode = document.createTextNode("");
		levelElement.appendChild(levelNode);
		draw(fpsNode, levelNode, scoreNode, gl, canvas);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
