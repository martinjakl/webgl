// Template
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_matrix;
out vec2 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = a_texcoord;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

function draw(fpsNode, gl) {
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);
		gl.gl.save();
		gl.gl.font = "130px verdana";
		gl.gl.fillText("Under construction", 20, 160);
		gl.gl.restore();

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	//const gl = new glUtils(canvas.getContext("webgl2"));
	const gl = new glUtils(canvas.getContext("2d"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
