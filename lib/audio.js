/*
 * Copyright 2018 Martin Jakl (martin.jakl@macsnet.cz).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
"use strict";

class WAAudio {
	constructor(ctx) {
		this.ctx = ctx;
		this.buffer = null;
	}

	play() {
		if (this.buffer !== null) {
			const source = this.ctx.context.createBufferSource();
			source.buffer = this.buffer;
			source.connect(this.ctx.context.destination);
			source.loop = false;
			source.start();
		}
	}
}

class WAContext {
	constructor() {
		this.ctx = new AudioContext();
	}

	load(file) {
		const req = new XMLHttpRequest();
		req.open('GET', file);
		req.responseType = 'arraybuffer';
		const ctx = this.ctx;
		const result = new WAAudio(this);
		req.onload = function() {
			const audioData = req.response;
			ctx.decodeAudioData(audioData, function(buffer) {
				result.buffer = buffer;
			}, function(e) {
				console.log("Error decoding audio data" + e.err);
			});
		};
		req.send();
		return result;
	}

	get context() {
		return this.ctx;
	}

	resume() {
		this.ctx.resume();
	}
}

