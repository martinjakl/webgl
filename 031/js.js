// Directional lightning
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec3 a_normal;
uniform mat4 u_worldViewProjection;
uniform mat4 u_worldInverseTranspose;
out vec3 v_normal;

void main() {
	gl_Position = u_worldViewProjection * a_position;
	v_normal = mat3(u_worldInverseTranspose) * a_normal;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec3 v_normal;
uniform vec3 u_reverseLightDirection;
uniform vec4 u_color;
out vec4 outColor;
void main() {
	vec3 normal = normalize(v_normal);
	float light = dot(normal, u_reverseLightDirection);
	outColor = u_color;
	outColor.rgb *= light;
}
`;

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", normal: "a_normal"}, Primitive.moveRotateScale(Primitive.createF(), [-50, -75, -15], [Math.PI, 0, 0], [1, 1, 1]));

	const fieldOfViewRadians = m3.deg2rad(60);
	let fRotation = 0;
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [100, 150, 200];
	const target = [0, 35, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		fRotation += deltaTime * Math.PI / 4;

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);
		const worldMatrix = m4.yRotation(fRotation);
		const worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, worldMatrix);
		const worldInverseMatrix = m4.inverse(worldMatrix);
		const worldInverseTransposeMatrix = m4.transpose(worldInverseMatrix);
		gl.drawVAO(vao, {u_worldInverseTranspose: worldInverseTransposeMatrix, u_worldViewProjection: worldViewProjectionMatrix, u_color: [0.2, 1, 0.2, 1], u_reverseLightDirection: m4.normalize([0.5, 0.7, 1])});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
