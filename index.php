<!DOCTYPE html>
<html>
<head>
	<title>WebGL</title>
	<link rel="stylesheet" href="css.css">
	<link rel="icon" type="image/png" href="img/icon.png">
	<script src="js.js"></script>
</head>
<body>
	<div id="outer"><div id="inner">
	<?php
		$dir = scandir(".");
		foreach ($dir as $d) {
			if (is_dir($d) && $d[0] != '.') {
				$h = @fopen($d.'/js.js', 'r');
				if ($h) {
					$l = fgets($h);
					fclose($h);
					echo '<p name="item"><a href="'.$d.'" >'.substr($l, 2).'</a></p>';
				}
			}
		}
	?>
	</div></div>
	<p id="masto"><a rel="me" href="https://mastodon.macsnet.cz/@Mac_CZ">Mastodon</a></p>
</body>
</html>

