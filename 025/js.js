// Scene graph
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec4 a_color;
uniform mat4 u_matrix;
out vec4 v_color;

void main() {
	gl_Position = u_matrix * a_position;
	v_color = a_color;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec4 v_color;
uniform vec4 u_colorMult;
uniform vec4 u_colorOffset;
out vec4 outColor;
void main() {
	outColor = v_color * u_colorMult + u_colorOffset;
}
`;

class Node {
	constructor() {
		this.children = [];
		this.localMatrix = m4.identity();
		this.worldMatrix = m4.identity();
		this.vao = null;
		this.parent = null;
		this.uniforms = {};
	}

	setParent(parent) {
		if (this.parent) {
			const ndx = this.parent.children.indexOf(this);
			if (ndx >= 0) {
				this.parent.children.splice(ndx, 1);
			}
		}
		if (parent) {
			parent.children.push(this);
		}
		this.parent = parent;
	}

	updateWorldMatrix(parentWorldMatrix) {
		if (parentWorldMatrix) {
			m4.multiply(parentWorldMatrix, this.localMatrix, this.worldMatrix);
		} else {
			this.localMatrix.set(this.worldMatrix);
		}
		const worldMatrix = this.worldMatrix;
		this.children.forEach(function(child) {
			child.updateWorldMatrix(worldMatrix);
		});
	}
}

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", color: "a_color"}, Primitive.moveRotateScale(Primitive.addRandColor(Primitive.createUVSphere(10, 12), 'position'), [0, 0, 0], [0, 0, 0], [5, 5, 5]), 'indices');

	const sunNode = new Node();
	sunNode.localMatrix = m4.scaling(5, 5, 5);
	sunNode.vao = vao;
	sunNode.uniforms = {u_colorOffset: [0.6, 0.6, 0, 1], u_colorMult: [0.4, 0.4, 0, 1]};

	const earthNode = new Node();
	earthNode.localMatrix = m4.scaling(2, 2, 2);
	earthNode.vao = vao;
	earthNode.uniforms = {u_colorOffset: [0.2, 0.5, 0.8, 1], u_colorMult: [0.8, 0.5, 0.2, 1]};

	const moonNode = new Node();
	moonNode.localMatrix = m4.scaling(0.4, 0.4, 0.4);
	moonNode.vao = vao;
	moonNode.uniforms = {u_colorOffset: [0.6, 0.6, 0.6, 1], u_colorMult: [0.1, 0.1, 0.1, 1]};

	const solarSystemNode = new Node();
	const earthOrbitNode = new Node();
	earthOrbitNode.localMatrix = m4.translation(100, 0, 0);
	const moonOrbitNode = new Node();
	moonOrbitNode.localMatrix = m4.translation(30, 0, 0);

	sunNode.setParent(solarSystemNode);
	earthOrbitNode.setParent(solarSystemNode);
	earthNode.setParent(earthOrbitNode);
	moonOrbitNode.setParent(earthOrbitNode);
	moonNode.setParent(moonOrbitNode);

	const objects = [sunNode, earthNode, moonNode];

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, -200, 0];
	const target = [0, 0, 0];
	const up = [0, 0, 1];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		earthOrbitNode.localMatrix = m4.multiply(m4.yRotation(0.6 * deltaTime), earthOrbitNode.localMatrix);
		moonOrbitNode.localMatrix = m4.multiply(m4.yRotation(0.6 * deltaTime), moonOrbitNode.localMatrix);
		sunNode.localMatrix = m4.multiply(m4.yRotation(0.2 * deltaTime), sunNode.localMatrix);
		earthNode.localMatrix = m4.multiply(m4.yRotation(2 * deltaTime), earthNode.localMatrix);
		moonNode.localMatrix = m4.multiply(m4.yRotation(-2 / 5 * deltaTime), moonNode.localMatrix);
		solarSystemNode.updateWorldMatrix();

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame(null, [0, 0, 0, 1]);
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		objects.forEach(function(object) {
			object.uniforms.u_matrix = m4.multiply(viewProjectionMatrix, object.worldMatrix);
			gl.drawVAO(object.vao, object.uniforms);
		});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
