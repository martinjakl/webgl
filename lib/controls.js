/*
 * Copyright 2018 Martin Jakl (martin.jakl@macsnet.cz).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
"use strict";

class Controls {
	constructor(canvas, feedback, askPointerLock) {
		this.feedback = feedback;
		const c = this;
		this.pointerLock = false;
		this.interval = null;

		//navigator.requestWakeLock('screen');

		document.onkeyup = function(event) {
			if (event.key == ' ') {
				if (c.feedback.ksp) c.feedback.ksp(0);
			} else if (event.key == 'ArrowDown') {
				if (c.feedback.kdo) c.feedback.kdo(0);
			} else if (event.key == 'ArrowUp') {
				if (c.feedback.kup) c.feedback.kup(0);
			} else if (event.key == 'ArrowLeft') {
				if (c.feedback.kle) c.feedback.kle(0);
			} else if (event.key == 'ArrowRight') {
				if (c.feedback.kri) c.feedback.kri(0);
			} else if (event.key == 'w') {
				if (c.feedback.kw) c.feedback.kw(0);
			} else if (event.key == 's') {
				if (c.feedback.ks) c.feedback.ks(0);
			}
		};

		document.onkeydown = function(event) {
			if (event.key == 'ArrowDown') {
				if (c.feedback.kdo) c.feedback.kdo(1);
			} else if (event.key == 'ArrowUp') {
				if (c.feedback.kup) c.feedback.kup(1);
			} else if (event.key == 'ArrowLeft') {
				if (c.feedback.kle) c.feedback.kle(1);
			} else if (event.key == 'ArrowRight') {
				if (c.feedback.kri) c.feedback.kri(1);
			} else if (event.key == 'w') {
				if (c.feedback.kw) c.feedback.kw(1);
			} else if (event.key == 's') {
				if (c.feedback.ks) c.feedback.ks(1);
			}
		};

		document.addEventListener('click', function(event) {
			if (event.button === 0 && c.feedback.mlc) c.feedback.mlc(event.clientX, event.clientY);
			if (event.button === 1 && c.feedback.mmc) c.feedback.mmc(event.clientX, event.clientY);
			if (event.button === 2 && c.feedback.mrc) c.feedback.mrc(event.clientX, event.clientY);
		});

		document.addEventListener('dblclick', function(event) {
			if (event.button === 0 && c.feedback.mld) c.feedback.mld(event.clientX, event.clientY);
			if (event.button === 1 && c.feedback.mmd) c.feedback.mmd(event.clientX, event.clientY);
			if (event.button === 2 && c.feedback.mrd) c.feedback.mrd(event.clientX, event.clientY);
		});

		if (askPointerLock) {
			canvas.onclick = function() {
				canvas.requestPointerLock();
			};
		}

		function updatePos(e) {
			if (c.feedback.mx) c.feedback.mx(e.movementX);
			if (c.feedback.my) c.feedback.my(e.movementY);
			clearInterval(c.interval);
			c.interval = setInterval(function() {
				if (c.feedback.mx) c.feedback.mx(0);
				if (c.feedback.my) c.feedback.my(0);
				clearInterval(c.interval);
			}, 20);
		};

		document.addEventListener('pointerlockchange', function() {
			if (document.pointerLockElement === canvas) {
				document.addEventListener("mousemove", updatePos, false);
			} else {
				document.removeEventListener("mousemove", updatePos, false);
				if (c.feedback.mx) c.feedback.mx(0);
				if (c.feedback.my) c.feedback.my(0);
			}
		}, false);

		this.touches = {};

		canvas.addEventListener("touchstart", function(evt) {
			const touches = evt.changedTouches;
			for (let i = 0; i < touches.length; ++i) {
				const t = {};
				t.lastX = touches[i].pageX;
				t.lastY = touches[i].pageY;
				t.startX = touches[i].pageX;
				t.startY = touches[i].pageY;
				c.touches[touches[i].identifier] = t;
				if (c.feedback.ts) c.feedback.ts(t);
			}
		}, false);

		canvas.addEventListener("touchend", function(evt) {
			if (c.feedback.tlx) c.feedback.tlx(0);
			if (c.feedback.tly) c.feedback.tly(0);
			const touches = evt.changedTouches;
			for (let i = 0; i < touches.length; ++i) {
				if (c.feedback.te) c.feedback.te(c.touches[touches[i].identifier]);
				delete c.touches[touches[i].identifier];
			}
		}, false);

		canvas.addEventListener("touchcancel", function(evt) {
			if (c.feedback.tlx) c.feedback.tlx(0);
			if (c.feedback.tly) c.feedback.tly(0);
			const touches = evt.changedTouches;
			for (let i = 0; i < touches.length; ++i) {
				if (c.feedback.te) c.feedback.te(c.touches[touches[i].identifier]);
				delete c.touches[touches[i].identifier];
			}
		}, false);

		canvas.addEventListener("touchmove", function(evt) {
			if (c.feedback.preventdeftouch && c.feedback.preventdeftouch()) {
				evt.preventDefault();
			}
			const touches = evt.changedTouches;
			for (let i = 0; i < touches.length; ++i) {
				const t = c.touches[touches[i].identifier];
				t.currX = touches[i].pageX;
				t.currY = touches[i].pageY;
				if (c.feedback.tlx) c.feedback.tlx(t.currX - t.lastX);
				if (c.feedback.tly) c.feedback.tly(t.currY - t.lastY);
				let update = true;
				if (c.feedback.tm) update = c.feedback.tm(t);
				if (update) {
					t.lastX = touches[i].pageX;
					t.lastY = touches[i].pageY;
				}
			}
		}, false);
	}

	updatePads() {
		const gp = navigator.getGamepads();

		for (let i = 0; i < gp.length; ++i) {
			if (gp[i]) {
				if (gp[i].axes[1] > 0.15 || gp[i].axes[1] < -0.15) {
					if (this.feedback.ply) this.feedback.ply(gp[i].axes[1], i);
				} else {
					if (this.feedback.ply) this.feedback.ply(0, i);
				}
				if (gp[i].axes[0] > 0.15 || gp[i].axes[0] < -0.15) {
					if (this.feedback.plx) this.feedback.plx(gp[i].axes[0], i);
				} else {
					if (this.feedback.plx) this.feedback.plx(0, i);
				}
				if (gp[i].axes[3] > 0.15 || gp[i].axes[3] < -0.15) {
					if (this.feedback.pry) this.feedback.pry(gp[i].axes[3], i);
				} else {
					if (this.feedback.pry) this.feedback.pry(0, i);
				}
				if (gp[i].axes[2] > 0.15 || gp[i].axes[2] < -0.15) {
					if (this.feedback.prx) this.feedback.prx(gp[i].axes[2], i);
				} else {
					if (this.feedback.prx) this.feedback.prx(0, i);
				}
				if (gp[i].buttons[0].pressed) {
					if (this.feedback.pb0) this.feedback.pb0(1, i);
				} else {
					if (this.feedback.pb0) this.feedback.pb0(0, i);
				}
			}
		}
	}
};

