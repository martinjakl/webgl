// Flag
"use strict";
const xVSSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_MVMatrix;
uniform mat4 u_PMatrix;
uniform float u_StartAngle;
uniform float u_Pi;
uniform float u_XSize;
uniform float u_YSize;
out vec2 v_texcoord;

void main() {
	float z = a_position.z + sin(u_StartAngle + (a_position.x / u_XSize + 1.0) * u_Pi) * 0.5 * u_XSize / 2.0;
	gl_Position = u_PMatrix * u_MVMatrix * vec4(a_position.xy, z, 1.0);
	v_texcoord = a_texcoord;
}
`;

const xyVSSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_MVMatrix;
uniform mat4 u_PMatrix;
uniform float u_StartAngle;
uniform float u_Pi;
uniform float u_XSize;
uniform float u_YSize;
out vec2 v_texcoord;

void main() {
	float currAngleX = u_StartAngle + (a_position.x / u_XSize + 1.0) * u_Pi;
	float currAngleY = u_StartAngle + (a_position.y / u_YSize + 1.0) * u_Pi;
	float z = a_position.z + sin(currAngleX + currAngleY) * 0.3 * u_XSize / 2.0;
	gl_Position = u_PMatrix * u_MVMatrix * vec4(a_position.xy, z, 1.0);
	v_texcoord = a_texcoord;
}
`;

const x_yVSSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_MVMatrix;
uniform mat4 u_PMatrix;
uniform float u_StartAngle;
uniform float u_Pi;
uniform float u_XSize;
uniform float u_YSize;
out vec2 v_texcoord;

void main() {
	float currAngleX = u_StartAngle + (a_position.x / u_XSize + 1.0) * u_Pi;
	float currAngleY = u_StartAngle + (a_position.y / u_YSize + 1.0) * u_Pi * 0.75;
	float zx = a_position.z + sin(currAngleX) * 0.3 * u_XSize / 2.0;
	float zy = a_position.z + sin(currAngleY) * 0.3 * u_YSize / 2.0;
	gl_Position = u_PMatrix * u_MVMatrix * vec4(a_position.xy, zx + zy, 1.0);
	v_texcoord = a_texcoord;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

function draw(fpsNode, gl) {
	const programs = [gl.createProgramShaders(xVSSource, fragmentShaderSource),
		gl.createProgramShaders(xyVSSource, fragmentShaderSource),
		gl.createProgramShaders(x_yVSSource, fragmentShaderSource)
	];
	const xSize = 9;
	const ySize = 12;
	const vao = gl.createVAO(programs[0], {position: "a_position", texcoord: "a_texcoord"}, Primitive.createRectangle(xSize, ySize), "indices");
	const texture = gl.loadTexture('../img/pic.jpg');
	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, -30];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;
	const mVMatrix = [
		m4.translation(0, 10, 10),
		m4.translation(-10, -10, 10),
		m4.translation(10, -10, 10),
	];
	mVMatrix.forEach(mat => {
		m4.xRotate(mat, m3.deg2rad(20), mat);
		m4.yRotate(mat, m3.deg2rad(-20), mat);
	});
	let startAngle = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		startAngle += 0.05;

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		const projectionMatrix = m4.perspective(fieldOfViewRadians, gl.clientWidth / gl.clientHeight, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);
		for (let i = 0; i < programs.length; ++i) {
			vao.pi = programs[i];
			gl.drawVAO(vao, {u_Pi: Math.PI, u_texture: texture, u_PMatrix: viewProjectionMatrix, u_MVMatrix: mVMatrix[i], u_StartAngle: startAngle, u_XSize: xSize, u_YSize: ySize});
		}

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
