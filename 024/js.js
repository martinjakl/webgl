// Spheres and gamepad/keyboard/mouse/touch control
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
uniform mat4 u_matrix;

void main() {
	gl_Position = u_matrix * a_position;
	gl_PointSize = 5.0;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
out vec4 outColor;
void main() {
	outColor = vec4(0.5, 0.5, 0.2, 1.0);
}
`;

const cVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec4 a_color;
uniform mat4 u_matrix;
out vec4 v_color;

void main() {
	gl_Position = u_matrix * a_position;
	v_color = a_color;
}
`;

const cFragmentShaderSource = `#version 300 es
precision mediump float;
in vec4 v_color;
out vec4 outColor;
void main() {
	outColor = v_color;
}
`;

function draw(fpsNode, gl, canvas) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const cProgram = gl.createProgramShaders(cVertexShaderSource, cFragmentShaderSource);
	const vao1 = gl.createVAO(cProgram, {position: "a_position", color: "a_color"}, Primitive.addRandColor(Primitive.createUVSphere(20, 20), 'position'), 'indices');
	const vao2 = gl.createVAO(cProgram, {position: "a_position", color: "a_color"}, Primitive.addRandColor(Primitive.createNormalizedCube(8), 'position'), 'indices');
	const vao3 = gl.createVAO(cProgram, {position: "a_position", color: "a_color"}, Primitive.addRandColor(Primitive.createSpherifiedCube(8), 'position'), 'indices');
	const vao4 = gl.createVAO(cProgram, {position: "a_position", color: "a_color"}, Primitive.addRandColor(Primitive.createIcosahedron(3), 'position'), 'indices');
	const vaoLetters = [];
	function VAOfromJSON(prim) {
		if (vaoLetters.length == 2) {
			vaoLetters.shift();
		}
		vaoLetters.push(gl.createVAO(cProgram, {position: "a_position", color: "a_color"}, Primitive.addRandColor(Primitive.moveRotateScale(prim, m4.multiplyVector(m4.addVectors(prim.extents.max, prim.extents.min), -.5), [0, 0, 0], [1, 1, 1]), 'position'), 'indices'));
	}
	Primitive.loadJSON('../mesh/A.json', VAOfromJSON);
	Primitive.loadJSON('../mesh/T.json', VAOfromJSON);

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, 10];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;
	let rotX = 0;
	let rotY = 0;
	let autoAnim = true;
	let xmod = 0;
	let ymod = 0;
	let xpmod = 0;
	let ypmod = 0;
	let lb = null;

	const controls = new Controls(canvas, {
		pb0: function(v, i) {if (v == 0 && lb === i) {autoAnim = !autoAnim; lb = null} if (v) {lb = i;}},
		ksp: function(v) {if (v == 0) {autoAnim = !autoAnim;}},
		mlc: function() {autoAnim = !autoAnim;},
		ply: function(y) {ypmod = y;},
		plx: function(x) {xpmod = x;},
		my: function(y) {ymod = y;},
		mx: function(x) {xmod = x;},
		tly: function(y) {ymod = y;},
		tlx: function(x) {xmod = x;},
		kup: function(v) {ymod = -v;},
		kdo: function(v) {ymod = v;},
		kri: function(v) {xmod = v;},
		kle: function(v) {xmod = -v;},
		preventdeftouch: function() {return !autoAnim;},
	}, true);
	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		controls.updatePads();

		if (autoAnim) {
			rotX += deltaTime * 0.5 % 2 * Math.PI;
			rotY += deltaTime * 0.7 % 2 * Math.PI;
		} else {
			rotX += deltaTime * 0.5 % 2 * Math.PI * (ypmod ? ypmod : ymod);
			rotY += deltaTime * 0.7 % 2 * Math.PI * (xpmod ? xpmod : xmod);
		}

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const matrix = m4.translate(viewProjectionMatrix, 0, 4, 0);
		m4.xRotate(matrix, rotX, matrix);
		m4.yRotate(matrix, rotY, matrix);
		gl.drawVAO(vao1, {u_matrix: matrix}, gl.PrimType.LINE_STRIP);

		m4.translate(viewProjectionMatrix, 4, 0, 0, matrix);
		m4.scale(matrix, 1, 1.1, 1, matrix);
		m4.xRotate(matrix, rotX, matrix);
		m4.yRotate(matrix, rotY, matrix);
		gl.drawVAO(vao2, {u_matrix: matrix}, gl.PrimType.LINE_STRIP);

		m4.translate(viewProjectionMatrix, -4, 0, 0, matrix);
		m4.scale(matrix, 1, 1.1, 1, matrix);
		m4.xRotate(matrix, rotX, matrix);
		m4.yRotate(matrix, rotY, matrix);
		gl.drawVAO(vao3, {u_matrix: matrix}, gl.PrimType.LINE_STRIP);

		m4.translate(viewProjectionMatrix, 0, -4, 0, matrix);
		m4.xRotate(matrix, rotX, matrix);
		m4.yRotate(matrix, rotY, matrix);
		gl.drawVAO(vao4, {u_matrix: matrix}, gl.PrimType.LINE_STRIP);

		m4.translate(viewProjectionMatrix, -2, 2, -10, matrix);
		m4.scale(matrix, 5, 5, 5, matrix);
		m4.xRotate(matrix, rotX, matrix);
		m4.yRotate(matrix, rotY, matrix);
		gl.drawVAO(vaoLetters[0], {u_matrix: matrix});

		m4.translate(viewProjectionMatrix, 2, -2, -10, matrix);
		m4.scale(matrix, 5, 5, 5, matrix);
		m4.xRotate(matrix, rotX, matrix);
		m4.yRotate(matrix, rotY, matrix);
		gl.drawVAO(vaoLetters[1], {u_matrix: matrix});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl, canvas);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
