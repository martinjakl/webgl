//Multiple textures
function createShader(gl, type, source) {
	const shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	if (success) {
		return shader;
	}
	console.log(gl.getShaderInfoLog(shader));
	gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
	const program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	const success = gl.getProgramParameter(program, gl.LINK_STATUS);
	if (success) {
		return program;
	}
	console.log(gl.getProgramInfoLog(program));
	gl.deleteProgram(program);
}

function resize(canvas) {
	const displayWidth = canvas.clientWidth;
	const displayHeight = canvas.clientHeight;
	if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
		canvas.width = displayWidth;
		canvas.height = displayHeight;
	}
}

function randomInt(range) {
	return Math.floor(Math.random() * range);
}

function setRectangle(gl, x, y, width, height) {
	const x1 = x;
	const x2 = x + width;
	const y1 = y;
	const y2 = y + height;
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
		x1, y1,
		x2, y1,
		x1, y2,
		x1, y2,
		x2, y1,
		x2, y2]), gl.STATIC_DRAW);
}

function render(images) {
	const canvas = document.getElementById("c");
	const gl = canvas.getContext("webgl2");
	if (gl) {
		const vertexShaderSource = `#version 300 es
		in vec2 a_position;
		in vec2 a_textCoord;
		uniform vec2 u_resolution;
		out vec2 v_textCoord;
		void main() {
			vec2 zeroToOne = a_position / u_resolution;
			vec2 zeroToTwo = zeroToOne * 2.0;
			vec2 clipSpace = zeroToTwo - 1.0;
			gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);
			v_textCoord = a_textCoord;
		}
		`;
		const fragmentShaderSource = `#version 300 es
		precision mediump float;
		uniform sampler2D u_image0;
		uniform sampler2D u_image1;
		in vec2 v_textCoord;
		out vec4 outColor;
		void main() {
			vec4 color0 = texture(u_image0, v_textCoord);
			vec4 color1 = texture(u_image1, v_textCoord);
			outColor = color0 * color1;
		}
		`;

		const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
		const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
		const program = createProgram(gl, vertexShader, fragmentShader);
		const positionAttributeLocation = gl.getAttribLocation(program, "a_position");
		const textCoordAttributeLocation = gl.getAttribLocation(program, "a_textCoord");
		const resolutionUniformLocation = gl.getUniformLocation(program, "u_resolution");
		const image0UniformLocation = gl.getUniformLocation(program, "u_image0");
		const image1UniformLocation = gl.getUniformLocation(program, "u_image1");
		const positionBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

		const vao = gl.createVertexArray();
		gl.bindVertexArray(vao);
		gl.enableVertexAttribArray(positionAttributeLocation);
		let size = 2;
		let type = gl.FLOAT;
		let normalize = false;
		let stride = 0;
		let offset = 0;
		gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset);

		const textCoordBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, textCoordBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
			0.0, 0.0,
			1.0, 0.0,
			0.0, 1.0,
			0.0, 1.0,
			1.0, 0.0,
			1.0, 1.0,
		]), gl.STATIC_DRAW);
		gl.enableVertexAttribArray(textCoordAttributeLocation);
		size = 2;
		type = gl.FLOAT;
		normalize = false;
		stride = 0;
		offset = 0;
		gl.vertexAttribPointer(textCoordAttributeLocation, size, type, normalize, stride, offset);

		const textures = [];
		for (let ii = 0; ii < 2; ++ii) {
			const texture = gl.createTexture();
			gl.bindTexture(gl.TEXTURE_2D, texture);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			const mipLevel = 0;
			const internalFormat = gl.RGBA;
			const srcFormat = gl.RGBA;
			const srcType = gl.UNSIGNED_BYTE;
			gl.texImage2D(gl.TEXTURE_2D, mipLevel, internalFormat, srcFormat, srcType, images[ii]);
			textures.push(texture);
		}

		resize(gl.canvas);
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
		gl.clearColor(0, 0, 0, 0);
		gl.clear(gl.COLOR_BUFFER_BIT);
		gl.useProgram(program);
		gl.uniform2f(resolutionUniformLocation, gl.canvas.width, gl.canvas.height);
		gl.uniform1i(image0UniformLocation, 0);
		gl.uniform1i(image1UniformLocation, 1);
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, textures[0]);
		gl.activeTexture(gl.TEXTURE1);
		gl.bindTexture(gl.TEXTURE_2D, textures[1]);

		setRectangle(gl, 0, 0, images[0].width, images[0].height);
		const primitiveType = gl.TRIANGLES;
		offset = 0;
		const count = 6;
		gl.drawArrays(primitiveType, offset, count);
	} else {
		console.log("No GL");
	}
}

function loadImage(url, callback) {
	const image = new Image();
	image.src = url;
	image.onload = callback;
	return image;
}

function loadImages(urls, callback) {
	const images = [];
	let imagesToLoad = urls.length;
	const onImageLoad = function() {
		--imagesToLoad;
		if (imagesToLoad == 0) {
			callback(images);
		}
	};
	for (let ii = 0; ii < imagesToLoad; ++ii) {
		const image = loadImage(urls[ii], onImageLoad);
		images.push(image);
	}
}

function start() {
	loadImages(["../img/pic1.jpg", "../img/pic2.jpg"], render);
}

document.addEventListener('DOMContentLoaded', start);
