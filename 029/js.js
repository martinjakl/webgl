// Scene graph 2
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec4 a_color;
uniform mat4 u_matrix;
out vec4 v_color;

void main() {
	gl_Position = u_matrix * a_position;
	v_color = a_color;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec4 v_color;
uniform vec4 u_colorMult;
uniform vec4 u_colorOffset;
out vec4 outColor;
void main() {
	outColor = v_color * u_colorMult + u_colorOffset;
}
`;

class TRS {
	constructor() {
		this.translation = [0, 0, 0];
		this.rotation = [0, 0, 0];
		this.scale = [1, 1, 1];
	}

	getMatrix(dst) {
		dst = dst || new Float32Array(16);
		const t = this.translation;
		const r = this.rotation;
		const s = this.scale;

		m4.translation(t[0], t[1], t[2], dst);
		m4.xRotate(dst, r[0], dst);
		m4.yRotate(dst, r[1], dst);
		m4.zRotate(dst, r[2], dst);
		m4.scale(dst, s[0], s[1], s[2], dst);
		return dst;
	}
}

class Node {
	constructor(source) {
		this.children = [];
		this.localMatrix = m4.identity();
		this.worldMatrix = m4.identity();
		this.vao = null;
		this.parent = null;
		this.uniforms = {};
		this.source = source;
	}

	setParent(parent) {
		if (this.parent) {
			const ndx = this.parent.children.indexOf(this);
			if (ndx >= 0) {
				this.parent.children.splice(ndx, 1);
			}
		}
		if (parent) {
			parent.children.push(this);
		}
		this.parent = parent;
	}

	updateWorldMatrix(parentWorldMatrix) {
		const source = this.source;
		if (source) {
			source.getMatrix(this.localMatrix);
		}
		if (parentWorldMatrix) {
			m4.multiply(parentWorldMatrix, this.localMatrix, this.worldMatrix);
		} else {
			this.localMatrix.set(this.worldMatrix);
		}
		const worldMatrix = this.worldMatrix;
		this.children.forEach(function(child) {
			child.updateWorldMatrix(worldMatrix);
		});
	}
}

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", color: "a_color"}, Primitive.createCube(1));

	const objects = [];
	const nodeInfosByName = {};
	const blockGuyNodeDescriptions = {
		name: "point between feet",
		draw: false,
		children: [{
			name: "waist",
			translation: [0, 3, 0],
			children: [{
				name: "torso",
				translation: [0, 2, 0],
				children: [{
					name: "neck",
					translation: [0, 1, 0],
					children: [{
						name: "head",
						translation: [0, 1, 0],
					}]
				}, {
					name: "left-arm",
					translation: [-1, 0, 0],
					children: [{
						name: "left-forearm",
						translation: [-1, 0, 0],
						children: [{
							name: "left-hand",
							translation: [-1, 0, 0]
						}]
					}],
				}, {
					name: "right-arm",
					translation: [1, 0, 0],
					children: [{
						name: "right-forearm",
						translation: [1, 0, 0],
						children: [{
							name: "right-hand",
							translation: [1, 0, 0]
						}]
					}],
				}]
			}, {
				name: "left-leg",
				translation: [-1, -1, 0],
				children: [{
					name: "left-calf",
					translation: [0, -1, 0],
					children: [{
						name: "left-foot",
						translation: [0, -1, 0],
					}]
				}]
			}, {
				name: "right-leg",
				translation: [1, -1, 0],
				children: [{
					name: "right-calf",
					translation: [0, -1, 0],
					children: [{
						name: "right-foot",
						translation: [0, -1, 0],
					}]
				}]
			}]
		}]
	}

	function makeNode(nodeDescription) {
		const trs = new TRS();
		const node = new Node(trs);
		nodeInfosByName[nodeDescription.name] = {trs: trs, node: node};
		trs.translation = nodeDescription.translation || trs.translation;
		if (nodeDescription.draw !== false) {
			node.uniforms = {u_colorOffset: [0, 0, 0.6, 0], u_colorMult: [0.4, 0.4, 0.4, 1]};
			node.vao = vao;
			objects.push(node);
		}
		makeNodes(nodeDescription.children).forEach(function(child) {
			child.setParent(node);
		});
		return node;
	}

	function makeNodes(nodeDescriptions) {
		return nodeDescriptions ? nodeDescriptions.map(makeNode) : [];
	}

	const scene = makeNode(blockGuyNodeDescriptions);

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 200;
	const cameraPosition = [4, 3.5, 10];
	const target = [0, 3.5, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		let adjust;
		const speed = 3;
		const c = now * speed;
		adjust = Math.abs(Math.sin(c));
		nodeInfosByName["point between feet"].trs.translation[1] = adjust;
		adjust = Math.sin(c);
		nodeInfosByName["left-leg"].trs.rotation[0] = adjust;
		nodeInfosByName["right-leg"].trs.rotation[0] = -adjust;
		adjust = Math.sin(c + 0.1) * 0.4;
		nodeInfosByName["left-calf"].trs.rotation[0] = -adjust;
		nodeInfosByName["right-calf"].trs.rotation[0] = adjust;
		nodeInfosByName["left-foot"].trs.rotation[0] = -adjust;
		nodeInfosByName["right-foot"].trs.rotation[0] = adjust;
		adjust = Math.sin(c) * 0.4;
		nodeInfosByName["left-arm"].trs.rotation[2] = adjust;
		nodeInfosByName["right-arm"].trs.rotation[2] = adjust;
		adjust = Math.sin(c + 0.1) * 0.4;
		nodeInfosByName["left-forearm"].trs.rotation[2] = adjust;
		nodeInfosByName["right-forearm"].trs.rotation[2] = adjust;
		adjust = Math.sin(c - 0.1) * 0.4;
		nodeInfosByName["left-hand"].trs.rotation[2] = adjust;
		nodeInfosByName["right-hand"].trs.rotation[2] = adjust;
		adjust = Math.sin(c) * 0.4;
		nodeInfosByName["waist"].trs.rotation[1] = adjust;
		nodeInfosByName["torso"].trs.rotation[1] = adjust;
		adjust = Math.sin(c + 0.25) * 0.4;
		nodeInfosByName["neck"].trs.rotation[1] = adjust;
		adjust = Math.sin(c + 0.5) * 0.4;
		nodeInfosByName["head"].trs.rotation[1] = adjust;
		adjust = Math.sin(c + 2) * 0.4;
		nodeInfosByName["head"].trs.rotation[0] = adjust;

		scene.updateWorldMatrix();

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		objects.forEach(function(object) {
			object.uniforms.u_matrix = m4.multiply(viewProjectionMatrix, object.worldMatrix);
			gl.drawVAO(object.vao, object.uniforms);
		});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
