// Instanced draw
"use strict";
const fVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
in vec2 a_texcoord2;
in vec4 a_trans;
in vec2 a_trans2;
in float a_speed;
in float a_start;
in float a_alt;
uniform mat4 u_matrix;
uniform float u_time;
out vec2 v_texcoord;

void main() {
	mat4 t = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, a_trans.x + a_trans2.x, a_trans.y, a_trans.z + a_trans2.y, 1);
	float angle = a_speed * u_time + a_start;
	float s = sin(angle);
	float c = cos(angle);
	mat4 rot = mat4(c, 0, -s, 0, 0, 1, 0, 0, s, 0, c, 0, 0, 0, 0, 1);
	vec4 pos = rot * a_position;
	pos = t * pos;
	gl_Position = u_matrix * pos;
	vec2[] tcoords = vec2[2](a_texcoord, a_texcoord2);
	v_texcoord = tcoords[int(a_alt)];
}
`;

const fFragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

const bigFVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec4 a_color;
in vec2 a_trans;
uniform mat4 u_matrix;
out vec4 v_color;

void main() {
	mat4 t = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, a_trans.x, 0, a_trans.y, 1);
	vec4 pos = t * a_position;
	gl_Position = u_matrix * pos;
	v_color = a_color;
}
`;

const bigFFragmentShaderSource = `#version 300 es
precision mediump float;
in vec4 v_color;
out vec4 outColor;

void main() {
	outColor = v_color;
}
`;

function addColumns(prim, count, positions) {
	const colcount = positions.length;
	const transcol = new Float32Array(count * 2 * colcount);
	for (let i = 0; i < transcol.length / (2 * colcount); ++i) {
		for (let j = 0; j < positions.length; ++j) {
			transcol.set(positions[j], i * 2 * colcount + j * 2);
		}
	}
	prim['trans2'] = new pcInfo(transcol, transcol.length / 2, 2, false, 0, 0);
}

function draw(fpsNode, gl) {
	const fProgram = gl.createProgramShaders(fVertexShaderSource, fFragmentShaderSource);
	const bigFProgram = gl.createProgramShaders(bigFVertexShaderSource, bigFFragmentShaderSource);

	const numFs = 35;
	const radius  = 750;
	const rows = 47;
	const rotF = [Math.PI, 0, 0];
	const moveF = [-50, -75, -15];
	const scaleF = [1, 1, 1];
	const prim = Primitive.createManyInstanced(numFs, rows, radius, Primitive.moveRotateScale(Primitive.createF(), moveF, rotF, scaleF), 2);

	const positions = [[0, 0], [2.5 * radius, 0], [-2.5 * radius, 0], [0, 2.5 * radius], [0, -2.5 * radius], [5 * radius, 0], [-5 * radius, 0], [0, 5 * radius], [0, -5 * radius]];
	const colcount = positions.length;
	addColumns(prim, prim['translation'].count, positions);
	const fvao1 = gl.createVAO(fProgram, {position: "a_position", texcoord: "a_texcoord", texcoord2: "a_texcoord2", translation: "a_trans", speed: "a_speed", start: "a_start", alternate: "a_alt", trans2: "a_trans2"}, prim, undefined, undefined, {translation: colcount, speed: colcount, start: colcount, alternate: colcount, trans2: 1}, 'trans2');
	const fTex = gl.loadTexture("../img/f-texture.png");

	const bigF = Primitive.moveRotateScale(Primitive.createF(), moveF, rotF, [7, 7, 7]);
	addColumns(bigF, 1, positions);
	const bvao = gl.createVAO(bigFProgram, {position: "a_position", color: "a_color", trans2: "a_trans"}, bigF, undefined, undefined, {trans2: 1}, 'trans2');

	const fieldOfViewRadians = m3.deg2rad(60);
	let camAngle = 0;

	const zNear = 1;
	const zFar = 10000;
	const fPosition = [radius, 0, 0];
	const up = [0, 1, 0];

	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();

		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

		camAngle += 6 * deltaTime;
		camAngle = camAngle % 360;

		const cameraPosition = [radius * 10 * Math.cos(m3.deg2rad(camAngle)), 1000, radius * 8 * Math.sin(m3.deg2rad(camAngle))];
		const camerMatrix = m4.lookAt(cameraPosition, fPosition, up);
		const viewMatrix = m4.inverse(camerMatrix);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);
		gl.drawVAO(fvao1, {u_matrix: viewProjectionMatrix, u_texture: fTex, u_time: now});
		gl.drawVAO(bvao, {u_matrix: viewProjectionMatrix});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
