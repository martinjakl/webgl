/*
 * Copyright 2018 Martin Jakl (martin.jakl@macsnet.cz).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
"use strict";

class v2 {
	static add(a, ...args) {
		const n = a.slice();
		[...args].forEach(p => {
			n[0] += p[0];
			n[1] += p[1];
		});
		return n;
	}

	static sub(a, ...args) {
		const n = a.slice();
		[...args].forEach(p => {
			n[0] -= p[0];
			n[1] -= p[1];
		});
		return n;
	}

	static mult(a, s) {
		if (Array.isArray(s)) {
			let t = s;
			s = a;
			a = t;
		}
		if (Array.isArray(s)) {
			return [
				a[0] * s[0],
				a[1] * s[1],
			];
		} else {
			return [a[0] * s, a[1] * s];
		}
	}

	static lerp(a, b, t) {
		if (Array.isArray(a)) {
			return [
				a[0] + (b[0] - a[0]) * t,
				a[1] + (b[1] - a[1]) * t,
			];
		} else {
			return a + (b - a) * t;
		}
	}

	static min(a, b) {
		return [
			Math.min(a[0], b[0]),
			Math.min(a[1], b[1]),
		];
	}

	static max(a, b) {
		return [
			Math.max(a[0], b[0]),
			Math.max(a[1], b[1]),
		];
	}

	static distanceSq(a, b) {
		const dx = a[0] - b[0];
		const dy = a[1] - b[1];
		return dx * dx + dy * dy;
	}

	static distance(a, b) {
		return Math.sqrt(this.distanceSq(a, b));
	}

	static distanceToSegmentSq(p, v, w) {
		const l2 = this.distanceSq(v, w);
		if (l2 === 0) {
			return this.distanceSq(p, v);
		}
		let t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2;
		t = Math.max(0, Math.min(1, t));
		return this.distanceSq(p, this.lerp(v, w, t));
	}

	static distanceToSegment(p, v, w) {
		return Math.sqrt(distanceToSegmentSq(p, v, w));
	}
}

class m3 {
	static multiply(a, b) {
		const a00 = a[0];
		const a01 = a[1];
		const a02 = a[2];
		const a10 = a[3];
		const a11 = a[4];
		const a12 = a[5];
		const a20 = a[6];
		const a21 = a[7];
		const a22 = a[8];
		const b00 = b[0];
		const b01 = b[1];
		const b02 = b[2];
		const b10 = b[3];
		const b11 = b[4];
		const b12 = b[5];
		const b20 = b[6];
		const b21 = b[7];
		const b22 = b[8];
		return [
			b00 * a00 + b01 * a10 + b02 * a20,
			b00 * a01 + b01 * a11 + b02 * a21,
			b00 * a02 + b01 * a12 + b02 * a22,
			b10 * a00 + b11 * a10 + b12 * a20,
			b10 * a01 + b11 * a11 + b12 * a21,
			b10 * a02 + b11 * a12 + b12 * a22,
			b20 * a00 + b21 * a10 + b22 * a20,
			b20 * a01 + b21 * a11 + b22 * a21,
			b20 * a02 + b21 * a12 + b22 * a22,
		];
	}

	static identity() {
		return [
			1, 0, 0,
			0, 1, 0,
			0, 0, 1,
		];
	}

	static projection(w, h) {
		return [
			2 / w, 0, 0,
			0, -2 / h, 0,
			-1, 1, 1,
		];
	}

	static project(m, w, h) {
		return this.multiply(m, this.projection(w, h));
	}

	static translation(tx, ty) {
		return [
			1, 0, 0,
			0, 1, 0,
			tx, ty, 1,
		];
	}

	static translate(m, tx, ty) {
		return this.multiply(m, this.translation(tx, ty));
	}

	static rotation(r) {
		const c = Math.cos(r);
		const s = Math.sin(r);
		return [
			c, -s, 0,
			s, c, 0,
			0, 0, 1,
		];
	}

	static rotate(m, r) {
		return this.multiply(m, this.rotation(r));
	}

	static scaling(sx, sy) {
		return [
			sx, 0, 0,
			0, sy, 0,
			0, 0, 1,
		];
	}

	static scale(m, sx, sy) {
		return this.multiply(m, this.scaling(sx, sy));
	}

	static dot(x1, y1, x2, y2) {
		return x1 * x2 + y1 * y2;
	}

	static distance(x1, y1, x2, y2) {
		const dx = x1 - x2;
		const dy = y1 - y2;
		return Math.sqrt(dx * dx + dy * dy);
	}

	static normalize(x, y) {
		const l = this.distance(0, 0, x, y);
		if (l > 0) {
			return [x / l, y / l];
		} else {
			return [0, 0];
		}
	}

	static reflect(ix, iy, nx, ny) {
		const d = this.dot(nx, ny, ix, iy);
		return [
			ix - 2 * d * nx,
			iy - 2 * d * ny,
		];
	}

	static rad2deg(r) {
		return r * 180 / Math.PI;
	}

	static deg2rad(d) {
		return d * Math.PI / 180;
	}

	static transformPoint(m, v) {
		const v0 = v[0];
		const v1 = v[1];
		const d = v0 * m[2] + v1 * m[5] + m[8];
		return [
			(v0 * m[0] + v1 * m[3] + m[6]) / d,
			(v1 * m[1] + v1 * m[4] + m[7]) / d,
		];
	}

	static inverse(m) {
		const t00 = m[4] * m[8] - m[5] * m [7];
		const t10 = m[1] * m[8] - m[2] * m [7];
		const t20 = m[1] * m[5] - m[2] * m [4];
		const d = 1/ (m[0] * t00 - m[3] * t10 + m[6] * t20);
		return [
			d * t00, -d * t10, d * t20,
			-d * (m[3] * m[8] - m[5] * m[6]),
			d * (m[0] * m[8] - m[2] * m[6]),
			-d * (m[0] * m[5] - m[2] * m[3]),
			d * (m[3] * m[7] - m[4] * m[6]),
			-d * (m[0] * m[7] - m[1] * m[6]),
			d * (m[0] * m[4] - m[1] * m[3]),
		];
	}
};

class m4 {
	static multiply(a, b, res) {
		res = res || new Float32Array(16);
		const a00 = a[0];
		const a01 = a[1];
		const a02 = a[2];
		const a03 = a[3];
		const a10 = a[4];
		const a11 = a[5];
		const a12 = a[6];
		const a13 = a[7];
		const a20 = a[8];
		const a21 = a[9];
		const a22 = a[10];
		const a23 = a[11];
		const a30 = a[12];
		const a31 = a[13];
		const a32 = a[14];
		const a33 = a[15];
		const b00 = b[0];
		const b01 = b[1];
		const b02 = b[2];
		const b03 = b[3];
		const b10 = b[4];
		const b11 = b[5];
		const b12 = b[6];
		const b13 = b[7];
		const b20 = b[8];
		const b21 = b[9];
		const b22 = b[10];
		const b23 = b[11];
		const b30 = b[12];
		const b31 = b[13];
		const b32 = b[14];
		const b33 = b[15];
		res[0] = b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30;
		res[1] = b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31;
		res[2] = b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32;
		res[3] = b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33;
		res[4] = b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30;
		res[5] = b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31;
		res[6] = b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32;
		res[7] = b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33;
		res[8] = b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30;
		res[9] = b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31;
		res[10] = b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32;
		res[11] = b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33;
		res[12] = b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30;
		res[13] = b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31;
		res[14] = b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32;
		res[15] = b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33;
		return res;
	}

	static addVectors(a, b, res) {
		res = res || new Float32Array(3);
		res[0] = a[0] + b[0];
		res[1] = a[1] + b[1];
		res[2] = a[2] + b[2];
		return res;
	}

	static subtractVectors(a, b, res) {
		res = res || new Float32Array(3);
		res[0] = a[0] - b[0];
		res[1] = a[1] - b[1];
		res[2] = a[2] - b[2];
		return res;
	}

	static multiplyVector(v, f, res) {
		res = res || new Float32Array(3);
		res[0] = v[0] * f;
		res[1] = v[1] * f;
		res[2] = v[2] * f;
		return res;
	}

	static multiplyVectors(a, b, res) {
		res = res || new Float32Array(3);
		res[0] = a[0] * b[0];
		res[1] = a[1] * b[1];
		res[2] = a[2] * b[2];
		return res;
	}

	static normalize(v, res) {
		res = res || new Float32Array(3);
		const l = Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		if (l) {
			res[0] = v[0] / l;
			res[1] = v[1] / l;
			res[2] = v[2] / l;
		}
		return res;
	}

	static cross(a, b, res) {
		res = res || new Float32Array(3);
		res[0] = a[1] * b[2] - a[2] * b[1];
		res[1] = a[2] * b[0] - a[0] * b[2];
		res[2] = a[0] * b[1] - a[1] * b[0];
		return res;
	}

	static dot(a, b) {
		return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
	}

	static distanceSq(a, b) {
		const dx = a[0] - b[0];
		const dy = a[1] - b[1];
		const dz = a[2] - b[2];
		return dx * dx + dy * dy + dz * dz;
	}

	static distance(a, b) {
		return Math.sqrt(this.distanceSq(a, b));
	}

	static identity(res) {
		res = res || new Float32Array(16);
		res[0] = 1;
		res[1] = 0;
		res[2] = 0;
		res[3] = 0;
		res[4] = 0;
		res[5] = 1;
		res[6] = 0;
		res[7] = 0;
		res[8] = 0;
		res[9] = 0;
		res[10] = 1;
		res[11] = 0;
		res[12] = 0;
		res[13] = 0;
		res[14] = 0;
		res[15] = 1;
		return res;
	}

	static transpose(m, res) {
		res = res || new Float32Array(16);
		res[0] = m[0];
		res[1] = m[4];
		res[2] = m[8];
		res[3] = m[12];
		res[4] = m[1];
		res[5] = m[5];
		res[6] = m[9];
		res[7] = m[13];
		res[8] = m[2];
		res[9] = m[6];
		res[10] = m[10];
		res[11] = m[14];
		res[12] = m[3];
		res[13] = m[7];
		res[14] = m[11];
		res[15] = m[15];
		return res;
	}

	static lookAt(cameraPosition, target, up, res) {
		res = res || new Float32Array(16);
		const z = this.normalize(this.subtractVectors(cameraPosition, target));
		const x = this.normalize(this.cross(up, z));
		const y = this.normalize(this.cross(z, x));
		res[0] = x[0];
		res[1] = x[1];
		res[2] = x[2];
		res[3] = 0;
		res[4] = y[0];
		res[5] = y[1];
		res[6] = y[2];
		res[7] = 0;
		res[8] = z[0];
		res[9] = z[1];
		res[10] = z[2];
		res[11] = 0;
		res[12] = cameraPosition[0];
		res[13] = cameraPosition[1];
		res[14] = cameraPosition[2];
		res[15] = 1;
		return res;
	}

	static perspective(fieldOfViewInRadians, aspect, near, far, res) {
		res = res || new Float32Array(16);
		const f  = Math.tan(Math.PI * .5 - .5 * fieldOfViewInRadians);
		const rangeInv = 1 / (near - far);
		res[0] = f / aspect;
		res[1] = 0;
		res[2] = 0;
		res[3] = 0;
		res[4] = 0;
		res[5] = f;
		res[6] = 0;
		res[7] = 0;
		res[8] = 0;
		res[9] = 0;
		res[10] = (near + far) * rangeInv;
		res[11] = -1;
		res[12] = 0;
		res[13] = 0;
		res[14] = near * far * rangeInv * 2;
		res[15] = 0;
		return res;
	}

	static orthographic(left, right, bottom, top, near, far, res) {
		res = res || new Float32Array(16);
		res[0] = 2 / (right - left);
		res[1] = 0;
		res[2] = 0;
		res[3] = 0;
		res[4] = 0;
		res[5] = 2 / (top - bottom);
		res[6] = 0;
		res[7] = 0;
		res[8] = 0;
		res[9] = 0;
		res[10] = 2 / (far - near);
		res[11] = 0;
		res[12] = (left + right) / (left - right);
		res[13] = (bottom + top) / (bottom - top);
		res[14] = (near + far) / (near - far);
		res[15] = 1;
		return res;
	}

	static frustum(left, right, bottom, top, near, far, res) {
		res = res || new Float32Array(16);
		const dx = right - left;
		const dy = top - bottom;
		const dz = far - near;
		res[0] = 2 * near / dx;
		res[1] = 0;
		res[2] = 0;
		res[3] = 0;
		res[4] = 0;
		res[5] = 2 * near / dy;
		res[6] = 0;
		res[7] = 0;
		res[8] = (left + right) / dx;
		res[9] = (top + bottom) / dy;
		res[10] = -(far + near) / dz;
		res[11] = -1;
		res[12] = 0;
		res[13] = 0;
		res[14] = -2 * near * far / dz;
		res[15] = 0;
		return res;
	}

	static translation(tx, ty, tz, res) {
		res = res || new Float32Array(16);
		res[0] = 1;
		res[1] = 0;
		res[2] = 0;
		res[3] = 0;
		res[4] = 0;
		res[5] = 1;
		res[6] = 0;
		res[7] = 0;
		res[8] = 0;
		res[9] = 0;
		res[10] = 1;
		res[11] = 0;
		res[12] = tx;
		res[13] = ty;
		res[14] = tz;
		res[15] = 1;
		return res;
	}

	static translate(m, tx, ty, tz, res) {
		res = res || new Float32Array(16);
		const m00 = m[0];
		const m01 = m[1];
		const m02 = m[2];
		const m03 = m[3];
		const m10 = m[4];
		const m11 = m[5];
		const m12 = m[6];
		const m13 = m[7];
		const m20 = m[8];
		const m21 = m[9];
		const m22 = m[10];
		const m23 = m[11];
		const m30 = m[12];
		const m31 = m[13];
		const m32 = m[14];
		const m33 = m[15];
		if (m !== res) {
			res[0] = m00;
			res[1] = m01;
			res[2] = m02;
			res[3] = m03;
			res[4] = m10;
			res[5] = m11;
			res[6] = m12;
			res[7] = m13;
			res[8] = m20;
			res[9] = m21;
			res[10] = m22;
			res[11] = m23;
		}
		res[12] = m00 * tx + m10 * ty + m20 * tz + m30;
		res[13] = m01 * tx + m11 * ty + m21 * tz + m31;
		res[14] = m02 * tx + m12 * ty + m22 * tz + m32;
		res[15] = m03 * tx + m13 * ty + m23 * tz + m33;
		return res;
	}

	static xRotation(r, res) {
		res = res || new Float32Array(16);
		const c = Math.cos(r);
		const s = Math.sin(r);
		res[0] = 1;
		res[1] = 0;
		res[2] = 0;
		res[3] = 0;
		res[4] = 0;
		res[5] = c;
		res[6] = s;
		res[7] = 0;
		res[8] = 0;
		res[9] = -s;
		res[10] = c;
		res[11] = 0;
		res[12] = 0;
		res[13] = 0;
		res[14] = 0;
		res[15] = 1;
		return res;
	}

	static xRotate(m, r, res) {
		res = res || new Float32Array(16);
		const m00 = m[0];
		const m01 = m[1];
		const m02 = m[2];
		const m03 = m[3];
		const m10 = m[4];
		const m11 = m[5];
		const m12 = m[6];
		const m13 = m[7];
		const m20 = m[8];
		const m21 = m[9];
		const m22 = m[10];
		const m23 = m[11];
		const m30 = m[12];
		const m31 = m[13];
		const m32 = m[14];
		const m33 = m[15];
		const c = Math.cos(r);
		const s = Math.sin(r);
		if (m !== res) {
			res[0] = m00;
			res[1] = m01;
			res[2] = m02;
			res[3] = m03;
			res[12] = m30;
			res[13] = m31;
			res[14] = m32;
			res[15] = m33;
		}
		res[4] = c * m10 + s * m20;
		res[5] = c * m11 + s * m21;
		res[6] = c * m12 + s * m22;
		res[7] = c * m13 + s * m23;
		res[8] = c * m20 - s * m10;
		res[9] = c * m21 - s * m11;
		res[10] = c * m22 - s * m12;
		res[11] = c * m23 - s * m13;
		return res;
	}

	static yRotation(r, res) {
		res = res || new Float32Array(16);
		const c = Math.cos(r);
		const s = Math.sin(r);
		res[0] = c;
		res[1] = 0;
		res[2] = -s;
		res[3] = 0;
		res[4] = 0;
		res[5] = 1;
		res[6] = 0;
		res[7] = 0;
		res[8] = s;
		res[9] = 0;
		res[10] = c;
		res[11] = 0;
		res[12] = 0;
		res[13] = 0;
		res[14] = 0;
		res[15] = 1;
		return res;
	}

	static yRotate(m, r, res) {
		res = res || new Float32Array(16);
		const m00 = m[0];
		const m01 = m[1];
		const m02 = m[2];
		const m03 = m[3];
		const m10 = m[4];
		const m11 = m[5];
		const m12 = m[6];
		const m13 = m[7];
		const m20 = m[8];
		const m21 = m[9];
		const m22 = m[10];
		const m23 = m[11];
		const m30 = m[12];
		const m31 = m[13];
		const m32 = m[14];
		const m33 = m[15];
		const c = Math.cos(r);
		const s = Math.sin(r);
		if (m !== res) {
			res[4] = m10;
			res[5] = m11;
			res[6] = m12;
			res[7] = m13;
			res[12] = m30;
			res[13] = m31;
			res[14] = m32;
			res[15] = m33;
		}
		res[0] = c * m00 - s * m20;
		res[1] = c * m01 - s * m21;
		res[2] = c * m02 - s * m22;
		res[3] = c * m03 - s * m23;
		res[8] = c * m20 + s * m00;
		res[9] = c * m21 + s * m01;
		res[10] = c * m22 + s * m02;
		res[11] = c * m23 + s * m03;
		return res;
	}

	static zRotation(r, res) {
		res = res || new Float32Array(16);
		const c = Math.cos(r);
		const s = Math.sin(r);
		res[0] = c;
		res[1] = s;
		res[2] = 0;
		res[3] = 0;
		res[4] = -s;
		res[5] = c;
		res[6] = 0;
		res[7] = 0;
		res[8] = 0;
		res[9] = 0;
		res[10] = 1;
		res[11] = 0;
		res[12] = 0;
		res[13] = 0;
		res[14] = 0;
		res[15] = 1;
		return res;
	}

	static zRotate(m, r, res) {
		res = res || new Float32Array(16);
		const m00 = m[0];
		const m01 = m[1];
		const m02 = m[2];
		const m03 = m[3];
		const m10 = m[4];
		const m11 = m[5];
		const m12 = m[6];
		const m13 = m[7];
		const m20 = m[8];
		const m21 = m[9];
		const m22 = m[10];
		const m23 = m[11];
		const m30 = m[12];
		const m31 = m[13];
		const m32 = m[14];
		const m33 = m[15];
		const c = Math.cos(r);
		const s = Math.sin(r);
		if (m !== res) {
			res[8] = m20;
			res[9] = m21;
			res[10] = m22;
			res[11] = m23;
			res[12] = m30;
			res[13] = m31;
			res[14] = m32;
			res[15] = m33;
		}
		res[0] = c * m00 + s * m10;
		res[1] = c * m01 + s * m11;
		res[2] = c * m02 + s * m12;
		res[3] = c * m03 + s * m13;
		res[4] = c * m10 - s * m00;
		res[5] = c * m11 - s * m01;
		res[6] = c * m12 - s * m02;
		res[7] = c * m13 - s * m03;
		return res;
	}

	static axisRotation(m, axis, r, res) {
		res = res || new Float32Array(16);
		let x = axis[0];
		let y = axis[1];
		let z = axis[2];
		const n = Math.sqrt(x * x + y * y + z * z);
		x /= n;
		y /= n;
		z /= n;
		const xx = x * x;
		const yy = y * y;
		const zz = z * z;
		const c = Math.cos(r);
		const s = Math.sin(r);
		const oneMinC = 1 - c;

		res[0] = xx + (1 - xx) * c;
		res[1] = x * y * oneMinC + z * s;
		res[2] = x * z * oneMinC - y * s;
		res[3] = 0;
		res[4] = x * y * oneMinC - z * s;
		res[5] = yy + (1 - yy) * c;
		res[6] = y * z * oneMinC + x * s;
		res[7] = 0;
		res[8] = x * z * oneMinC + y * s;
		res[9] = y * z * oneMinC - x * s;
		res[10] = zz + (1 - zz) * c;
		res[11] = 0;
		res[12] = 0;
		res[13] = 0;
		res[14] = 0;
		res[15] = 1;
		return res;
	}

	static axisRotate(m, axis, r, res) {
		res = res || new Float32Array(16);
		let x = axis[0];
		let y = axis[1];
		let z = axis[2];
		const n = Math.sqrt(x * x + y * y + z * z);
		x /= n;
		y /= n;
		z /= n;
		const xx = x * x;
		const yy = y * y;
		const zz = z * z;
		const c = Math.cos(r);
		const s = Math.sin(r);
		const oneMinC = 1 - c;

		r00 = xx + (1 - xx) * c;
		r01 = x * y * oneMinC + z * s;
		r02 = x * z * oneMinC - y * s;
		r10 = x * y * oneMinC - z * s;
		r11 = yy + (1 - yy) * c;
		r12 = y * z * oneMinC + x * s;
		r20 = x * z * oneMinC + y * s;
		r21 = y * z * oneMinC - x * s;
		r22 = zz + (1 - zz) * c;

		const m00 = m[0];
		const m01 = m[1];
		const m02 = m[2];
		const m03 = m[3];
		const m10 = m[4];
		const m11 = m[5];
		const m12 = m[6];
		const m13 = m[7];
		const m20 = m[8];
		const m21 = m[9];
		const m22 = m[10];
		const m23 = m[11];
		const m30 = m[12];
		const m31 = m[13];
		const m32 = m[14];
		const m33 = m[15];
		if (m !== res) {
			res[12] = m30;
			res[13] = m31;
			res[14] = m32;
			res[15] = m33;
		}
		res[0] = r00 * m00 + r01 * m10 + r02 * m20;
		res[1] = r00 * m01 + r01 * m11 + r02 * m21;
		res[2] = r00 * m02 + r01 * m12 + r02 * m22;
		res[3] = r00 * m03 + r01 * m13 + r02 * m22;
		res[4] = r10 * m00 + r11 * m10 + r12 * m20;
		res[5] = r10 * m01 + r11 * m11 + r12 * m21;
		res[6] = r10 * m02 + r11 * m12 + r12 * m22;
		res[7] = r10 * m03 + r11 * m13 + r12 * m23;
		res[8] = r20 * m00 + r21 * m10 + r22 * m20;
		res[9] = r20 * m01 + r21 * m11 + r22 * m21;
		res[10] = r20 * m02 + r21 * m12 + r22 * m22;
		res[11] = r20 * m03 + r21 * m13 + r22 * m23;
		return res;
	}

	static scaling(sx, sy, sz, res) {
		res = res || new Float32Array(16);
		res[0] = sx;
		res[1] = 0;
		res[2] = 0;
		res[3] = 0;
		res[4] = 0;
		res[5] = sy;
		res[6] = 0;
		res[7] = 0;
		res[8] = 0;
		res[9] = 0;
		res[10] = sz;
		res[11] = 0;
		res[12] = 0;
		res[13] = 0;
		res[14] = 0;
		res[15] = 1;
		return res;
	}

	static scale(m, sx, sy, sz, res) {
		res = res || new Float32Array(16);
		const m00 = m[0];
		const m01 = m[1];
		const m02 = m[2];
		const m03 = m[3];
		const m10 = m[4];
		const m11 = m[5];
		const m12 = m[6];
		const m13 = m[7];
		const m20 = m[8];
		const m21 = m[9];
		const m22 = m[10];
		const m23 = m[11];
		const m30 = m[12];
		const m31 = m[13];
		const m32 = m[14];
		const m33 = m[15];
		if (m !== res) {
			res[12] = m30;
			res[13] = m31;
			res[14] = m32;
			res[15] = m33;
		}
		res[0] = sx * m00;
		res[1] = sx * m01;
		res[2] = sx * m02;
		res[3] = sx * m03;
		res[4] = sy * m10;
		res[5] = sy * m11;
		res[6] = sy * m12;
		res[7] = sy * m13;
		res[8] = sz * m20;
		res[9] = sz * m21;
		res[10] = sz * m22;
		res[11] = sz * m23;
		return res;
	}

	static inverse(m, res) {
		res = res || new Float32Array(16);
		const m00 = m[0];
		const m01 = m[1];
		const m02 = m[2];
		const m03 = m[3];
		const m10 = m[4];
		const m11 = m[5];
		const m12 = m[6];
		const m13 = m[7];
		const m20 = m[8];
		const m21 = m[9];
		const m22 = m[10];
		const m23 = m[11];
		const m30 = m[12];
		const m31 = m[13];
		const m32 = m[14];
		const m33 = m[15];

		const t00 = m22 * m33;
		const t01 = m32 * m23;
		const t02 = m12 * m33;
		const t03 = m32 * m13;
		const t04 = m12 * m23;
		const t05 = m22 * m13;
		const t06 = m02 * m33;
		const t07 = m32 * m03;
		const t08 = m02 * m23;
		const t09 = m22 * m03;
		const t10 = m02 * m13;
		const t11 = m12 * m03;
		const t12 = m20 * m31;
		const t13 = m30 * m21;
		const t14 = m10 * m31;
		const t15 = m30 * m11;
		const t16 = m10 * m21;
		const t17 = m20 * m11;
		const t18 = m00 * m31;
		const t19 = m30 * m01;
		const t20 = m00 * m21;
		const t21 = m20 * m01;
		const t22 = m00 * m11;
		const t23 = m10 * m01;

		const t0 = (t00 * m11 + t03 * m21 + t04 * m31) - (t01 * m11 + t02 * m21 + t05 * m31);
		const t1 = (t01 * m01 + t06 * m21 + t09 * m31) - (t00 * m01 + t07 * m21 + t08 * m31);
		const t2 = (t02 * m01 + t07 * m11 + t10 * m31) - (t03 * m01 + t06 * m11 + t11 * m31);
		const t3 = (t05 * m01 + t08 * m11 + t11 * m21) - (t04 * m01 + t09 * m11 + t10 * m21);

		const d = 1 / (m00 * t0 + m10 * t1 + m20 * t2 + m30 * t3);

		res[0] = d * t0;
		res[1] = d * t1;
		res[2] = d * t2;
		res[3] = d * t3;
		res[4] = d * ((t01 * m10 + t02 * m20 + t05 * m30) - (t00 * m10 + t03 * m20 + t04 * m30));
		res[5] = d * ((t00 * m00 + t07 * m20 + t08 * m30) - (t01 * m00 + t06 * m20 + t09 * m30));
		res[6] = d * ((t03 * m00 + t06 * m10 + t11 * m30) - (t02 * m00 + t07 * m10 + t10 * m30));
		res[7] = d * ((t04 * m00 + t09 * m10 + t10 * m20) - (t05 * m00 + t08 * m10 + t11 * m20));
		res[8] = d * ((t12 * m13 + t15 * m23 + t16 * m33) - (t13 * m13 + t14 * m23 + t17 * m33));
		res[9] = d * ((t13 * m03 + t18 * m23 + t21 * m33) - (t12 * m03 + t19 * m23 + t20 * m33));
		res[10] = d * ((t14 * m03 + t19 * m13 + t22 * m33) - (t15 * m03 + t18 * m13 + t23 * m33));
		res[11] = d * ((t17 * m03 + t20 * m13 + t23 * m23) - (t16 * m03 + t21 * m13 + t22 * m23));
		res[12] = d * ((t14 * m22 + t17 * m32 + t13 * m12) - (t16 * m32 + t12 * m12 + t15 * m22));
		res[13] = d * ((t20 * m32 + t12 * m02 + t19 * m22) - (t18 * m22 + t21 * m32 + t13 * m02));
		res[14] = d * ((t18 * m12 + t23 * m32 + t15 * m02) - (t22 * m32 + t14 * m02 + t19 * m12));
		res[15] = d * ((t22 * m22 + t16 * m02 + t21 * m12) - (t20 * m12 + t23 * m22 + t17 * m02));
		return res;
	}

	static transformVector(m, v, res) {
		res = res || new Float32Array(4);
		for (let i = 0; i < 4; ++i) {
			res[i] = 0;
			for (let j = 0; j < 4; ++j) {
				res[i] += v[j] * m[j * 4 + i];
			}
		}
		return res;
	}

	static transformPoint(m, v, res) {
		res = res || new Float32Array(3);
		const v0 = v[0];
		const v1 = v[1];
		const v2 = v[2];
		const d = v0 * m[3] + v1 * m[7] + v2 * m[11] + m[15];
		res[0] = (v0 * m[0] + v1 * m[4] + v2 * m[8] + m[12]) / d;
		res[1] = (v0 * m[1] + v1 * m[5] + v2 * m[9] + m[13]) / d;
		res[2] = (v0 * m[2] + v1 * m[6] + v2 * m[10] + m[14]) / d;
		return res;
	}

	static transformDirection(m, v, res) {
		res = res || new Float32Array(3);
		const v0 = v[0];
		const v1 = v[1];
		const v2 = v[2];
		res[0] = v0 * m[0] + v1 * m[4] + v2 * m[8];
		res[1] = v0 * m[1] + v1 * m[5] + v2 * m[9];
		res[2] = v0 * m[2] + v1 * m[6] + v2 * m[10];
		return res;
	}

	static transformNormal(m, v, res) {
		res = res || new Float32Array(3);
		const mi = this.inverse(m);
		const v0 = v[0];
		const v1 = v[1];
		const v2 = v[2];
		res[0] = v0 * mi[0] + v1 * mi[1] + v2 * mi[2];
		res[1] = v0 * mi[4] + v1 * mi[5] + v2 * mi[6];
		res[2] = v0 * mi[8] + v1 * mi[9] + v2 * mi[10];
		return res;
	}
};

