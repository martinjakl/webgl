// DrawImage
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_matrix;
uniform mat4 u_textureMatrix;
out vec2 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = (u_textureMatrix * vec4(a_texcoord, 0, 1)).xy;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;

void main() {
	if (v_texcoord.x < 0.0 || v_texcoord.y < 0.0 || v_texcoord.x > 1.0 || v_texcoord.y > 1.0) {
		discard;
	}
	outColor = texture(u_texture, v_texcoord);
}
`;

function drawImage(gl, vao, tex, texWidth, texHeigh, srcX, srcY, srcWidth, srcHeight, dstX, dstY, dstWidth, dstHeight, srcRotation) {
	if (dstX === undefined) {
		dstX = srcX;
		srcX = 0;
	}
	if (dstY === undefined) {
		dstY = srcY;
		srcY = 0;
	}
	if (srcWidth === undefined) {
		srcWidth = texWidth;
	}
	if (srcHeight === undefined) {
		srcHeight = texHeigh;
	}
	if (dstWidth === undefined) {
		dstWidth = srcWidth;
		srcWidth = texWidth;
	}
	if (dstHeight === undefined) {
		dstHeight = srcHeight;
		srcHeight = texHeigh;
	}
	if (srcRotation === undefined) {
		srcRotation = 0;
	}
	const matrix = m4.orthographic(0, gl.canvas.clientWidth, gl.canvas.clientHeight, 0, -1, 1);
	m4.translate(matrix, dstX, dstY, 0, matrix);
	m4.scale(matrix, dstWidth, dstHeight, 1, matrix);

	const texMatrix = m4.scaling(1 / texWidth, 1 / texHeigh, 1);
	m4.translate(texMatrix, texWidth * 0.5, texHeigh * 0.5, 0, texMatrix);
	m4.zRotate(texMatrix, srcRotation, texMatrix);
	m4.translate(texMatrix, texWidth * -0.5, texHeigh * -0.5, 0, texMatrix);
	m4.translate(texMatrix, srcX, srcY, 0, texMatrix);
	m4.scale(texMatrix, srcWidth, srcHeight, 1, texMatrix);

	gl.drawVAO(vao, {u_matrix: matrix, u_texture: tex, u_textureMatrix: texMatrix});
}

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", texcoord: "a_texcoord"}, Primitive.createXYQuad(1));
	const texs = [gl.loadTexture("../img/keyboard.jpg", undefined, undefined, true), gl.loadTexture("../img/pic1.jpg", undefined, undefined, true), gl.loadTexture("../img/pic2.jpg", undefined, undefined, true)];
	let then = 0;

	const drawInfos = [];
	const numToDraw = 9;
	const speed = 60;
	for (let i = 0; i < numToDraw; ++i) {
		const scale = Math.random() * 0.25 + 0.25;
		const drawInfo = {
			x: Math.random() * gl.canvas.width,
			y: Math.random() * gl.canvas.height,
			dx: Math.random() > 0.5 ? -1 : 1,
			dy: Math.random() > 0.5 ? -1 : 1,
			xScale: scale,
			yScale: scale,
			offX: 0,
			offY: 0,
			rotation: Math.random() * Math.PI * 2,
			deltaRotation: (0.5 + Math.random() * 0.5) * (Math.random() > 0.5 ? -1 : 1),
			width: 1,
			height: 1,
			textureInfo: texs[Math.random() * texs.length | 0]
		};
		drawInfos.push(drawInfo);
	}

	function update(deltaTime) {
		drawInfos.forEach(function(drawInfo) {
			drawInfo.x += drawInfo.dx * speed * deltaTime;
			drawInfo.y += drawInfo.dy * speed * deltaTime;
			if (drawInfo.x < 0) {
				drawInfo.dx = 1;
			}
			if (drawInfo.x >= gl.canvas.width) {
				drawInfo.dx = -1;
			}
			if (drawInfo.y < 0) {
				drawInfo.dy = 1;
			}
			if (drawInfo.y >= gl.canvas.height) {
				drawInfo.dy = -1;
			}
			drawInfo.rotation += drawInfo.deltaRotation * deltaTime;
		});
	}

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		update(deltaTime);
		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame(undefined, undefined, undefined, undefined, false);
		drawInfos.forEach(function(drawInfo) {
			const dstX = drawInfo.x;
			const dstY = drawInfo.y;
			const dstWidth = drawInfo.textureInfo.w * drawInfo.xScale;
			const dstHeight = drawInfo.textureInfo.h * drawInfo.yScale;
			const srcX = drawInfo.textureInfo.w * drawInfo.offX;
			const srcY = drawInfo.textureInfo.h * drawInfo.offY;
			const srcWidth = drawInfo.textureInfo.w * drawInfo.width;
			const srcHeight = drawInfo.textureInfo.h * drawInfo.height;
			drawImage(gl, vao, drawInfo.textureInfo.texture, drawInfo.textureInfo.w, drawInfo.textureInfo.h, srcX, srcY, srcWidth, srcHeight, dstX, dstY, dstWidth, dstHeight, drawInfo.rotation);
		});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
