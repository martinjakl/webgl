//Color based on position
function createShader(gl, type, source) {
	const shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	if (success) {
		return shader;
	}
	console.log(gl.getShaderInfoLog(shader));
	gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
	const program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	const success = gl.getProgramParameter(program, gl.LINK_STATUS);
	if (success) {
		return program;
	}
	console.log(gl.getProgramInfoLog(program));
	gl.deleteProgram(program);
}

function resize(canvas) {
	const displayWidth = canvas.clientWidth;
	const displayHeight = canvas.clientHeight;
	if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
		canvas.width = displayWidth;
		canvas.height = displayHeight;
	}
}

function randomInt(range) {
	return Math.floor(Math.random() * range);
}

function setGeometry(gl) {
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
		-150, -100,
		150, -100,
		-150, 100,
		150, -100,
		-150, 100,
		150, 100,
	]), gl.STATIC_DRAW);
}

function setColors(gl) {
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
		Math.random(), Math.random(), Math.random(), 1,
		Math.random(), Math.random(), Math.random(), 1,
		Math.random(), Math.random(), Math.random(), 1,
		Math.random(), Math.random(), Math.random(), 1,
		Math.random(), Math.random(), Math.random(), 1,
		Math.random(), Math.random(), Math.random(), 1,
	]), gl.STATIC_DRAW);
}

function start() {
	const canvas = document.getElementById("c");
	const gl = canvas.getContext("webgl2");
	if (gl) {
		const vertexShaderSource = `#version 300 es
		in vec2 a_position;
		in vec4 a_color;
		uniform mat3 u_matrix;
		out vec4 v_color;

		void main() {
			gl_Position = vec4((u_matrix * vec3(a_position, 1)).xy, 0, 1);
			v_color = a_color;
		}
		`;
		const fragmentShaderSource = `#version 300 es
		precision mediump float;
		in vec4 v_color;
		out vec4 outColor;
		void main() {
			outColor = v_color;
		}
		`;

		resize(gl.canvas);

		const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
		const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
		const program = createProgram(gl, vertexShader, fragmentShader);
		gl.useProgram(program);

		const positionAttributeLocation = gl.getAttribLocation(program, "a_position");
		const colorAttributeLocation = gl.getAttribLocation(program, "a_color");
		const matrixUniformLocation = gl.getUniformLocation(program, "u_matrix");

		let matrix = m3.projection(gl.canvas.clientWidth, gl.canvas.clientHeight);
		matrix = m3.translate(matrix, 200, 150);
		matrix = m3.rotate(matrix, 0);
		matrix = m3.scale(matrix, 1, 1);
		gl.uniformMatrix3fv(matrixUniformLocation, false, matrix);

		const vao = gl.createVertexArray();
		gl.bindVertexArray(vao);
		const positionBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		setGeometry(gl);
		gl.enableVertexAttribArray(positionAttributeLocation);
		let size = 2;
		let type = gl.FLOAT;
		let normalize = false;
		let stride = 0;
		let offset = 0;
		gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset);

		const colorBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
		setColors(gl);
		gl.enableVertexAttribArray(colorAttributeLocation);
		size = 4;
		type = gl.FLOAT;
		normalize = false;
		stride = 0;
		offset = 0;
		gl.vertexAttribPointer(colorAttributeLocation, size, type, normalize, stride, offset);

		gl.useProgram(program);
		gl.bindVertexArray(vao);
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
		gl.clearColor(0, 0, 0, 0);
		gl.clear(gl.COLOR_BUFFER_BIT);

		gl.drawArrays(gl.TRIANGLES, 0, 6);
	} else {
		console.log("No GL");
	}
}

document.addEventListener('DOMContentLoaded', start);
