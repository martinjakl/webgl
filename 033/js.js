// Spot lightning
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec3 a_normal;
uniform vec3 u_lightWorldPosition;
uniform vec3 u_viewWorldPosition;
uniform mat4 u_world;
uniform mat4 u_worldViewProjection;
uniform mat4 u_worldInverseTranspose;
out vec3 v_normal;
out vec3 v_surfaceToLight;
out vec3 v_surfaceToView;

void main() {
	gl_Position = u_worldViewProjection * a_position;
	v_normal = mat3(u_worldInverseTranspose) * a_normal;
	vec3 surfaceWorldPosition = (u_world * a_position).xyz;
	v_surfaceToLight = u_lightWorldPosition - surfaceWorldPosition;
	v_surfaceToView = u_viewWorldPosition - surfaceWorldPosition;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec3 v_normal;
in vec3 v_surfaceToLight;
in vec3 v_surfaceToView;
uniform vec4 u_color;
uniform float u_shininess;
uniform vec3 u_lightDirection;
uniform float u_innerLimit;
uniform float u_outerLimit;
out vec4 outColor;
void main() {
	vec3 normal = normalize(v_normal);
	vec3 surfaceToLightDirection = normalize(v_surfaceToLight);
	vec3 surfaceToViewDirection = normalize(v_surfaceToView);
	vec3 halfVector = normalize(surfaceToLightDirection + surfaceToViewDirection);
	float dotFromDirection = dot(surfaceToLightDirection, -u_lightDirection);
	float inLight = smoothstep(u_outerLimit, u_innerLimit, dotFromDirection);
	float light = inLight * dot(normal, surfaceToLightDirection);
	float specular = inLight * pow(dot(normal, halfVector), u_shininess);
	outColor = u_color;
	outColor.rgb *= light;
	outColor.rgb += specular;
}
`;

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", normal: "a_normal"}, Primitive.moveRotateScale(Primitive.createF(), [-25, -75, -15], [Math.PI, 0, 0], [1, 1, 1]));

	const fieldOfViewRadians = m3.deg2rad(60);
	let fRotation = 0;
	let lightRotX = 0;
	let lightRotY = 0;
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [100, 150, 200];
	const lightPosistion = [40, 60, 120];
	const target = [0, 35, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		fRotation += deltaTime * Math.PI / 4;
		lightRotX = Math.sin(now * Math.PI / 16) / 2;
		lightRotY = Math.sin(now * Math.PI / 6) / 4;

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);
		const worldMatrix = m4.yRotation(fRotation);
		const worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, worldMatrix);
		const worldInverseMatrix = m4.inverse(worldMatrix);
		const worldInverseTransposeMatrix = m4.transpose(worldInverseMatrix);
		const lmat = m4.lookAt(lightPosistion, target, up);
		m4.xRotate(lmat, lightRotX, lmat);
		m4.yRotate(lmat, lightRotY, lmat);
		gl.drawVAO(vao, {u_worldInverseTranspose: worldInverseTransposeMatrix, u_worldViewProjection: worldViewProjectionMatrix, u_world: worldMatrix, u_viewWorldPosition: cameraPosition, u_color: [0.2, 1, 0.2, 1], u_lightWorldPosition: lightPosistion, u_shininess: 150, u_innerLimit: Math.cos(m3.deg2rad(10)), u_outerLimit: Math.cos(m3.deg2rad(20)), u_lightDirection: [-lmat[8], -lmat[9], -lmat[10]]});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
