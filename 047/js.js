// Diffuse lightning
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec3 a_normal;
in vec2 a_texcoord;
uniform mat4 u_worldViewProjection;
uniform mat4 u_worldInverseTranspose;
out vec3 v_normal;
out vec2 v_texcoord;

void main() {
	gl_Position = u_worldViewProjection * a_position;
	v_normal = (u_worldInverseTranspose * vec4(a_normal, 0)).xyz;
	v_texcoord = a_texcoord;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec3 v_normal;
in vec2 v_texcoord;
uniform vec3 u_lightDirection;
uniform sampler2D u_texture;
uniform vec4 u_lightcolor;
uniform float u_ambient;
uniform mat4 u_world;
out vec4 outColor;
void main() {
	vec3 normal = normalize(v_normal);
	float light = dot(normal, -(normalize(u_world * vec4(u_lightDirection, 0)).xyz));
	outColor = texture(u_texture, v_texcoord) * u_lightcolor;
	outColor.rgb *= light * u_ambient;
}
`;

// function getColor() {
// 	return [parseFloat(document.getElementById("objcr").value), parseFloat(document.getElementById("objcg").value), parseFloat(document.getElementById("objcb").value), 1];
// }
// 
function getLightColor() {
	return [parseFloat(document.getElementById("ligcr").value), parseFloat(document.getElementById("ligcg").value), parseFloat(document.getElementById("ligcb").value), 1];
}

function getAmbient() {
	return parseFloat(document.getElementById("amb").value);
}

function draw(fpsNode, gl, canvas) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", normal: "a_normal", texcoord: "a_texcoord"}, Primitive.moveRotateScale(Primitive.createSpherifiedCube(15), [0, 0, 0], [0, 0, 0], [60, 60, 60]), "indices");
	const tex = gl.loadTexture("../img/uv-grid.png");

	const fieldOfViewRadians = m3.deg2rad(60);
	let yRotation = 0;
	let xRotation = 0;
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, 200];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;
	let autoAnim = true;
	let xmod = 0;
	let ymod = 0;

	const controls = new Controls(canvas, {
		mlc: function() {autoAnim = !autoAnim;},
		my: function(y) {ymod = y;},
		mx: function(x) {xmod = x;},
	}, true);

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		if (autoAnim) {
			yRotation += deltaTime * Math.PI / 4;
		} else {
			yRotation += deltaTime * xmod;
			xRotation += deltaTime * ymod;
		}
		if (yRotation > 2 * Math.PI) {
			yRotation = 0;
		}
		if (xRotation > 2 * Math.PI) {
			xRotation = 0;
		}

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);
		const worldMatrix = m4.xRotation(xRotation);
		m4.yRotate(worldMatrix, yRotation, worldMatrix);
		const worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, worldMatrix);
		const worldInverseMatrix = m4.inverse(worldMatrix);
		const worldInverseTransposeMatrix = m4.transpose(worldInverseMatrix);
		gl.drawVAO(vao, {u_worldInverseTranspose: worldInverseTransposeMatrix, u_worldViewProjection: worldViewProjectionMatrix, u_world: worldMatrix, u_texture: tex, u_lightcolor: getLightColor(), u_ambient: getAmbient(), u_lightDirection: [-0.5, -0.7, -1]});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl, canvas);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
