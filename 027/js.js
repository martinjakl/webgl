// Sampler Cube
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec3 a_texcoord;
uniform mat4 u_matrix;
out vec3 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = a_texcoord;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec3 v_texcoord;
uniform samplerCube u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", texcoordsc: "a_texcoord"}, Primitive.createCube(1));
	const texture = gl.loadSamplerCube(["../img/noodles-01.jpg", "../img/noodles-02.jpg", "../img/noodles-03.jpg", "../img/noodles-04.jpg", "../img/noodles-05.jpg", "../img/noodles-06.jpg"]);

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, 2];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let modelXRotation = 0;
	let modelYRotation = 0;
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		modelXRotation += -0.7 * deltaTime;
		modelYRotation += -0.4 * deltaTime;

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const matrix = m4.xRotate(viewProjectionMatrix, modelXRotation);
		m4.yRotate(matrix, modelYRotation, matrix);

		gl.drawVAO(vao, {u_matrix: matrix, u_texture: texture});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
