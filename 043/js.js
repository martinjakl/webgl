// Cylinder
"use strict";
const simpleVertexShader = `#version 300 es
in vec4 a_position;
in vec3 a_normal;
in vec2 a_texcoord;
uniform mat4 u_matrix;
out vec2 v_texcoord;
out vec3 v_normal;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = a_texcoord;
	v_normal = a_normal;
}
`;

const specularVertexShader = `#version 300 es
in vec4 a_position;
in vec3 a_normal;
uniform vec3 u_lightWorldPosition;
uniform vec3 u_viewWorldPosition;
uniform mat4 u_world;
uniform mat4 u_worldViewProjection;
uniform mat4 u_worldInverseTranspose;
out vec3 v_normal;
out vec3 v_surfaceToLight;
out vec3 v_surfaceToView;

void main() {
	gl_Position = u_worldViewProjection * a_position;
	v_normal = mat3(u_worldInverseTranspose) * a_normal;
	vec3 surfaceWorldPosition = (u_world * a_position).xyz;
	v_surfaceToLight = u_lightWorldPosition - surfaceWorldPosition;
	v_surfaceToView = u_viewWorldPosition - surfaceWorldPosition;
}
`;

const normalVertexShader = `#version 300 es
in vec4 a_position;
uniform mat4 u_matrix;

void main() {
	gl_Position = u_matrix * a_position;
}
`;

const normalShader = `#version 300 es
precision mediump float;
in vec3 v_normal;
out vec4 outColor;

void main() {
	outColor = vec4(v_normal * .5 + .5, 1);
}
`;

const normalLineShader = `#version 300 es
precision mediump float;
out vec4 outColor;

void main() {
	outColor = vec4(1.0, 0.0, 0.0, 1.0);
}
`;

const textureShader = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
in vec3 v_normal;
uniform sampler2D u_texture;
out vec4 outColor;

void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

const specularShader = `#version 300 es
#if GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
in vec3 v_normal;
in vec3 v_surfaceToLight;
in vec3 v_surfaceToView;
uniform vec4 u_color;
uniform float u_shininess;
out vec4 outColor;

void main() {
	vec3 normal = normalize(v_normal);
	vec3 surfaceToLightDirection = normalize(v_surfaceToLight);
	vec3 surfaceToViewDirection = normalize(v_surfaceToView);
	vec3 halfVector = normalize(surfaceToLightDirection + surfaceToViewDirection);

	float light = dot(normal, surfaceToLightDirection);
	float specular = 0.0;
	if (light > 0.0) {
		specular = pow(dot(normal, halfVector), u_shininess);
	}
	outColor = u_color;
	outColor.rgb *= light;
	outColor.rgb += specular;
}
`;

function draw(fpsNode, gl, canvas) {
	const programs = [gl.createProgramShaders(simpleVertexShader, textureShader),
		gl.createProgramShaders(simpleVertexShader, normalShader),
		gl.createProgramShaders(specularVertexShader, specularShader),
		gl.createProgramShaders(normalVertexShader, normalLineShader)];
	let prim1 = Primitive.createCylinder();
	prim1 = Primitive.moveRotateScale(prim1, [prim1.extents.max[0] * 1.5, 0, 0], [0, 0, 0], [1, 1, 1]);
	let prim2 = Primitive.createCylinder(.4);
	prim2 = Primitive.moveRotateScale(prim2, [prim2.extents.max[0] * -1.5, 0, 0], [0, 0, 0], [1, 1, 1]);

	function primToNorm(prim) {
		const pm = [];
		for (let i = 0; i < prim.position.data.length; i += 3) {
			pm.push(prim.position.data[i], prim.position.data[i + 1], prim.position.data[i + 2]);
			const v = m4.addVectors(prim.position.data.slice(i, i + 3), m4.multiplyVector(prim.normal.data.slice(i, i + 3), .2));
			pm.push(v[0], v[1], v[2]);
		}
		return pm;
	}
	const pm1 = primToNorm(prim1);
	const pm2 = primToNorm(prim2);
	const vaos = [];
	programs.forEach(p => {
		if (p !== programs[3]) {
			vaos.push(gl.createVAO(p, {position: "a_position", texcoord: "a_texcoord", normal: "a_normal"}, prim1, 'indices'),
				gl.createVAO(p, {position: "a_position", texcoord: "a_texcoord", normal: "a_normal"}, prim2, 'indices'))
		}
	});
	vaos.push(gl.createVAO(programs[3], {position: "a_position"}, {position: new pcInfo(new Float32Array(pm1), pm1.length / 3, 3, false, 0, 0)}),
	gl.createVAO(programs[3], {position: "a_position"}, {position: new pcInfo(new Float32Array(pm2), pm2.length / 3, 3, false, 0, 0)}));

	const tex = gl.loadTexture("../img/uv-grid.png");

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const midY = v2.lerp(prim1.extents.min[1], prim1.extents.max[1], .5);
	const sizeToFitOnScreen = (prim1.extents.max[1] - prim1.extents.min[1]) * 1.2;
	const distance = sizeToFitOnScreen / Math.tan(fieldOfViewRadians * .5);
	const cameraPosition = [0, midY, distance];
	const target = [0, midY, 0];
	const up = [0, -1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let modelXRotationRadians = 0;
	let modelYRotationRadians = 0;
	let then = 0;
	let mode = 0;
	let autoAnim = true;
	let xmod = 0;
	let ymod = 0;
	let xpmod = 0;
	let ypmod = 0;
	let lb = null;
	let showNormals = false;

	const controls = new Controls(canvas, {
		pb0: function(v, i) {if (v == 0 && lb === i) {autoAnim = !autoAnim; lb = null} if (v) {lb = i;}},
		ksp: function(v) {if (v == 0) {autoAnim = !autoAnim;}},
		mlc: function() {autoAnim = !autoAnim;},
		mmc: function() {if (++mode > 2) mode = 0;},
		mrc: function() {showNormals = !showNormals;},
		ply: function(y) {ypmod = y;},
		plx: function(x) {xpmod = x;},
		my: function(y) {ymod = y;},
		mx: function(x) {xmod = x;},
		tly: function(y) {ymod = y;},
		tlx: function(x) {xmod = x;},
		kup: function(v) {ymod = -v;},
		kdo: function(v) {ymod = v;},
		kri: function(v) {xmod = v;},
		kle: function(v) {xmod = -v;},
		preventdeftouch: function() {return !autoAnim;},
	}, true);

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);

		controls.updatePads();

		if (autoAnim) {
			modelXRotationRadians += deltaTime * 0.5;
			modelYRotationRadians += deltaTime * 0.7;
		} else {
			modelXRotationRadians += deltaTime * 0.5 * (ypmod ? ypmod : ymod);
			modelYRotationRadians += deltaTime * 0.7 * (xpmod ? xpmod : xmod);
		}
		modelXRotationRadians %= 2 * Math.PI;
		modelYRotationRadians %= 2 * Math.PI;

		gl.initFrame();

		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const matrix = m4.xRotation(modelXRotationRadians);
		m4.yRotate(matrix, modelYRotationRadians, matrix);
		const worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, matrix);

		let uniforms;
		if (mode == 2) {
			uniforms = {u_worldViewProjection: worldViewProjectionMatrix, u_world: matrix, u_worldInverseTranspose: m4.transpose(m4.inverse(matrix)), u_lightWorldPosition: [midY * 1.5, midY * 2, distance * 1.5], u_viewWorldPosition: cameraMatrix.slice(12, 15), u_color: [1, 0.8, 0.2, 1], u_shininess: 50};
		} else {
			uniforms = {u_matrix: worldViewProjectionMatrix, u_texture: tex};
		}

		gl.drawVAO(vaos[mode * 2], uniforms);
		gl.drawVAO(vaos[mode * 2 + 1], uniforms);

		if (showNormals) {
			gl.drawVAO(vaos[vaos.length - 2], {u_matrix: worldViewProjectionMatrix}, gl.PrimType.LINES);
			gl.drawVAO(vaos[vaos.length - 1], {u_matrix: worldViewProjectionMatrix}, gl.PrimType.LINES);
		}

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl, canvas);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
