// Text webgl glyph map
"use strict";
const fVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec4 a_color;
uniform mat4 u_matrix;
out vec4 v_color;

void main() {
	gl_Position = u_matrix * a_position;
	v_color = a_color;
}
`;

const fFragmentShaderSource = `#version 300 es
precision mediump float;
in vec4 v_color;
out vec4 outColor;
void main() {
	outColor = v_color;
}
`;

const textVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_textcoord;
uniform mat4 u_matrix;
out vec2 v_textcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_textcoord = a_textcoord;
}
`;

const textFragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_textcoord;
uniform sampler2D u_texture;
out vec4 outColor;

void main() {
	outColor = texture(u_texture, v_textcoord);
}
`;

const fontInfo = {
	letterHeight: 8,
	spaceWidth: 8,
	spacing: -1,
	textureWidth: 64,
	textureHeight: 40,
	glyphInfos: {
		'a': {x:  0, y:  0, width: 8},
		'b': {x:  8, y:  0, width: 8},
		'c': {x: 16, y:  0, width: 8},
		'd': {x: 24, y:  0, width: 8},
		'e': {x: 32, y:  0, width: 8},
		'f': {x: 40, y:  0, width: 8},
		'g': {x: 48, y:  0, width: 8},
		'h': {x: 56, y:  0, width: 8},
		'i': {x:  0, y:  8, width: 8},
		'j': {x:  8, y:  8, width: 8},
		'k': {x: 16, y:  8, width: 8},
		'l': {x: 24, y:  8, width: 8},
		'm': {x: 32, y:  8, width: 8},
		'n': {x: 40, y:  8, width: 8},
		'o': {x: 48, y:  8, width: 8},
		'p': {x: 56, y:  8, width: 8},
		'q': {x:  0, y: 16, width: 8},
		'r': {x:  8, y: 16, width: 8},
		's': {x: 16, y: 16, width: 8},
		't': {x: 24, y: 16, width: 8},
		'u': {x: 32, y: 16, width: 8},
		'v': {x: 40, y: 16, width: 8},
		'w': {x: 48, y: 16, width: 8},
		'x': {x: 56, y: 16, width: 8},
		'y': {x:  0, y: 24, width: 8},
		'z': {x:  8, y: 24, width: 8},
		'0': {x: 16, y: 24, width: 8},
		'1': {x: 24, y: 24, width: 8},
		'2': {x: 32, y: 24, width: 8},
		'3': {x: 40, y: 24, width: 8},
		'4': {x: 48, y: 24, width: 8},
		'5': {x: 56, y: 24, width: 8},
		'6': {x:  0, y: 32, width: 8},
		'7': {x:  8, y: 32, width: 8},
		'8': {x: 16, y: 32, width: 8},
		'9': {x: 24, y: 32, width: 8},
		'-': {x: 32, y: 32, width: 8},
		'*': {x: 40, y: 32, width: 8},
		'!': {x: 48, y: 32, width: 8},
		'?': {x: 56, y: 32, width: 8},
	},
};

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	const textCtx = document.createElement("canvas").getContext("2d");
	if (gl.gl && textCtx) {
		const fProgram = gl.createProgramShaders(fVertexShaderSource, fFragmentShaderSource);
		const textProgram = gl.createProgramShaders(textVertexShaderSource, textFragmentShaderSource);
		const fvao = gl.createVAO(fProgram, {position: "a_position", color: "a_color"}, Primitive.createF());

		const names = ["anna", "colin", "james", "danny", "kalin", "hiro", "eddie", "shu", "brian", "tami", "rick", "gene", "natalie", "evan", "sakura", "kai"];
		const glyphTex = gl.loadTexture("../img/8x8-font.png");

		const translation = [0, 30, 0];
		const rotation = [m3.deg2rad(190), 0, 0];
		const scale = [1, 1, 1];
		const fieldOfViewRadians = m3.deg2rad(60);
		const rotationSpeed = 1.2;
		let then = 0;

		requestAnimationFrame(drawScene);

		function drawScene(time) {
			const now = time * 0.001;
			const deltaTime = now - then;
			then = now;

			glUtils.resizeCanvas(gl.canvas);

			rotation[1] += rotationSpeed * deltaTime;

			gl.initFrame();

			const aspect = gl.clientWidth / gl.clientHeight;
			const zNear = 1;
			const zFar = 2000;
			const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

			const cameraRadius = 360;
			const cameraPosition = [Math.cos(now) * cameraRadius, 0, Math.sin(now) * cameraRadius];
			const target = [0, 0, 0];
			const up = [0, 1, 0];
			const cameraMatrix = m4.lookAt(cameraPosition, target, up);
			const viewMatrix = m4.inverse(cameraMatrix);

			const spread = 170;
			const textPositions = [];
			//First draw opaque
			gl.blendOff();
			for (let yy = -1; yy <= 1; ++yy) {
				for (let xx = -2; xx <= 2; ++xx) {
					const matrix = m4.translate(viewMatrix, translation[0] + xx * spread, translation[1] + yy * spread, translation[2]);
					m4.xRotate(matrix, rotation[0], matrix);
					m4.yRotate(matrix, rotation[1] + yy * xx * 0.2, matrix);
					m4.zRotate(matrix, rotation[2] + now + (yy * 3 * xx) * 0.1, matrix);
					m4.scale(matrix, scale[0], scale[1], scale[2], matrix);
					m4.translate(matrix, -50, -75, 0, matrix);
					textPositions.push([matrix[12], matrix[13], matrix[14]]);
					gl.drawVAO(fvao, {u_matrix: m4.multiply(projectionMatrix, matrix)});
				}
			}
			//Then draw transparent, ideally sorted by Z
			gl.blendOn();
			textPositions.forEach(function(pos, ndx) {
				const name = names[ndx];
				const s = name + ":" + pos[0].toFixed(0) + "," + pos[1].toFixed(0) + "," + pos[2].toFixed(0);
				const tvao = gl.createVAO(textProgram, {position: "a_position", texcoord: "a_textcoord"}, Primitive.createString(fontInfo, s));
				const fromEye = m4.normalize(pos);
				const amountToMoveTowardEye = 150;
				const viewX = pos[0] - fromEye[0] * amountToMoveTowardEye;
				const viewY = pos[1] - fromEye[1] * amountToMoveTowardEye;
				const viewZ = pos[2] - fromEye[2] * amountToMoveTowardEye;
				const desiredTextScale = -1 / gl.canvas.height;
				const scale = viewZ * desiredTextScale;
				const textMatrix = m4.translate(projectionMatrix, viewX, viewY, viewZ);
				m4.scale(textMatrix, scale, scale, 1, textMatrix);
				gl.drawVAO(tvao, {u_matrix: textMatrix, u_texture: glyphTex});
			});


			requestAnimationFrame(drawScene);
		}
	} else {
		console.log("No GL");
	}
}

document.addEventListener('DOMContentLoaded', start);
