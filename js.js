function mouseIn(e) {
	e.target.className = 'over';
}

function mouseOut(e) {
	e.target.className = '';
}

function start() {
	document.getElementsByName('item').forEach(function (e) {
		e.addEventListener('mouseover', mouseIn);
		e.addEventListener('mouseout', mouseOut);
	});
}

document.addEventListener('DOMContentLoaded', start);
