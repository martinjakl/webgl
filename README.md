# Experimenting with WebGL2

## Introduction

I've started learning WebGL2 and this repository maps my progress. Maybe it can inspire somebody. Also during that process I've created some libraries, that can be usefull and make using WebGL2 much easier.

## Cordova build
1. `cd webgl/Cordova/Pong`
2. `mkdir build`
3. `cd build`
4. `cordova create Pong`
5. `cd Pong`
6. `ln -sfn ../../../res/icon.png res/`
7. `ln -sfn ../../{config.xml,package.json} .`
8. `rm -R www/*`
9. `ln -sfn ../../../www/index.html www/`
10. `ln -sfn ../../../../../{lib,sound,028} www/`
11. `cordova platform add android`
12. `cordova plugin add cordova-plugin-nativeaudio`
13. `cordova build android`

## Thanks

I was inspired a lot by [WebGl2 Fundamentals](https://webgl2fundamentals.org/) (and in the beginning I've just rewritten examples from there).  
Algorithms for spheres creation came from [caosdoar](https://github.com/caosdoar/spheres/).  
Waving flag was inspired by [pepsigit](https://github.com/pepsigit/WebGL2).  
Guitar mesh created in [Blender](https://www.blender.org/) and following tutorial on [Planet Blender](http://planetblender.com/acousticguitar/)  
