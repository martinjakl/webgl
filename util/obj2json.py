#! /usr/bin/python3

import json
import sys

error = False
try:
    infile = sys.stdin if len(sys.argv) < 3 else open(sys.argv[2], 'r')
    outfile = sys.stdout if len(sys.argv) < 4 else open(sys.argv[3], 'w')
except:
    print("Can't open file.")
    error = True

if not error:
    mesh = '' if len(sys.argv) < 2 else sys.argv[1]
    pos = []
    tex = []
    norm = []
    ind = []
    npos = []
    nnorm = []
    ntex = []
    find = dict()
    skip = (len(mesh) > 0)
    for line in infile:
        l = line.strip().split(' ')
        l1 = []
        for ll in l:
            if len(ll):
                l1.append(ll)
        l = l1
        if len(l):
            if l[0] == 'o':
                if len(mesh): 
                    skip = (mesh != l[1])
            elif l[0] == 'v':
                pos.append([float(l[n]) for n in range(1, 4)])
            elif l[0] == 'vn':
                norm.append([float(l[n]) for n in range(1, 4)])
            elif l[0] == 'vt':
                tex.append([float(l[n]) for n in range(1, 3)])
            elif l[0] == 'f':
                if not skip:
                    l.pop(0)
                    indcurr = []
                    for f in l:
                        i = f.split('/')
                        p = int(i[0])
                        t = -1 if not len(i[1]) else int(i[1])
                        n = int(i[2])
                        if f in find:
                            indcurr.append(find[f])
                        else:
                            ndx = int(len(npos) / 3)
                            find[f] = ndx
                            indcurr.append(ndx)
                            npos.extend(pos[p - 1])
                            nnorm.extend(norm[n - 1])
                            if t != -1:
                                ntex.extend(tex[t - 1])
                    ind.append(indcurr)
    nind = []
    for i in ind:
        if len(i) == 3:
            for ic in i:
                nind.append(ic)
        else:
            nind.append(i[0])
            nind.append(i[1])
            nind.append(i[3])
            nind.append(i[3])
            nind.append(i[1])
            nind.append(i[2])
    result = dict()
    result['position'] = npos
    result['normal'] = nnorm
    if len(tex):
        result['texcoord'] = ntex
    result['indices'] = nind
    outfile.write(json.dumps(result))
