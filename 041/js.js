// Lathe
"use strict";
const fVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_matrix;
out vec2 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = a_texcoord;
}
`;

const fFragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

function draw(fpsNode, gl) {
	const fProgram = gl.createProgramShaders(fVertexShaderSource, fFragmentShaderSource);
	const prim = Primitive.createPin();
	const fvao = gl.createVAO(fProgram, {position: "a_position", texcoord: "a_texcoord"}, prim, 'indices');
	const fTex = gl.loadTexture("../img/uv-grid.png");

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const midY = v2.lerp(prim.extents.min[1], prim.extents.max[1], .5);
	const sizeToFitOnScreen = (prim.extents.max[1] - prim.extents.min[1]) * .6;
	const distance = sizeToFitOnScreen / Math.tan(fieldOfViewRadians * .5);
	const cameraPosition = [0, midY, distance];
	const target = [0, midY, 0];
	const up = [0, -1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let modelXRotationRadians = 0;
	let modelYRotationRadians = 0;
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);

		modelXRotationRadians += 1.2 * deltaTime;
		modelYRotationRadians += 0.7 * deltaTime;

		gl.initFrame();

		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const matrix = m4.xRotate(viewProjectionMatrix, modelXRotationRadians);
		m4.yRotate(matrix, modelYRotationRadians, matrix);
		gl.drawVAO(fvao, {u_matrix: matrix, u_texture: fTex});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
