/*
 * Copyright 2018 Martin Jakl (martin.jakl@macsnet.cz).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
"use strict";

class pcInfo {
	constructor(data, count, size, normalize, stride, offset) {
		this.data = data;
		this.count = count;
		this.size = size;
		this.normalize = normalize;
		this.stride = stride;
		this.offset = offset;
	}
}

class Primitive {
	static createF() {
		const positions =  new Float32Array([
			// left column front
			 0,   0,  0,
			 0, 150,  0,
			30,   0,  0,
			 0, 150,  0,
			30, 150,  0,
			30,   0,  0,

			// top rung front
			 30,   0,  0,
			 30,  30,  0,
			100,   0,  0,
			 30,  30,  0,
			100,  30,  0,
			100,   0,  0,

			// middle rung front
			30,  60,  0,
			30,  90,  0,
			67,  60,  0,
			30,  90,  0,
			67,  90,  0,
			67,  60,  0,

			// left column back
			 0,   0,  30,
			30,   0,  30,
			 0, 150,  30,
			 0, 150,  30,
			30,   0,  30,
			30, 150,  30,

			// top rung back
			 30,   0,  30,
			100,   0,  30,
			 30,  30,  30,
			 30,  30,  30,
			100,   0,  30,
			100,  30,  30,

			// middle rung back
			30,  60,  30,
			67,  60,  30,
			30,  90,  30,
			30,  90,  30,
			67,  60,  30,
			67,  90,  30,

			// top
			  0,   0,   0,
			100,   0,   0,
			100,   0,  30,
			  0,   0,   0,
			100,   0,  30,
			  0,   0,  30,

			// top rung right
			100,   0,   0,
			100,  30,   0,
			100,  30,  30,
			100,   0,   0,
			100,  30,  30,
			100,   0,  30,

			// under top rung
			 30,  30,   0,
			 30,  30,  30,
			100,  30,  30,
			 30,  30,   0,
			100,  30,  30,
			100,  30,   0,

			// between top rung and middle
			30,   30,   0,
			30,   60,  30,
			30,   30,  30,
			30,   30,   0,
			30,   60,   0,
			30,   60,  30,

			// top of middle rung
			30,   60,   0,
			67,   60,  30,
			30,   60,  30,
			30,   60,   0,
			67,   60,   0,
			67,   60,  30,

			// right of middle rung
			67,   60,   0,
			67,   90,  30,
			67,   60,  30,
			67,   60,   0,
			67,   90,   0,
			67,   90,  30,

			// bottom of middle rung.
			30,   90,   0,
			30,   90,  30,
			67,   90,  30,
			30,   90,   0,
			67,   90,  30,
			67,   90,   0,

			// right of bottom
			30,   90,   0,
			30,  150,  30,
			30,   90,  30,
			30,   90,   0,
			30,  150,   0,
			30,  150,  30,

			// bottom
			0,   150,   0,
			0,   150,  30,
			30,  150,  30,
			0,   150,   0,
			30,  150,  30,
			30,  150,   0,

			// left side
			0,   0,   0,
			0,   0,  30,
			0, 150,  30,
			0,   0,   0,
			0, 150,  30,
			0, 150,   0,
		]);

		const u8 = new Uint8Array(16 * 6 * 3);
		for (let i = 0; i < 16 * 6; ++i) {
			switch (Math.floor(i / 6)) {
				case 0:
				case 1:
				case 2:
					u8.set([200, 70, 120], i * 3);
					break;
				case 3:
				case 4:
				case 5:
					u8.set([80, 70, 200], i * 3);
					break;
				case 6:
					u8.set([70, 200, 210], i * 3);
					break;
				case 7:
					u8.set([200, 200, 70], i * 3);
					break;
				case 8:
					u8.set([210, 100, 70], i * 3);
					break;
				case 9:
					u8.set([210, 160, 70], i * 3);
					break;
				case 10:
					u8.set([70, 180, 210], i * 3);
					break;
				case 11:
					u8.set([100, 70, 210], i * 3);
					break;
				case 12:
					u8.set([76, 210, 100], i * 3);
					break;
				case 13:
					u8.set([140, 210, 80], i * 3);
					break;
				case 14:
					u8.set([90, 130, 110], i * 3);
					break;
				case 15:
					u8.set([160, 160, 220], i * 3);
					break;
			}
		}

		const tcoord = new Float32Array(16 * 6 * 2);
		for (let i = 0; i < 16; ++i) {
			switch (i) {
				case 0:
					tcoord.set([
						38 / 255, 44 / 255,
						38 / 255, 223 / 255,
						113 / 255, 44 / 255,
						38 / 255, 223 / 255,
						113 / 255, 223 / 255,
						113 / 255, 44 / 255,
					], i * 12);
					break;
				case 1:
					tcoord.set([
						113 / 255, 44 / 255,
						113 / 255, 85 / 255,
						218 / 255, 44 / 255,
						113 / 255, 85 / 255,
						218 / 255, 85 / 255,
						218 / 255, 44 / 255,
					], i * 12);
					break;
				case 2:
					tcoord.set([
						113 / 255, 112 / 255,
						113 / 255, 151 / 255,
						203 / 255, 112 / 255,
						113 / 255, 151 / 255,
						203 / 255, 151 / 255,
						203 / 255, 112 / 255,
					], i * 12);
					break;
				case 3:
					tcoord.set([
						38 / 255, 44 / 255,
						113 / 255, 44 / 255,
						38 / 255, 223 / 255,
						38 / 255, 223 / 255,
						113 / 255, 44 / 255,
						113 / 255, 223 / 255,
					], i * 12);
					break;
				case 4:
					tcoord.set([
						113 / 255, 44 / 255,
						218 / 255, 44 / 255,
						113 / 255, 85 / 255,
						113 / 255, 85 / 255,
						218 / 255, 44 / 255,
						218 / 255, 85 / 255,
					], i * 12);
					break;
				case 5:
					tcoord.set([
						113 / 255, 112 / 255,
						203 / 255, 112 / 255,
						113 / 255, 151 / 255,
						113 / 255, 151 / 255,
						203 / 255, 112 / 255,
						203 / 255, 151 / 255,
					], i * 12);
					break;
				case 6:
				case 7:
				case 8:
					tcoord.set([
						0, 0,
						1, 0,
						1, 1,
						0, 0,
						1, 1,
						0, 1,
					], i * 12);
					break;
				case 9:
				case 10:
				case 11:
				case 13:
					tcoord.set([
						0, 0,
						1, 1,
						0, 1,
						0, 0,
						1, 0,
						1, 1,
					], i * 12);
					break;
				case 12:
				case 14:
				case 15:
					tcoord.set([
						0, 0,
						0, 1,
						1, 1,
						0, 0,
						1, 1,
						1, 0,
					], i * 12);
					break;
			}
		}
		const tcoord2 = new Float32Array(16 * 6 * 2);
		for (let i = 0; i < 16; ++i) {
			switch (i) {
				case 0:
				case 1:
				case 2:
					tcoord2.set([
						0, 0,
						0, 1,
						1, 0,
						0, 1,
						1, 1,
						1, 0,
					], i * 12);
					break;
				case 3:
				case 4:
				case 5:
					tcoord2.set([
						0, 0,
						1, 0,
						0, 1,
						0, 1,
						1, 0,
						1, 1,
					], i * 12);
					break;
				case 6:
				case 7:
				case 8:
					tcoord2.set([
						0, 0,
						1, 0,
						1, 1,
						0, 0,
						1, 1,
						0, 1,
					], i * 12);
					break;
				case 9:
				case 10:
				case 11:
				case 13:
					tcoord2.set([
						0, 0,
						1, 1,
						0, 1,
						0, 0,
						1, 0,
						1, 1,
					], i * 12);
					break;
				case 12:
				case 14:
				case 15:
					tcoord2.set([
						0, 0,
						0, 1,
						1, 1,
						0, 0,
						1, 1,
						1, 0,
					], i * 12);
					break;
			}
		}

		const normal = new Float32Array(16 * 6 * 3);
		for (let i = 0; i < 16; ++i) {
			switch (i) {
				case 0:
				case 1:
				case 2:
					for (let j = 0; j < 6; ++j) {
						normal.set([0, 0, 1], i * 18 + j * 3);
					}
					break;
				case 3:
				case 4:
				case 5:
					for (let j = 0; j < 6; ++j) {
						normal.set([0, 0, -1], i * 18 + j * 3);
					}
					break;
				case 6:
				case 10:
					for (let j = 0; j < 6; ++j) {
						normal.set([0, 1, 0], i * 18 + j * 3);
					}
					break;
				case 7:
				case 9:
				case 11:
				case 13:
					for (let j = 0; j < 6; ++j) {
						normal.set([1, 0, 0], i * 18 + j * 3);
					}
					break;
				case 8:
				case 12:
				case 14:
					for (let j = 0; j < 6; ++j) {
						normal.set([0, -1, 0], i * 18 + j * 3);
					}
					break;
				case 15:
					for (let j = 0; j < 6; ++j) {
						normal.set([-1, 0, 0], i * 18 + j * 3);
					}
					break;
			}
		}

		const pInfo = new pcInfo(positions, 16 * 6, 3, false, 0, 0);
		const cInfo = new pcInfo(u8, 16 * 6, 3, true, 0, 0);
		const tInfo = new pcInfo(tcoord, 16 * 6, 2, true, 0, 0);
		const t2Info = new pcInfo(tcoord2, 16 * 6, 2, true, 0, 0);
		const normInfo = new pcInfo(normal, 16 * 6, 3, true, 0, 0);
		return {position: pInfo, color: cInfo, texcoord: tInfo, texcoord2: t2Info, normal: normInfo};
	}

	static createXYQuad(size) {
		const p = size / 2;
		const pos = new Float32Array([
			-p, -p, 0,
			p, -p, 0,
			-p, p, 0,
			-p, p, 0,
			p, -p, 0,
			p, p, 0,
		]);
		const tex = new Float32Array([
			0.0, 0.0,
			1.0, 0.0,
			0.0, 1.0,
			0.0, 1.0,
			1.0, 0.0,
			1.0, 1.0,
		]);
		const tex2 = new Float32Array([
			-3.0, -1.0,
			2.0, -1.0,
			-3.0, 4.0,
			-3.0, 4.0,
			2.0, -1.0,
			2.0, 4.0,
		]);
		return {
			position: new pcInfo(pos, 6, 3, false, 0, 0),
			texcoord: new pcInfo(tex, 6, 2, false, 0, 0),
			texcoord2: new pcInfo(tex2, 6, 2, false, 0, 0),
		};
	}

	static createPlane(size, zDepth) {
		const p = size / 2;
		const pos = new Float32Array([
			-p, p, -p,
			p, p, -p,
			-p, p, p,
			-p, p, p,
			p, p, -p,
			p, p, p,
		]);
		const tex = new Float32Array([
			0, 0,
			1, 0,
			0, zDepth,
			0, zDepth,
			1, 0,
			1, zDepth,
		]);
		return {
			position: new pcInfo(pos, 6, 3, false, 0, 0),
			texcoord: new pcInfo(tex, 6, 2, true, 0, 0),
		};
	}

	static createCube(size) {
		const p = size / 2;
		const pos = new Float32Array([
			-p, -p, -p,
			-p, p, -p,
			p, -p, -p,
			-p, p, -p,
			p, p, -p,
			p, -p, -p,

			-p, -p, p,
			p, -p, p,
			-p, p, p,
			-p, p, p,
			p, -p, p,
			p, p, p,

			-p, p, -p,
			-p, p, p,
			p, p, -p,
			-p, p, p,
			p, p, p,
			p, p, -p,

			-p, -p, -p,
			p, -p, -p,
			-p, -p, p,
			-p, -p, p,
			p, -p, -p,
			p, -p, p,

			-p, -p, -p,
			-p, -p, p,
			-p, p, -p,
			-p, -p, p,
			-p, p, p,
			-p, p, -p,

			p, -p, -p,
			p, p, -p,
			p, -p, p,
			p, -p, p,
			p, p, -p,
			p, p, p,
		]);
		const tex = new Float32Array([
			// select the bottom left image
			0, 0,
			0, 0.5,
			0.25, 0,
			0, 0.5,
			0.25, 0.5,
			0.25, 0,
			// select the bottom middle image
			0.25, 0,
			0.5, 0,
			0.25, 0.5,
			0.25, 0.5,
			0.5, 0,
			0.5, 0.5,
			// select to bottom right image
			0.5, 0,
			0.5, 0.5,
			0.75, 0,
			0.5, 0.5,
			0.75, 0.5,
			0.75, 0,
			// select the top left image
			0, 0.5,
			0.25, 0.5,
			0, 1,
			0, 1,
			0.25, 0.5,
			0.25, 1,
			// select the top middle image
			0.25, 0.5,
			0.25, 1,
			0.5, 0.5,
			0.25, 1,
			0.5, 1,
			0.5, 0.5,
			// select the top right image
			0.5, 0.5,
			0.75, 0.5,
			0.5, 1,
			0.5, 1,
			0.75, 0.5,
			0.75, 1,
		]);
		const col = new Float32Array(36 * 3);
		for (let i = 0; i < 6; ++i) {
			let c = [1, 1, 1];
			if (i == 1 || i == 3 || i == 0) {
				c = [0.5, 0.5, 0.5];
			}
			for (let j = 0; j < 6; ++j) {
				col.set(c, i * 18 + j * 3);
			}
		}
		const norm = new Float32Array(36 * 3);
		const n = [[0, 0, -1], [0, 0, 1], [0, 1, 0], [0, -1, 0], [-1, 0, 0], [1, 0, 0]];
		for (let i = 0; i < 6; ++i) {
			for (let j = 0; j < 6; ++j) {
				norm.set(n[i], i * 18 + j * 3);
			}
		}
		const tex2 = new Float32Array(36 * 2);
		for (let i = 0; i < 6; ++i) {
			const t = i % 2 ? [0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1] : [0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0];
			tex2.set(t, i * 12);
		}
		return {
			position: new pcInfo(pos, 36, 3, false, 0, 0),
			texcoord: new pcInfo(tex, 36, 2, true, 0, 0),
			texcoordsc: new pcInfo(pos, 36, 3, true, 0, 0),
			color: new pcInfo(col, 36, 3, false, 0, 0),
			normal: new pcInfo(norm, 36, 3, true, 0, 0),
			texcoord2: new pcInfo(tex2, 36, 2, true, 0, 0),
		};
	}

	static createString(fontInfo, s) {
		const len = s.length;
		const numVertices = len * 6;
		const positions = new Float32Array(numVertices * 2);
		const texcoords = new Float32Array(numVertices * 2);
		let offset = 0;
		let x = 0;
		const maxX = fontInfo.textureWidth;
		const maxY = fontInfo.textureHeight;
		for (let ii = 0; ii < len; ++ii) {
			const letter = s[ii];
			const glyphInfo = fontInfo.glyphInfos[letter];
			if (glyphInfo) {
				const x2 = x + glyphInfo.width;
				const u1 = glyphInfo.x / maxX;
				const v1 = (glyphInfo.y + fontInfo.letterHeight - 1) / maxY;
				const u2 = (glyphInfo.x + glyphInfo.width - 1) / maxX;
				const v2 = glyphInfo.y / maxY;

				positions[offset + 0] = x;
				positions[offset + 1] = 0;
				texcoords[offset + 0] = u1;
				texcoords[offset + 1] = v1;

				positions[offset + 2] = x2;
				positions[offset + 3] = 0;
				texcoords[offset + 2] = u2;
				texcoords[offset + 3] = v1;

				positions[offset + 4] = x;
				positions[offset + 5] = fontInfo.letterHeight;
				texcoords[offset + 4] = u1;
				texcoords[offset + 5] = v2;

				positions[offset + 6] = x;
				positions[offset + 7] = fontInfo.letterHeight;
				texcoords[offset + 6] = u1;
				texcoords[offset + 7] = v2;

				positions[offset + 8] = x2;
				positions[offset + 9] = 0;
				texcoords[offset + 8] = u2;
				texcoords[offset + 9] = v1;

				positions[offset + 10] = x2;
				positions[offset + 11] = fontInfo.letterHeight;
				texcoords[offset + 10] = u2;
				texcoords[offset + 11] = v2;

				x += glyphInfo.width + fontInfo.spacing;
				offset += 12;
			} else {
				x += fontInfo.spaceWidth;
			}
		}
		return {
			position: new pcInfo(positions, numVertices, 2, false, 0, 0),
			texcoord: new pcInfo(texcoords, numVertices, 2, false, 0, 0)};
	}

	static createMany(num, rows, radius, primitives, alternatePos, positionsPos) {
		const prim = {};
		for (let i in primitives[0]) {
			prim[i] = new pcInfo(new primitives[0][i].data.constructor(num * rows * primitives[0][i].data.length), primitives[0][i].count * num * rows, primitives[0][i].size, primitives[0][i].normalize, primitives[0][i].stride, primitives[0][i].offset);
		}
		let ypos = 0;
		const translations = [];
		const speeds = [];
		for (let ytr = -(rows - 1) / 2; ytr <= (rows - 1) / 2; ++ytr) {
			const y = ytr * 200;
			const mat = m4.translation(0, y, 0);
			for (let i = 0; i < num; ++i) {
				const angle = i * Math.PI * 2 / num;
				const x = Math.cos(angle) * radius;
				const z = Math.sin(angle) * radius;
				const matrix = m4.yRotation(Math.random() * 2 * Math.PI);
				const matrix2 = m4.translation(x, 0, z);
				const speed = (Math.random() * 12 * Math.PI) - (6 * Math.PI);
				for (let ii = 0; ii < primitives[0][positionsPos].data.length; ii += 3) {
					translations.push(x);
					translations.push(z);
					speeds.push(speed);
					let vector = m4.transformVector(mat, [primitives[0][positionsPos].data[ii + 0], primitives[0][positionsPos].data[ii + 1], primitives[0][positionsPos].data[ii + 2], 1]);
					vector = m4.transformVector(matrix, [vector[0], vector[1], vector[2], 1]);
					vector = m4.transformVector(matrix2, [vector[0], vector[1], vector[2], 1]);
					prim[positionsPos].data[primitives[0][positionsPos].data.length * num * ypos + i * primitives[0][positionsPos].data.length + ii + 0] = vector[0];
					prim[positionsPos].data[primitives[0][positionsPos].data.length * num * ypos + i * primitives[0][positionsPos].data.length + ii + 1] = vector[1];
					prim[positionsPos].data[primitives[0][positionsPos].data.length * num * ypos + i * primitives[0][positionsPos].data.length + ii + 2] = vector[2];
				}
				for (let j in prim) {
					if (j === positionsPos) {
						continue;
					}
					let p = null;
					if (j === alternatePos) {
						p = primitives[Math.floor(Math.random() * primitives.length)][j].data;
					} else {
						p = primitives[0][j].data;
					}
					prim[j].data.set(p, p.length * num * ypos + i * p.length);
				}
			}
			++ypos;
		}
		const transdata = new pcInfo(new Float32Array(translations), translations.length / 2, 2, false, 0, 0);
		const speeddata = new pcInfo(new Float32Array(speeds), speeds.length, 1, false, 0, 0);
		prim['translation'] = transdata;
		prim['speed'] = speeddata;
		return prim;
	}

	static createManyInstanced(num, rows, radius, primitive, altcount) {
		const prim = [];
		const translations = [];
		const speeds = [];
		const starts = [];
		const alts = [];
		for (let ytr = -(rows - 1) / 2; ytr <= (rows - 1) / 2; ++ytr) {
			const y = ytr * 200;
			for (let i = 0; i < num; ++i) {
				const alt = Math.floor(Math.random() * altcount);
				const angle = i * Math.PI * 2 / num;
				const x = Math.cos(angle) * radius;
				const z = Math.sin(angle) * radius;
				const start = Math.random() * 2 * Math.PI;
				const speed = (Math.random() * 12 * Math.PI) - (6 * Math.PI);
				translations.push(x, y, z);
				speeds.push(speed);
				starts.push(start);
				alts.push(alt);
			}
		}
		for (let i in primitive) {
			prim[i] = primitive[i];
		}
		const transdata = new pcInfo(new Float32Array(translations), translations.length / 3, 3, false, 0, 0);
		const speeddata = new pcInfo(new Float32Array(speeds), speeds.length, 1, false, 0, 0);
		const startdata = new pcInfo(new Float32Array(starts), starts.length, 1, false, 0, 0);
		const altdata = new pcInfo(new Uint8Array(alts), alts.length, 1, false, 0, 0);
		prim['translation'] = transdata;
		prim['speed'] = speeddata;
		prim['start'] = startdata;
		prim['alternate'] = altdata;
		return prim;
	}

	static moveRotateScale(primitive, move, rotate, scale) {
		let mt = m4.xRotation(rotate[0]);
		m4.yRotate(mt, rotate[1], mt);
		m4.zRotate(mt, rotate[2], mt);
		m4.scale(mt, scale[0], scale[1], scale[2], mt);
		m4.translate(mt, move[0], move[1], move[2], mt);
		for (let ii = 0; ii < primitive.position.data.length; ii += 3) {
			const vector = m4.transformVector(mt, [primitive.position.data[ii + 0], primitive.position.data[ii + 1], primitive.position.data[ii + 2], 1]);
			primitive.position.data[ii + 0] = vector[0];
			primitive.position.data[ii + 1] = vector[1];
			primitive.position.data[ii + 2] = vector[2];
		}
		return primitive;
	}

	static createUVSphere(meridians, parallels) {
		const triangles = [];
		const verticies = [0, 1, 0];
		for (let j = 0; j < parallels - 1; ++j) {
			const polar = Math.PI * (j + 1) / parallels;
			const sp = Math.sin(polar);
			const cp = Math.cos(polar);
			for (let i = 0; i < meridians; ++i) {
				const azimuth = 2 * Math.PI * i / meridians;
				const sa = Math.sin(azimuth);
				const ca = Math.cos(azimuth);
				const x = sp * ca;
				const y = cp;
				const z = sp * sa;
				verticies.push(x, y, z);
			}
		}
		verticies.push(0, -1, 0);
		for (let i = 0; i < meridians; ++i) {
			const a = i + 1;
			const b = (i + 1) % meridians + 1;
			triangles.push(0, b, a);
		}
		for (let j = 0; j < parallels - 2; ++j) {
			const aStart = j * meridians + 1;
			const bStart = (j + 1) * meridians + 1;
			for (let i = 0; i < meridians; ++i) {
				const a = aStart + i;
				const a1 = aStart + (i + 1) % meridians;
				const b = bStart + i;
				const b1 = bStart + (i + 1) % meridians;
				triangles.push(a, a1, b, a1, b1, b);
			}
		}
		for (let i = 0; i < meridians; ++i) {
			const a = i + meridians * (parallels - 2) + 1
			const b = (i + 1) % meridians + meridians * (parallels - 2) + 1;
			triangles.push(verticies.length / 3 - 1, a, b);
		}
		return {position: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, false, 0, 0),
			normal: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, true, 0, 0),
			indices: new pcInfo(new Uint32Array(triangles), triangles.length, 1, false, 0, 0)};
	}

	static getCubeToSphere() {
		return [[
			[ -1, -1, -1],
			[1, -1, -1],
			[1, -1, 1],
			[-1, -1, 1],
			[-1, 1, -1],
			[-1, -1, 1]
		],
		[
			[2, 0, 0],
			[0, 0, 2],
			[-2, 0, 0],
			[0, 0, -2],
			[2, 0, 0],
			[2, 0, 0]
		],
		[
			[0, 2, 0],
			[0, 2, 0],
			[0, 2, 0],
			[0, 2, 0],
			[0, 0, 2],
			[0, 0, -2]
		]];
	}

	static createNormalizedCube(division) {
		const [origins, rights, ups] = this.getCubeToSphere();
		const step = 1 / division;
		const verticies = [];
		for (let face = 0; face < 6; ++face) {
			const origin = origins[face];
			const right = rights[face];
			const up = ups[face];
			for (let j = 0; j < division + 1; ++j) {
				for (let i = 0; i < division + 1; ++i) {
					const p = m4.addVectors(origin, m4.multiplyVector(m4.addVectors(m4.multiplyVector(right, i), m4.multiplyVector(up, j)), step));
					const v = [];
					m4.normalize(p, v)
					verticies.push(v[0], v[1], v[2]);
				}
			}
		}
		const k = division + 1;
		const triangles = [];
		for (let face = 0; face < 6; ++face) {
			for (let j = 0; j < division; ++j) {
				for (let i = 0; i < division; ++i) {
					const a = (face * k + j) * k + i;
					const b = (face * k + j) * k + i + 1;
					const c = (face * k + j + 1) * k + i;
					const d = (face * k + j + 1) * k + i + 1;
					triangles.push(a, b, c, b, d, c);
				}
			}
		}
		return {position: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, false, 0, 0),
			normal: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, true, 0, 0),
			indices: new pcInfo(new Uint32Array(triangles), triangles.length, 1, false, 0, 0)};
	}

	static createSpherifiedCube(division) {
		const [origins, rights, ups] = this.getCubeToSphere();
		const step = 1 / division;
		const verticies = [];
		const uv = [];
		for (let face = 0; face < 6; ++face) {
			const origin = origins[face];
			const right = rights[face];
			const up = ups[face];
			for (let j = 0; j < division + 1; ++j) {
				for (let i = 0; i < division + 1; ++i) {
					const p = m4.addVectors(origin, m4.multiplyVector(m4.addVectors(m4.multiplyVector(right, i), m4.multiplyVector(up, j)), step));
					const p2 = m4.multiplyVectors(p, p);
					const v = [
						p[0] * Math.sqrt(1 - 0.5 * (p2[1] + p2[2]) + p2[1] * p2[2] / 3),
						p[1] * Math.sqrt(1 - 0.5 * (p2[2] + p2[0]) + p2[2] * p2[0] / 3),
						p[2] * Math.sqrt(1 - 0.5 * (p2[0] + p2[1]) + p2[0] * p2[1] / 3),
					];
					verticies.push(v[0], v[1], v[2]);
					uv.push(i / division, j / division);
				}
			}
		}
		const k = division + 1;
		const triangles = [];
		for (let face = 0; face < 6; ++face) {
			for (let j = 0; j < division; ++j) {
				for (let i = 0; i < division; ++i) {
					const a = (face * k + j) * k + i;
					const b = (face * k + j) * k + i + 1;
					const c = (face * k + j + 1) * k + i;
					const d = (face * k + j + 1) * k + i + 1;
					triangles.push(a, c, b, b, c, d);
				}
			}
		}
		return {position: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, false, 0, 0),
			normal: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, false, 0, 0),
			indices: new pcInfo(new Uint32Array(triangles), triangles.length, 1, false, 0, 0),
			texcoord: new pcInfo(new Float32Array(uv), uv.length / 2, 2, false, 0, 0),
		};
	}

	static subdivdeEdge(f0, f1, v0, v1, verticies, divisions) {
		const edge = f0 < f1 ? [f0, f1] : [f1, f0];
		const it = divisions[edge];
		if (it !== undefined) {
			return it;
		}
		const v = m4.normalize(m4.multiplyVector(m4.addVectors(v0, v1), 0.5));
		const f = verticies.length / 3;
		verticies.push(v[0], v[1], v[2]);
		divisions[edge] = f;
		return f;
	}

	static subdivideMesh(verticies, triangles) {
		const trianglesout = [];
		const divisions = {};
		for (let i = 0; i < triangles.length / 3; ++i) {
			const f0 = triangles[i * 3];
			const f1 = triangles[i * 3 + 1];
			const f2 = triangles[i * 3 + 2];
			const v0 = [verticies[f0 * 3], verticies[f0 * 3 + 1], verticies[f0 * 3 + 2]];
			const v1 = [verticies[f1 * 3], verticies[f1 * 3 + 1], verticies[f1 * 3 + 2]];
			const v2 = [verticies[f2 * 3], verticies[f2 * 3 + 1], verticies[f2 * 3 + 2]];
			const f3 = this.subdivdeEdge(f0, f1, v0, v1, verticies, divisions);
			const f4 = this.subdivdeEdge(f1, f2, v1, v2, verticies, divisions);
			const f5 = this.subdivdeEdge(f2, f0, v2, v0, verticies, divisions);
			trianglesout.push(f0, f3, f5);
			trianglesout.push(f3, f1, f4);
			trianglesout.push(f4, f2, f5);
			trianglesout.push(f3, f4, f5);
		}
		return [verticies, trianglesout];
	}

	static createIcosahedron(count) {
		const t = (1 + Math.sqrt(5)) / 2;
		let verticies = [];
		let v = m4.normalize([-1, t, 0]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([1, t, 0]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([-1, -t, 0]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([1, -t, 0]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([0, -1, t]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([0, 1, t]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([0, -1, -t]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([0, 1, -t]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([t, 0, -1]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([t, 0, 1]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([-t, 0, -1]);
		verticies.push(v[0], v[1], v[2]);
		v = m4.normalize([-t, 0, 1]);
		verticies.push(v[0], v[1], v[2]);

		let triangles = [];
		triangles.push(0, 11, 5);
		triangles.push(0, 5, 1);
		triangles.push(0, 1, 7);
		triangles.push(0, 7, 10);
		triangles.push(0, 10, 11);
		triangles.push(1, 5, 9);
		triangles.push(5, 11, 4);
		triangles.push(11, 10, 2);
		triangles.push(10, 7, 6);
		triangles.push(7, 1, 8);
		triangles.push(3, 9, 4);
		triangles.push(3, 4, 2);
		triangles.push(3, 2, 6);
		triangles.push(3, 6, 8);
		triangles.push(3, 8, 9);
		triangles.push(4, 9, 5);
		triangles.push(2, 4, 11);
		triangles.push(6, 2, 10);
		triangles.push(8, 6, 7);
		triangles.push(9, 8, 1);

		for (let i = 0; i < count; ++i) {
			[verticies, triangles] = this.subdivideMesh(verticies, triangles);
		}

		const uv = [];
		for (let i = 0; i < verticies.length; i += 3) {
			const u = 0.5 * (1.0 + Math.atan(verticies[i + 2], verticies[i + 0]) * (1.0 / Math.PI));
			const v = Math.acos(verticies[i + 1]) * (1.0 / Math.PI);
			uv.push(u, v);
		}

		return {position: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, false, 0, 0),
			normal: new pcInfo(new Float32Array(verticies), verticies.length / 3, 3, false, 0, 0),
			indices: new pcInfo(new Uint32Array(triangles), triangles.length, 1, false, 0, 0),
			texcoord: new pcInfo(new Float32Array(uv), uv.length / 2, 2, false, 0, 0),
		};
	}

	static addRandColor(prim, posind) {
		const colors = new Float32Array(prim[posind].count * 3);
		colors.forEach(function(c, ind, ar) {
			ar[ind] = Math.random();
		});
		prim['color'] = new pcInfo(colors, prim[posind].count, 3, false, 0, 0);
		return prim;
	}

	static getPointOnBezierCurve(point, offset, t) {
		const invT = 1 - t;
		return v2.add(v2.mult(points[offset + 0], invT * invT * invT),
			v2.mult(points[offset + 1], 3 * t * invT * invT),
			v2.mult(points[offset + 2], 3 * invT * t * t),
			v2.mult(points[offset + 3], t * t * t));
	}

	static getPointsOnBezierCurve(points, offset, numPoints) {
		const p = [];
		for (let i = 0; i < numPoints; ++i) {
			const t = i / (numPoints - 1);
			p.push(this.getPointOnBezierCurve(points, offset, t));
		}
		return p;
	}

	static flatness(points, offset) {
		const p1 = points[offset + 0];
		const p2 = points[offset + 1];
		const p3 = points[offset + 2];
		const p4 = points[offset + 3];

		let ux = 3 * p2[0] - 2 * p1[0] - p4[0]; ux *= ux;
		let uy = 3 * p2[1] - 2 * p1[1] - p4[1]; uy *= uy;
		let vx = 3 * p3[0] - 2 * p4[0] - p1[0]; vx *= vx;
		let vy = 3 * p3[1] - 2 * p4[1] - p1[1]; vy *= vy;

		if (ux < vx) {
			ux = vx;
		}

		if (uy < vy) {
			uy = vy;
		}

		return ux + uy;
	}

	static getPointsOnBezierCurveWithSplitting(points, offset, tolerance, newPoints) {
		const outPoints = newPoints || [];
		if (this.flatness(points, offset) < tolerance) {
			outPoints.push(points[offset + 0]);
			outPoints.push(points[offset + 3]);
		} else {
			const t = .5;
			const p1 = points[offset + 0];
			const p2 = points[offset + 1];
			const p3 = points[offset + 2];
			const p4 = points[offset + 3];

			const q1 = v2.lerp(p1, p2, t);
			const q2 = v2.lerp(p2, p3, t);
			const q3 = v2.lerp(p3, p4, t);

			const r1 = v2.lerp(q1, q2, t);
			const r2 = v2.lerp(q2, q3, t);

			const red = v2.lerp(r1, r2, t);

			this.getPointsOnBezierCurveWithSplitting([p1, q1, r1, red], 0, tolerance, outPoints);
			this.getPointsOnBezierCurveWithSplitting([red, r2, q3, p4], 0, tolerance, outPoints);
		}
		return outPoints;
	}

	static simplifyPoints(points, start, end, epsilon, newPoints) {
		const outPoints = newPoints || [];

		const s = points[start];
		const e = points[end - 1];
		let maxDistSq = 0;
		let maxNdx = 1;
		for (let i = start + 1; i < end; ++i) {
			const distSq = v2.distanceToSegmentSq(points[i], s, e);
			if (distSq > maxDistSq) {
				maxDistSq = distSq;
				maxNdx = i;
			}
		}

		if (Math.sqrt(maxDistSq) > epsilon) {
			this.simplifyPoints(points, start, maxNdx + 1, epsilon, outPoints);
			this.simplifyPoints(points, maxNdx, end, epsilon, outPoints);
		} else {
			outPoints.push(s, e);
		}
		return outPoints;
	}

	static getPointsOnBezierCurves(points, tolerance) {
		const newPoints = [];
		const numSegments = (points.length - 1) / 3;
		for (let i = 0; i < numSegments; ++i) {
			const offset = i * 3;
			this.getPointsOnBezierCurveWithSplitting(points, offset, tolerance, newPoints);
		}
		return newPoints;
	}

	static lathePoints(points, startAngle, endAngle, numDivisions, capStart, capEnd) {
		const positions = [];
		const texcoords = [];
		const indices = [];

		const vOffset = capStart ? 1 : 0;
		const pointsPerColumns = points.length + vOffset + (capEnd ? 1 : 0);
		const quadsDown = pointsPerColumns - 1;

		let vcoords = [];
		let length = 0;
		for (let i = 0; i < points.length - 1; ++i) {
			vcoords.push(length);
			length += v2.distance(points[i], points[i + 1]);
		}
		vcoords.push(length);
		vcoords = vcoords.map(v => v /length);

		for (let division = 0; division <= numDivisions; ++division) {
			const u = division / numDivisions;
			const angle = v2.lerp(startAngle, endAngle, u) % (Math.PI * 2);
			const mat = m4.yRotation(angle);
			if (capStart) {
				positions.push(0, points[0][1], 0);
				texcoords.push(u, 0);
			}
			points.forEach(function(p, ndx) {
				const tp = m4.transformPoint(mat, [...p, 0]);
				positions.push(tp[0], tp[1], tp[2]);
				texcoords.push(u, vcoords[ndx]);
			});
			if (capEnd) {
				positions.push(0, points[points.length - 1][1], 0);
				texcoords.push(u, 1);
			}
		}

		for (let division = 0; division < numDivisions; ++division) {
			const column1Offset = division * pointsPerColumns;
			const column2Offset = column1Offset + pointsPerColumns;
			for (let quad = 0; quad < quadsDown; ++quad) {
				indices.push(column1Offset + quad, column1Offset + quad + 1, column2Offset + quad);
				indices.push(column1Offset + quad + 1, column2Offset + quad + 1, column2Offset + quad);
			}
		}
		return {position: positions, texcoord: texcoords, indices: indices};
	}

	static parseSVGPath(svg, opt) {
		const points = [];
		let delta = false;
		let keepNext = false;
		let need = 0;
		let value = '';
		let values = [];
		let lastValues = [0, 0];
		let nextLastValues = [0, 0];
		let mode;

		function addValue() {
			if (value.length > 0) {
				values.push(parseFloat(value));
				if (values.length === 2) {
					if (delta) {
						values[0] += lastValues[0];
						values[1] += lastValues[1];
					}
					points.push(values);
					if (keepNext) {
						nextLastValues = values.slice();
					}
					--need;
					if (!need) {
						if (mode === 'l') {
							const m4 = points.pop();
							const m1 = points.pop();
							const m2 = v2.lerp(m1, m4, 0.25);
							const m3 = v2.lerp(m1, m4, 0.75);
							points.push(m1, m2, m3, m4);
						}
						lastValues = nextLastValues;
					}
					values = [];
				}
				value = '';
			}
		}

		svg.split('').forEach(function(c) {
			if ((c >= '0' && c <= '9') || c === '.') {
				value += c;
			} else if (c === '-') {
				addValue();
				value = '-';
			} else if (c === 'm') {
				addValue();
				keepNext = true;
				need = 1;
				delta = true;
				mode = 'm';
			} else if (c === 'c') {
				addValue();
				keepNext = true;
				need = 3;
				delta = true;
				mode = 'c';
			} else if (c === 'l') {
				addValue();
				keepNext = true;
				need = 1;
				delta = true;
				mode = 'l';
			} else if (c === 'M') {
				addValue();
				keepNext = true;
				need = 1;
				delta = false;
				mode = 'm';
			} else if (c === 'C') {
				addValue();
				keepNext = true;
				need = 3;
				delta = false;
				mode = 'c';
			} else if (c === 'L') {
				addValue();
				keepNext = true;
				need = 1;
				delta = false;
				mode = 'l';
			} else if (c === 'Z') {
			} else if (c === ',') {
				addValue();
			} else if (c === ' ') {
				addValue();
			} else {
				throw ("unsupported path option");
			}
		});
		addValue();
		let min = points[0].slice();
		let max = points[0].slice();
		for (let i = 1; i < points.length; ++i) {
			min = v2.min(min, points[i]);
			max = v2.max(max, points[i]);
		}
		const range = v2.sub(max, min);
		const halfRange = v2.mult(range, .5);
		for (let i = 0; i < points.length; ++i) {
			let p = points[i];
			if (opt.xFlip) {
				p[0] = max[0] - p[0];
			} else {
				p[0] = p[0] - min[0];
			}
			p[1] = (p[1] - min[0]) - halfRange[1];
		}
		return points;
	}

	static getExtents(positions) {
		const min = positions.slice(0, 3);
		const max = positions.slice(0, 3);
		for (let i = 3; i < positions.length; i += 3) {
			min[0] = Math.min(positions[i + 0], min[0]);
			min[1] = Math.min(positions[i + 1], min[1]);
			min[2] = Math.min(positions[i + 2], min[2]);
			max[0] = Math.max(positions[i + 0], max[0]);
			max[1] = Math.max(positions[i + 1], max[1]);
			max[2] = Math.max(positions[i + 2], max[2]);
		}
		return {min: min, max: max};
	}

	static makeIndexedIndicesFn(arrays) {
		const indices = arrays.indices;
		let ndx = 0;
		const fn = function() {
			return indices[ndx++];
		};
		fn.reset = function() {
			ndx = 0;
		}
		fn.numElements = indices.length;
		return fn;
	}

	static makeUnindexedIndicesFn(arrays) {
		let ndx = 0;
		const fn = function() {
			return ndx++;
		}
		fn.reset = function() {
			ndx = 0;
		}
		fn.numElements = arrays.position.length / 3;
		return fn;
	}

	static makeIndiceIterator(arrays) {
		return arrays.indices ? this.makeIndexedIndicesFn(arrays) : this.makeUnindexedIndicesFn(arrays);
	}

	static checkFaceOrientation(pos, norm, ind) {
		if (!(ind.length % 3)) {
			const tmp = ind[ind.length - 2];
			ind[ind.length - 2] = ind[ind.length -1];
			ind[ind.length - 1] = tmp;
		}
	}

	static generateNormals(arrays, maxAngle, invertNormals) {
		const positions = arrays.position;
		const texcoords = arrays.texcoord;

		const getNextIndex = this.makeIndiceIterator(arrays);
		const numFaceVerts = getNextIndex.numElements;
		const numVerts = arrays.position.length;
		const numFaces = numFaceVerts / 3;
		const faceNormals = [];

		for (let i = 0; i < numFaces; ++i) {
			const n1 = getNextIndex() * 3;
			const n2 = getNextIndex() * 3;
			const n3 = getNextIndex() * 3;

			const v1 = positions.slice(n1, n1 + 3);
			const v2 = positions.slice(n2, n2 + 3);
			const v3 = positions.slice(n3, n3 + 3);

			faceNormals.push(m4.normalize(m4.cross(m4.subtractVectors(v1, v2), m4.subtractVectors(v3, v2))));
		}

		let tempVerts = {};
		let temVertNdx = 0;

		function getVertIndex(x, y, z) {
			const vertId = x + "," + y + "," + z;
			const ndx = tempVerts[vertId];
			if (ndx !== undefined) {
				return ndx;
			}
			const newNdx = temVertNdx++;
			tempVerts[vertId] = newNdx;
			return newNdx;
		}

		const vertIndices = [];
		for (let i = 0; i < numVerts;  ++i) {
			const offset = i * 3;
			const vert = positions.slice(offset, offset + 3);
			vertIndices.push(getVertIndex(vert));
		}

		const vertFaces = [];
		getNextIndex.reset();
		for (let i = 0; i < numFaces; ++i) {
			for (let j = 0; j < 3; ++j) {
				const ndx = getNextIndex();
				const shareNdx = vertIndices[ndx];
				let faces = vertFaces[shareNdx];
				if (!faces) {
					faces = [];
					vertFaces[shareNdx] = faces;
				}
				faces.push(i);
			}
		}

		tempVerts = {};
		temVertNdx = 0;
		const newPositions = [];
		const newTexcoords = [];
		const newNormals = [];

		function getNewVertIndex(x, y, z, nx, ny, nz, u, v) {
			const vertId = x + "," + y + "," + z + "," +
				nx + "," + ny + "," + nz + "," +
				u + "," + v;
			const ndx = tempVerts[vertId];
			if (ndx !== undefined) {
				return ndx;
			}
			const newNdx = temVertNdx++;
			tempVerts[vertId] = newNdx;
			newPositions.push(x, y, z);
			if (invertNormals) {
				let v = [nx, ny, nz];
				v = m4.multiplyVector(v, -1);
				newNormals.push(v[0], v[1], v[2]);
			} else {
				newNormals.push(nx, ny, nz);
			}
			newTexcoords.push(u, v);
			return newNdx;
		}

		const newVertIndices = [];
		getNextIndex.reset();
		const maxAngleCos = Math.cos(maxAngle);
		for (let i = 0; i < numFaces; ++i) {
			const thisFaceNormal = faceNormals[i];
			for (let j = 0; j < 3; ++j) {
				const ndx = getNextIndex();
				const shareNdx = vertIndices[ndx];
				const faces = vertFaces[shareNdx];
				const norm = [0, 0, 0];
				faces.forEach(faceNdx => {
					const otherFaceNormal = faceNormals[faceNdx];
					const dot = m4.dot(thisFaceNormal, otherFaceNormal);
					if (dot > maxAngleCos) {
						m4.addVectors(norm, otherFaceNormal, norm);
					}
				});
				m4.normalize(norm, norm);
				const poffset = ndx * 3;
				const toffset = ndx * 2;
				newVertIndices.push(getNewVertIndex(positions[poffset + 0], positions[poffset + 1], positions[poffset + 2], norm[0], norm[1], norm[2], texcoords[toffset + 0], texcoords[toffset + 1]));
				if (!invertNormals) {
					this.checkFaceOrientation(newPositions, newNormals, newVertIndices);
				}
			}
		}
		return {
			position: newPositions,
			texcoord: newTexcoords,
			normal: newNormals,
			indices: newVertIndices,
		};
	}

	static createLatheFromPoints(points, divisions, startAngle, endAngle, capStart, capEnd, invertNormals) {
		let arrays = this.lathePoints(points, startAngle, endAngle, divisions, capStart, capEnd);
		arrays = this.generateNormals(arrays, m3.deg2rad(30), invertNormals);
		return {
			position: new pcInfo(new Float32Array(arrays.position), arrays.position.length / 3, 3, false, 0, 0),
			texcoord: new pcInfo(new Float32Array(arrays.texcoord), arrays.texcoord.length / 2, 2, true, 0, 0),
			normal: new pcInfo(new Float32Array(arrays.normal), arrays.normal.length / 3, 3, true, 0, 0),
			indices: new pcInfo(new Uint32Array(arrays.indices), arrays.indices.length, 1, false, 0, 0),
			extents: this.getExtents(arrays.position),
		};
	}

	static createLatheFromSVG(svg, tolerance, distance, divisions, startAngle, endAngle, capStart, capEnd, svgopt, invertNormals) {
		const svgPoints = this.parseSVGPath(svg, svgopt);
		const tempPoints = this.getPointsOnBezierCurves(svgPoints, tolerance);
		const points = this.simplifyPoints(tempPoints, 0, tempPoints.length, distance);
		return this.createLatheFromPoints(points, divisions, startAngle, endAngle, capStart, capEnd, invertNormals);
	}

	static createPin() {
		return this.createLatheFromSVG('m44,434c18,-33 19,-66 15,-111c-4,-45 -37,-104 -39,-132c-2,-28 11,-51 16,-81c5,-30 3,-63 -36,-63', 0.15, .4, 16, 0, Math.PI * 2, true, true, {}, true);
	}

	static createCandleHolder() {
		return this.createLatheFromSVG("M236,124L197,112L197,34C197,34 184.859,31.871 165,33C186.997,66.892 161.894,89.627 173,109C184.106,128.373 186.493,137.68 205,144C219.37,148.907 222,154 222,154L220,175L202,174L191,194L204,209L222,208C222,208 226.476,278.566 218,295C209.524,311.434 191.013,324.945 201,354C210.987,383.055 213,399 213,399L191,403L191,417L212,422C212,422 233.283,437.511 211,444C188.717,450.489 111,472 111,472L111,485L236,484L236,124Z", 0.15, .4, 16, 0, Math.PI * 2, true, true, {xFlip: true}, false);
	}

	static createCylinder(inner) {
		let points;
		if (inner === undefined) {
			points = [[0, 1], [1, 1], [1, -1], [0, -1]];
		} else {
			points = [[inner, 1], [1, 1], [1, -1], [inner, -1], [inner, 1]];
		}
		return this.createLatheFromPoints(points, 32, 0, 2 * Math.PI, false, false, true);
	}

	static createRectangle(divX, divY) {
		const pos = [];
		const tex = [];
		const norm = [];
		const ind = [];
		for (let i = 0; i <= divX; ++i) {
			for (let j = 0; j <= divY; ++j) {
				norm.push(0, 0, -1);
				tex.push(1 - i / divX, 1 - j / divY);
				pos.push(i, j, 0);
			}
		}
		for (let i = 0; i < divX; ++i) {
			for (let j = 0; j < divY; ++j) {
				const o = i * (divY + 1);
				const o1 = o + divY + 1;
				ind.push(o + j, o + j + 1, o1 + j, o1 + j, o + j + 1, o1 + j + 1);
			}
		}
		return {
			position: new pcInfo(new Float32Array(pos), pos.length / 3, 3, false, 0, 0),
			texcoord: new pcInfo(new Float32Array(tex), tex.length / 2, 2, true, 0, 0),
			normal: new pcInfo(new Float32Array(norm), norm.length / 3, 3, true, 0, 0),
			indices: new pcInfo(pos.length < 16384 ? new Uint16Array(ind) : new Uint32Array(ind), ind.length, 1, false, 0, 0),
		};
	}

	static addTangent(prim, texpos, pospos, indpos) {
		const ind = prim[indpos].data;
		const tex = prim[texpos].data;
		const pos = prim[pospos].data;
		const tang = new Float32Array(pos.length);
		for (let i = 0; i < ind.length; i += 3) {
			const v0t = tex.slice(ind[i] * 2, ind[i] * 2 + 2);
			const v1t = tex.slice(ind[i + 1] * 2, ind[i + 1] * 2 + 2);
			const v2t = tex.slice(ind[i + 2] * 2, ind[i + 2] * 2 + 2);
			const v0p = pos.slice(ind[i] * 3, ind[i] * 3 + 3);
			const v1p = pos.slice(ind[i + 1] * 3, ind[i + 1] * 3 + 3);
			const v2p = pos.slice(ind[i + 2] * 3, ind[i + 2] * 3 + 3);
			const edge1 = m4.subtractVectors(v1p, v0p);
			const edge2 = m4.subtractVectors(v2p, v0p);
			const deltaU1 = v1t[0] - v0t[0];
			const deltaV1 = v1t[1] - v0t[1];
			const deltaU2 = v2t[0] - v0t[0];
			const deltaV2 = v2t[1] - v0t[1];
			const f = 1 / (deltaU1 * deltaV2 - deltaU2 * deltaV1);
			const tg = [
				f * (deltaV2 * edge1[0] - deltaV1 * edge2[0]),
				f * (deltaV2 * edge1[1] - deltaV1 * edge2[1]),
				f * (deltaV2 * edge1[2] - deltaV1 * edge2[2]),
			];
			for (let j = 0; j < 3; ++j) {
				tang[ind[i + j] * 3 + 0] += tg[0];
				tang[ind[i + j] * 3 + 1] += tg[1];
				tang[ind[i + j] * 3 + 2] += tg[2];
			}
		}
		for (let i = 0; i < tang.length; i += 3) {
			const n = [];
			m4.normalize(tang.slice(i, i + 3), n);
			tang.set(n, i);
		}
		prim['tangent'] = new pcInfo(tang, tang.length / 3, 3, false, 0, 0);
		return prim;
	}

	static loadJSON(url, fn) {
		let result = {
			position: new pcInfo(new Float32Array(1), 0, 3, false, 0, 0),
			texcoord: new pcInfo(new Float32Array(1), 0, 2, false, 0, 0),
			normal: new pcInfo(new Float32Array(1), 0, 3, false, 0, 0),
			indices: new pcInfo(new Uint32Array(0), 0, 1, false, 0, 0),
			extents: {min: [0, 0, 0], max: [0, 0, 0]},
		};
		fn(result);
		let req = new XMLHttpRequest();
		req.open('GET', url);
		req.responseType = 'json';
		req.onload = function() {
			const json = req.response;
			result = {
				position: new pcInfo(new Float32Array(json.position), json.position.length / 3, 3, false, 0, 0),
				normal: new pcInfo(new Float32Array(json.normal), json.normal.length / 3, 3, false, 0, 0),
				indices: new pcInfo(new Uint32Array(json.indices), json.indices.length, 1, false, 0, 0),
				extents: Primitive.getExtents(json.position),
			};
			if (json.texcoord) {
				result.texcoord = new pcInfo(new Float32Array(json.texcoord), json.texcoord.length / 2, 2, false, 0, 0);
			}
			fn(result);
		}
		req.send();
	}
}

