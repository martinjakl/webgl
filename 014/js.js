// Text canvas
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec4 a_color;
uniform mat4 u_matrix;
out vec4 v_color;

void main() {
	gl_Position = u_matrix * a_position;
	v_color = a_color;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec4 v_color;
out vec4 outColor;
void main() {
	outColor = v_color;
}
`;

function createShader(gl, type, source) {
	const shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	if (success) {
		return shader;
	}
	console.log(gl.getShaderInfoLog(shader));
	gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
	const program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	const success = gl.getProgramParameter(program, gl.LINK_STATUS);
	if (success) {
		return program;
	}
	console.log(gl.getProgramInfoLog(program));
	gl.deleteProgram(program);
}

function createProgramShaders(gl, vs, fs) {
	const vertexShader = createShader(gl, gl.VERTEX_SHADER, vs);
	const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fs);
	const program = createProgram(gl, vertexShader, fragmentShader);
	return program;
}

function resize(canvas) {
	const displayWidth = canvas.clientWidth;
	const displayHeight = canvas.clientHeight;
	if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
		canvas.width = displayWidth;
		canvas.height = displayHeight;
	}
}

function randomInt(range) {
	return Math.floor(Math.random() * range);
}

function setGeometry(gl) {
	const positions =  new Float32Array([
		// left column front
		0,   0,  0,
		0, 150,  0,
		30,  0,  0,
		0, 150,  0,
		30,150,  0,
		30,   0,  0,

		// top rung front
		30,   0,  0,
		30,  30,  0,
		100,   0,  0,
		30,  30,  0,
		100,  30,  0,
		100,   0,  0,

		// middle rung front
		30,  60,  0,
		30,  90,  0,
		67,  60,  0,
		30,  90,  0,
		67,  90,  0,
		67,  60,  0,

		// left column back
		0,   0,  30,
		30,   0,  30,
		0, 150,  30,
		0, 150,  30,
		30,   0,  30,
		30, 150,  30,

		// top rung back
		30,   0,  30,
		100,   0,  30,
		30,  30,  30,
		30,  30,  30,
		100,   0,  30,
		100,  30,  30,

		// middle rung back
		30,  60,  30,
		67,  60,  30,
		30,  90,  30,
		30,  90,  30,
		67,  60,  30,
		67,  90,  30,

		// top
		0,   0,   0,
		100,   0,   0,
		100,   0,  30,
		0,   0,   0,
		100,   0,  30,
		0,   0,  30,

		// top rung right
		100,   0,   0,
		100,  30,   0,
		100,  30,  30,
		100,   0,   0,
		100,  30,  30,
		100,   0,  30,

		// under top rung
		30,   30,   0,
		30,   30,  30,
		100,  30,  30,
		30,   30,   0,
		100,  30,  30,
		100,  30,   0,

		// between top rung and middle
		30,   30,   0,
		30,   60,  30,
		30,   30,  30,
		30,   30,   0,
		30,   60,   0,
		30,   60,  30,

		// top of middle rung
		30,   60,   0,
		67,   60,  30,
		30,   60,  30,
		30,   60,   0,
		67,   60,   0,
		67,   60,  30,

		// right of middle rung
		67,   60,   0,
		67,   90,  30,
		67,   60,  30,
		67,   60,   0,
		67,   90,   0,
		67,   90,  30,

		// bottom of middle rung.
		30,   90,   0,
		30,   90,  30,
		67,   90,  30,
		30,   90,   0,
		67,   90,  30,
		67,   90,   0,

		// right of bottom
		30,   90,   0,
		30,  150,  30,
		30,   90,  30,
		30,   90,   0,
		30,  150,   0,
		30,  150,  30,

		// bottom
		0,   150,   0,
		0,   150,  30,
		30,  150,  30,
		0,   150,   0,
		30,  150,  30,
		30,  150,   0,

		// left side
		0,   0,   0,
		0,   0,  30,
		0, 150,  30,
		0,   0,   0,
		0, 150,  30,
		0, 150,   0,
	]);
	gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
}

function setColors(gl) {
	const u8 = new Uint8Array(new ArrayBuffer(16 * 6 * 3));
	for (let i = 0; i < 16 * 6; ++i) {
		switch (Math.floor(i / 6)) {
			case 0:
			case 1:
			case 2:
				u8.set([200, 70, 120], i * 3);
				break;
			case 3:
			case 4:
			case 5:
				u8.set([80, 70, 200], i * 3);
				break;
			case 6:
				u8.set([70, 200, 210], i * 3);
				break;
			case 7:
				u8.set([200, 200, 70], i * 3);
				break;
			case 8:
				u8.set([210, 100, 70], i * 3);
				break;
			case 9:
				u8.set([210, 160, 70], i * 3);
				break;
			case 10:
				u8.set([70, 180, 210], i * 3);
				break;
			case 11:
				u8.set([100, 70, 210], i * 3);
				break;
			case 12:
				u8.set([76, 210, 100], i * 3);
				break;
			case 13:
				u8.set([140, 210, 80], i * 3);
				break;
			case 14:
				u8.set([90, 130, 110], i * 3);
				break;
			case 15:
				u8.set([160, 160, 220], i * 3);
				break;
		}
	}
	gl.bufferData(gl.ARRAY_BUFFER, u8, gl.STATIC_DRAW);
}

function printText(ctx, msg, pixelX, pixelY) {
	ctx.save();
	ctx.translate(pixelX, pixelY);
	ctx.beginPath();
	ctx.moveTo(10, 5);
	ctx.lineTo(0, 0);
	ctx.lineTo(5, 10);
	ctx.moveTo(0, 0);
	ctx.lineTo(15, 15);
	ctx.stroke();
	ctx.fillText(msg, 20, 20);
	ctx.restore();
}

function start() {
	const canvas = document.getElementById("c");
	const gl = canvas.getContext("webgl2");
	const textCanvas = document.getElementById("text");
	const ctx = textCanvas.getContext("2d");
	if (gl && ctx) {
		const program = createProgramShaders(gl, vertexShaderSource, fragmentShaderSource);
		gl.useProgram(program);

		const positionAttributeLocation = gl.getAttribLocation(program, "a_position");
		const matrixUniformLocation = gl.getUniformLocation(program, "u_matrix");
		const colorAttribLocation = gl.getAttribLocation(program, "a_color");

		const vao = gl.createVertexArray();
		gl.bindVertexArray(vao);
		const positionBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		setGeometry(gl);
		gl.enableVertexAttribArray(positionAttributeLocation);
		let size = 3;
		let type = gl.FLOAT;
		let normalize = false;
		let stride = 0;
		let offset = 0;
		gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset);

		const colorBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
		setColors(gl);
		gl.enableVertexAttribArray(colorAttribLocation);
		size = 3;
		type = gl.UNSIGNED_BYTE;
		normalize = true;
		stride = 0;
		offset = 0;
		gl.vertexAttribPointer(colorAttribLocation, size, type, normalize, stride, offset);

		const translation = [0, 30, -360];
		const rotation = [m3.deg2rad(190), m3.deg2rad(40), m3.deg2rad(30)];
		const scale = [1, 1, 1];
		const fieldOfViewRadians = m3.deg2rad(60);
		const rotationSpeed = 1.2;
		let then = 0;

		requestAnimationFrame(drawScene);

		function drawScene(time) {
			const now = time * 0.001;
			const deltaTime = now - then;
			then = now;

			resize(gl.canvas);
			resize(ctx.canvas);

			rotation[1] += rotationSpeed * deltaTime;

			ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

			gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
			gl.enable(gl.CULL_FACE);
			gl.enable(gl.DEPTH_TEST);
			gl.clearColor(0, 0, 0, 0);
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

			gl.useProgram(program);
			gl.bindVertexArray(vao);

			const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
			const zNear = 1;
			const zFar = 2000;
			const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

			const spread = 170;
			for (let yy = -1; yy <= 1; ++yy) {
				for (let xx = -2; xx <= 2; ++xx) {
					const matrix = m4.translate(projectionMatrix, translation[0] + xx * spread, translation[1] + yy * spread, translation[2]);
					m4.xRotate(matrix, rotation[0], matrix);
					m4.yRotate(matrix, rotation[1] + yy * xx * 0.2, matrix);
					m4.zRotate(matrix, rotation[2], matrix);
					m4.scale(matrix, scale[0], scale[1], scale[2], matrix);
					gl.uniformMatrix4fv(matrixUniformLocation, false, matrix);
					gl.drawArrays(gl.TRIANGLES, 0, 16 * 6);

					const clipspace = m4.transformVector(matrix, [100, 0, 0, 1]);
					clipspace[0] /= clipspace[3];
					clipspace[1] /= clipspace[3];
					const pixelX = (clipspace[0] * 0.5 + 0.5) * gl.canvas.width;
					const pixelY = (clipspace[1] * -0.5 + 0.5) * gl.canvas.height;
					printText(ctx, "" + xx + "," + yy, pixelX, pixelY);
				}
			}

			requestAnimationFrame(drawScene);
		}
	} else {
		console.log("No GL");
	}
}

document.addEventListener('DOMContentLoaded', start);
