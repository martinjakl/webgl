//Translation / Rotation / Scale 2
const vertexShaderSource = `#version 300 es
in vec2 a_position;
uniform mat3 u_matrix;

void main() {
	gl_Position = vec4((u_matrix * vec3(a_position, 1)).xy, 0, 1);
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
uniform vec4 u_color;
out vec4 outColor;
void main() {
	outColor = u_color;
}
`;

function createShader(gl, type, source) {
	const shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	if (success) {
		return shader;
	}
	console.log(gl.getShaderInfoLog(shader));
	gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
	const program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	const success = gl.getProgramParameter(program, gl.LINK_STATUS);
	if (success) {
		return program;
	}
	console.log(gl.getProgramInfoLog(program));
	gl.deleteProgram(program);
}

function createProgramShaders(gl, vs, fs) {
	const vertexShader = createShader(gl, gl.VERTEX_SHADER, vs);
	const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fs);
	const program = createProgram(gl, vertexShader, fragmentShader);
	return program;
}

function resize(canvas) {
	const displayWidth = canvas.clientWidth;
	const displayHeight = canvas.clientHeight;
	if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
		canvas.width = displayWidth;
		canvas.height = displayHeight;
	}
}

function randomInt(range) {
	return Math.floor(Math.random() * range);
}

function setGeometry(gl) {
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
		0, 0,
		30, 0,
		0, 150,
		0, 150,
		30, 0,
		30, 150,

		30, 0,
		100, 0,
		30, 30,
		30, 30,
		100, 0,
		100, 30,

		30, 60,
		67, 60,
		30, 90,
		30, 90,
		67, 60,
		67, 90,
	]), gl.STATIC_DRAW);
}

function start() {
	const canvas = document.getElementById("c");
	const gl = canvas.getContext("webgl2");
	if (gl) {
		resize(gl.canvas);

		const program = createProgramShaders(gl, vertexShaderSource, fragmentShaderSource);
		gl.useProgram(program);

		const positionAttributeLocation = gl.getAttribLocation(program, "a_position");
		const matrixUniformLocation = gl.getUniformLocation(program, "u_matrix");
		const colorUniformLocation = gl.getUniformLocation(program, "u_color");

		const vao = gl.createVertexArray();
		gl.bindVertexArray(vao);
		const positionBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		setGeometry(gl);
		gl.enableVertexAttribArray(positionAttributeLocation);
		const size = 2;
		const type = gl.FLOAT;
		const normalize = false;
		const stride = 0;
		const offset = 0;
		gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset);

		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
		gl.clearColor(0, 0, 0, 0);
		gl.clear(gl.COLOR_BUFFER_BIT);

		gl.useProgram(program);
		gl.bindVertexArray(vao);

		const translationMatrix = m3.translation(100, 250);
		const rotationMatrix = m3.rotation(Math.PI / 4);
		const scaleMatrix = m3.scaling(1.2, 1.4);
		const moveOriginMatrix = m3.translation(-50, -75);

		gl.uniform4f(colorUniformLocation, 0.6, 0.8, 0.2, 1);
		let matrix = m3.projection(gl.canvas.clientWidth, gl.canvas.clientHeight);
		for (let i = 0; i < 5; ++i) {
			matrix = m3.multiply(matrix, translationMatrix);
			matrix = m3.multiply(matrix, rotationMatrix);
			matrix = m3.multiply(matrix, moveOriginMatrix);
			matrix = m3.multiply(matrix, scaleMatrix);
			gl.uniformMatrix3fv(matrixUniformLocation, false, matrix);
			gl.drawArrays(gl.TRIANGLES, 0, 18);
		}
	} else {
		console.log("No GL");
	}
}

document.addEventListener('DOMContentLoaded', start);
