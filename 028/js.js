// Pong
/*
 * Copyright 2018 Martin Jakl (martin.jakl@macsnet.cz).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
uniform mat4 u_matrix;

void main() {
	gl_Position = u_matrix * a_position;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
out vec4 outColor;
void main() {
	outColor = vec4(1, 1, 1, 1);
}
`;

function createMiddle(top, bottom) {
	const coords = [];
	for (let i = top; i >= bottom; i -= 2) {
		coords.push(
			-0.5, i, 0,
			-0.5, i - 1, 0,
			0.5, i, 0,
			0.5, i, 0,
			-0.5, i - 1, 0,
			0.5, i - 1, 0,
		);
	}
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create0() {
	const coords = [];
	coords.push(
		-1.5, -2.5, 0,
		1.5, -2.5, 0,
		-1.5, -1.5, 0,
		-1.5, -1.5, 0,
		1.5, -2.5, 0,
		1.5, -1.5, 0,

		-1.5, -1.5, 0,
		-0.5, -1.5, 0,
		-1.5, 1.5, 0,
		-1.5, 1.5, 0,
		-0.5, -1.5, 0,
		-0.5, 1.5, 0,

		0.5, -1.5, 0,
		1.5, -1.5, 0,
		1.5, 1.5, 0,
		0.5, -1.5, 0,
		1.5, 1.5, 0,
		0.5, 1.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		1.5, 2.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create1() {
	const coords = [];
	coords.push(
		0.5, -2.5, 0,
		1.5, -2.5, 0,
		1.5, 2.5, 0,
		0.5, -2.5, 0,
		1.5, 2.5, 0,
		0.5, 2.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create2() {
	const coords = [];
	coords.push(
		-1.5, -2.5, 0,
		1.5, -2.5, 0,
		-1.5, -1.5, 0,
		-1.5, -1.5, 0,
		1.5, -2.5, 0,
		1.5, -1.5, 0,

		-1.5, -0.5, 0,
		1.5, -0.5, 0,
		-1.5, 0.5, 0,
		-1.5, 0.5, 0,
		1.5, -0.5, 0,
		1.5, 0.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
		1.5, 2.5, 0,

		-1.5, -1.5, 0,
		-0.5, -1.5, 0,
		-1.5, -0.5, 0,
		-1.5, -0.5, 0,
		-0.5, -1.5, 0,
		-0.5, -0.5, 0,

		1.5, 1.5, 0,
		0.5, 1.5, 0,
		1.5, 0.5, 0,
		1.5, 0.5, 0,
		0.5, 1.5, 0,
		0.5, 0.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create3() {
	const coords = [];
	coords.push(
		-1.5, -2.5, 0,
		1.5, -2.5, 0,
		-1.5, -1.5, 0,
		-1.5, -1.5, 0,
		1.5, -2.5, 0,
		1.5, -1.5, 0,

		-1.5, -0.5, 0,
		1.5, -0.5, 0,
		-1.5, 0.5, 0,
		-1.5, 0.5, 0,
		1.5, -0.5, 0,
		1.5, 0.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
		1.5, 2.5, 0,

		1.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -0.5, 0,
		0.5, -1.5, 0,

		1.5, 1.5, 0,
		0.5, 1.5, 0,
		1.5, 0.5, 0,
		1.5, 0.5, 0,
		0.5, 1.5, 0,
		0.5, 0.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create4() {
	const coords = [];
	coords.push(
		-1.5, -0.5, 0,
		1.5, -0.5, 0,
		-1.5, 0.5, 0,
		-1.5, 0.5, 0,
		1.5, -0.5, 0,
		1.5, 0.5, 0,

		1.5, -2.5, 0,
		1.5, 2.5, 0,
		0.5, -2.5, 0,
		1.5, 2.5, 0,
		0.5, 2.5, 0,
		0.5, -2.5, 0,

		-1.5, 2.5, 0,
		-1.5, 0.5, 0,
		-0.5, 2.5, 0,
		-1.5, 0.5, 0,
		-0.5, 0.5, 0,
		-0.5, 2.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create5() {
	const coords = [];
	coords.push(
		-1.5, -2.5, 0,
		1.5, -2.5, 0,
		-1.5, -1.5, 0,
		-1.5, -1.5, 0,
		1.5, -2.5, 0,
		1.5, -1.5, 0,

		-1.5, -0.5, 0,
		1.5, -0.5, 0,
		-1.5, 0.5, 0,
		-1.5, 0.5, 0,
		1.5, -0.5, 0,
		1.5, 0.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
		1.5, 2.5, 0,

		-1.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 1.5, 0,
		-0.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 0.5, 0,

		0.5, -1.5, 0,
		1.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -0.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create6() {
	const coords = [];
	coords.push(
		-1.5, -2.5, 0,
		1.5, -2.5, 0,
		-1.5, -1.5, 0,
		-1.5, -1.5, 0,
		1.5, -2.5, 0,
		1.5, -1.5, 0,

		-1.5, -0.5, 0,
		1.5, -0.5, 0,
		-1.5, 0.5, 0,
		-1.5, 0.5, 0,
		1.5, -0.5, 0,
		1.5, 0.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
		1.5, 2.5, 0,

		-1.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 1.5, 0,
		-0.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 0.5, 0,

		-1.5, -0.5, 0,
		-1.5, -1.5, 0,
		-0.5, -1.5, 0,
		-1.5, -0.5, 0,
		-0.5, -1.5, 0,
		-0.5, -0.5, 0,

		0.5, -1.5, 0,
		1.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -0.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create7() {
	const coords = [];
	coords.push(
		0.5, -2.5, 0,
		1.5, -2.5, 0,
		1.5, 1.5, 0,
		0.5, -2.5, 0,
		1.5, 1.5, 0,
		0.5, 1.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		1.5, 2.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create8() {
	const coords = [];
	coords.push(
		-1.5, -2.5, 0,
		1.5, -2.5, 0,
		-1.5, -1.5, 0,
		-1.5, -1.5, 0,
		1.5, -2.5, 0,
		1.5, -1.5, 0,

		-1.5, -0.5, 0,
		1.5, -0.5, 0,
		-1.5, 0.5, 0,
		-1.5, 0.5, 0,
		1.5, -0.5, 0,
		1.5, 0.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
		1.5, 2.5, 0,

		-1.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 1.5, 0,
		-0.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 0.5, 0,

		-1.5, -0.5, 0,
		-1.5, -1.5, 0,
		-0.5, -1.5, 0,
		-1.5, -0.5, 0,
		-0.5, -1.5, 0,
		-0.5, -0.5, 0,

		1.5, 0.5, 0,
		1.5, 1.5, 0,
		0.5, 1.5, 0,
		1.5, 0.5, 0,
		0.5, 1.5, 0,
		0.5, 0.5, 0,

		0.5, -1.5, 0,
		1.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -0.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function create9() {
	const coords = [];
	coords.push(
		-1.5, -2.5, 0,
		1.5, -2.5, 0,
		-1.5, -1.5, 0,
		-1.5, -1.5, 0,
		1.5, -2.5, 0,
		1.5, -1.5, 0,

		-1.5, -0.5, 0,
		1.5, -0.5, 0,
		-1.5, 0.5, 0,
		-1.5, 0.5, 0,
		1.5, -0.5, 0,
		1.5, 0.5, 0,

		1.5, 2.5, 0,
		-1.5, 2.5, 0,
		-1.5, 1.5, 0,
		-1.5, 1.5, 0,
		1.5, 1.5, 0,
		1.5, 2.5, 0,

		-1.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 1.5, 0,
		-0.5, 1.5, 0,
		-1.5, 0.5, 0,
		-0.5, 0.5, 0,

		1.5, 0.5, 0,
		1.5, 1.5, 0,
		0.5, 1.5, 0,
		1.5, 0.5, 0,
		0.5, 1.5, 0,
		0.5, 0.5, 0,

		0.5, -1.5, 0,
		1.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -1.5, 0,
		1.5, -0.5, 0,
		0.5, -0.5, 0,
	);
	return {position: new pcInfo(new Float32Array(coords), coords.length / 3, 3, false, 0, 0)};
}

function createNumbers(gl, program) {
	const numbers = [];
	numbers.push(gl.createVAO(program, {position: "a_position"}, create0()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create1()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create2()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create3()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create4()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create5()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create6()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create7()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create8()));
	numbers.push(gl.createVAO(program, {position: "a_position"}, create9()));
	return numbers;
}

function draw(gl, canvas) {
	const awCtx = new WAContext();
	const aw = awCtx.load(document.getElementById("aw").getAttribute("path"));
	const ar = awCtx.load(document.getElementById("ar").getAttribute("path"));
	const af = awCtx.load(document.getElementById("af").getAttribute("path"));
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const ball = gl.createVAO(program, {position: "a_position"}, Primitive.createXYQuad(1));
	const racket1 = gl.createVAO(program, {position: "a_position"}, Primitive.moveRotateScale(Primitive.createXYQuad(1), [0, 0, 0], [0, 0, 0], [1, 5, 1]));
	const racket2 = gl.createVAO(program, {position: "a_position"}, Primitive.moveRotateScale(Primitive.createXYQuad(1), [0, 0, 0], [0, 0, 0], [1, 5, 1]));
	const border1 = gl.createVAO(program, {position: "a_position"}, Primitive.moveRotateScale(Primitive.createXYQuad(1), [0, 20, 0], [0, 0, 0], [70, 1, 1]));
	const border2 = gl.createVAO(program, {position: "a_position"}, Primitive.moveRotateScale(Primitive.createXYQuad(1), [0, -20, 0], [0, 0, 0], [70, 1, 1]));
	const middle = gl.createVAO(program, {position: "a_position"}, createMiddle(18.5, -19));
	const numbers = createNumbers(gl, program);

	const fieldOfViewRadians = m3.deg2rad(35);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, 70];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);

	let r = [0, 0];
	const rd = [0, 0];
	const rpd = [{}, {}];
	let bpos = [];
	let bdir = [];
	const dirs = [-0.5, -0.25, 0, 0.25, 0.5];
	let ballspeed = 0;
	let score = [0, 0];
	let finish = false;
	let preventdefault = false;
	const maxY = 19.5;
	const maxX = 34.5;
	const playerX = 32;
	const maxPlayerY = 17;
	let wasPressed = null;
	let then = 0;

	function resetBall(dir) {
		bpos = [0, maxY];
		bdir = [-0.5 * dir, -0.25];
	}

	function reset() {
		score = [0, 0];
		finish = false;
		r = [0, 0];
		resetBall(1);
		ballspeed = 25;
	}

	reset();

	function mover(ind, d) {
		const rdc = rd[ind];
		if (!rdc) {
			for (const k in rpd[ind]) {
				if (rpd[ind][k]) {
					rdc = rpd[ind][k];
					break;
				}
			}
		}
		if (rdc > 0) {
			r[ind] += 60 * d * rdc;
			if (r[ind] > maxPlayerY) {
				r[ind]= maxPlayerY;
			}
		} else if (rdc < 0) {
			r[ind] -= 60 * d * -rdc;
			if (r[ind] < -maxPlayerY) {
				r[ind] = -maxPlayerY;
			}
		}
	}

	function scored(ind, dir) {
		resetBall(dir);
		if (!finish) {
			af.play();
			if (++score[ind] == 15) {
				finish = true;
			}
			if (Math.max(score[0], score[1]) == 4) {
				ballspeed = 50;
			}
			if (Math.max(score[0], score[1]) == 10) {
				ballspeed = 75;
			}
		}
	}

	function hitBall(i, dir) {
		if (bpos[1] >= r[i] - 2.5 && bpos[1] <= r[i] + 2.5) {
			bpos[0] = dir * playerX;
			bdir[0] *= -1;
			const ind = Math.floor(bpos[1] - r[i] + 2.5);
			bdir[1] = dirs[ind];
			ar.play();
		}
	}

	function moveb(delta) {
		bpos[0] += ballspeed * delta * bdir[0];
		bpos[1] += ballspeed * delta * bdir[1];
		if (bpos[1] >= maxY) {
			bpos[1] = maxY;
			bdir[1] *= -1;
			aw.play();
		}
		if (bpos[1] <= -maxY) {
			bpos[1] = -maxY;
			bdir[1] *= -1;
			aw.play();
		}
		if (bpos[0] > maxX) {
			scored(0, 1);
		}
		if (bpos[0] < -maxX) {
			scored(1, -1);
		}
		if (!finish) {
			if (bpos[0] >= playerX) {
				hitBall(1, 1);
			}
			if (bpos[0] <= -playerX) {
				hitBall(0, -1);
			}
		}
	}

	const con = new Controls(canvas, {
		kw: function(v) {rd[0] = v;},
		kup: function(v) {rd[1] = v;},
		ks: function(v) {rd[0] = -v;},
		kdo: function(v) {rd[1] = -v;},
		ksp: reset,
		mlc: function(x, y) {awCtx.resume(); if (x / gl.clientWidth > 0.4 && x / gl.clientWidth < 0.6) {if (y / gl.clientHeight > 0.5) {reset();} else {preventdefault = !preventdefault;}}},
		preventdeftouch: function() {return preventdefault;},
		te: function(t) {if (t.currX / gl.clientWidth > 0.5) {rd[1] = 0;} else {rd[0] = 0;}},
		tm: function(t) {if (t.currX / gl.clientWidth > 0.5) {rd[1] = (t.lastY - t.currY) / 3;} else {rd[0] = (t.lastY - t.currY) / 3;} return true;},
		pry: function(v, i) {rpd[1][i] = -v;},
		ply: function(v, i) {rpd[0][i] = -v;},
		pb0: function(v, i) {if (!v && wasPressed === i) {reset(); wasPressed = null;} if (v) {wasPressed = i;}},
	}, false);

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;

		con.updatePads();
		mover(0, deltaTime);
		mover(1, deltaTime);
		moveb(deltaTime);

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame(null, [0, 0, 0, 1]);
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const matrix = m4.translate(viewProjectionMatrix, bpos[0], bpos[1], 0);
		gl.drawVAO(ball, {u_matrix: matrix});

		m4.translate(viewProjectionMatrix, -playerX - 1, r[0], 0, matrix);
		gl.drawVAO(racket1, {u_matrix: matrix});

		m4.translate(viewProjectionMatrix, playerX + 1, r[1], 0, matrix);
		gl.drawVAO(racket2, {u_matrix: matrix});

		gl.drawVAO(border1, {u_matrix: viewProjectionMatrix});
		gl.drawVAO(border2, {u_matrix: viewProjectionMatrix});
		gl.drawVAO(middle, {u_matrix: viewProjectionMatrix});

		function drawScore(ind, pos) {
			let sc = score[ind];
			if (score[ind] >= 10) {
				sc -= 10;
			}
			const matrix = m4.translate(viewProjectionMatrix, pos[0], pos[1], 0);
			gl.drawVAO(numbers[sc], {u_matrix: matrix});
			if (score[ind] >= 10) {
				const matrix = m4.translate(viewProjectionMatrix, pos[0] - 4, pos[1], 0);
				gl.drawVAO(numbers[1], {u_matrix: matrix});
			}
		}

		drawScore(0, [-5, 13]);
		drawScore(1, [6, 13]);

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		draw(gl, canvas);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
	}
}

function getReady() {
	if (window.cordova) {
		document.addEventListener('deviceready', start, false);
	} else {
		start();
	}
}

document.addEventListener('DOMContentLoaded', getReady);
