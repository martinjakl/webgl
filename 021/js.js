// Mipmap 1
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_matrix;
out vec2 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = a_texcoord;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

function draw(fpsNode, gl) {
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const texture = gl.loadTexture("../img/mip-low-res-example.png", gl.TextureWrap.CLAMP_TO_EDGE, gl.TextureWrap.CLAMP_TO_EDGE);
	const qvao = gl.createVAO(program, {position: "a_position", texcoord: "a_texcoord"}, Primitive.createXYQuad(1));

	let allocateFBTexture = true;
	let frameBufferWidth;
	let frameBufferHeight;
	const fbtex = gl.createFBTexture();

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, 3];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		if (glUtils.resizeCanvas(gl.canvas, window.devicePixelRatio) || allocateFBTexture) {
			allocateFBTexture = false;
			frameBufferWidth = gl.canvas.clientWidth / 4;
			frameBufferHeight = gl.canvas.clientHeight / 4;
			gl.resizeFBTexture(fbtex.tex, frameBufferWidth, frameBufferHeight);
		}

		gl.initFrame(fbtex.fb, [0, 0, 0, 1], frameBufferWidth, frameBufferHeight);

		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const settings = [
			{x: -1, y: -3, z: -30, filter: gl.MipMap.NEAREST,              },
			{x:  0, y: -3, z: -30, filter: gl.MipMap.LINEAR,               },
			{x:  1, y: -3, z: -30, filter: gl.MipMap.NEAREST_MIPMAP_LINEAR,},
			{x: -1, y: -1, z: -10, filter: gl.MipMap.NEAREST,              },
			{x:  0, y: -1, z: -10, filter: gl.MipMap.LINEAR,               },
			{x:  1, y: -1, z: -10, filter: gl.MipMap.NEAREST_MIPMAP_LINEAR,},
			{x: -1, y:  1, z:   0, filter: gl.MipMap.NEAREST,              },
			{x:  0, y:  1, z:   0, filter: gl.MipMap.LINEAR,               },
			{x:  1, y:  1, z:   0, filter: gl.MipMap.LINEAR_MIPMAP_NEAREST,},
		];
		const xSpacing = 1.2;
		const ySpacing = 0.7;
		settings.forEach(function(s) {
			const z = -5 + s.z;
			const r = Math.abs(z) * Math.sin(fieldOfViewRadians * 0.5);
			const x = Math.sin(now * 0.2) * r;
			const y = Math.cos(now * 0.2) * r * 0.5;
			const r2 = 1 + r * 0.2;

			gl.changeTexMipMapFilter(texture, gl.MipMapFilter.TEXTURE_MIN_FILTER, s.filter);

			const matrix = m4.translate(viewProjectionMatrix, x + s.x * xSpacing * r2, y + s.y * ySpacing * r2, z);
			gl.drawVAO(qvao, {u_matrix: matrix, u_texture: texture});
		});

		gl.initFrame();
		gl.drawVAO(qvao, {u_matrix: [
			2, 0, 0, 0,
			0, 2, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1,
		], u_texture: fbtex.tex});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
