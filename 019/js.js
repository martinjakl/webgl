// Camera fixed to object with new libs and 1 VAO
"use strict";
const fVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
in vec2 a_trans;
in float a_speed;
uniform mat4 u_matrix;
uniform float u_time;
out vec2 v_texcoord;

void main() {
	mat4 t1 = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, -a_trans.x, 0, -a_trans.y, 1);
	mat4 t2 = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, a_trans.x, 0, a_trans.y, 1);
	vec4 pos = t1 * a_position;
	float angle = a_speed * u_time;
	float s = sin(angle);
	float c = cos(angle);
	mat4 rot = mat4(c, 0, -s, 0, 0, 1, 0, 0, s, 0, c, 0, 0, 0, 0, 1);
	pos = rot * pos;
	pos = t2 * pos;
	gl_Position = u_matrix * pos;
	v_texcoord = a_texcoord;
}
`;

const fFragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

const bigFVertexShaderSource = `#version 300 es
in vec4 a_position;
in vec4 a_color;
uniform mat4 u_matrix;
out vec4 v_color;

void main() {
	gl_Position = u_matrix * a_position;
	v_color = a_color;
}
`;

const bigFFragmentShaderSource = `#version 300 es
precision mediump float;
in vec4 v_color;
out vec4 outColor;

void main() {
	outColor = v_color;
}
`;

function draw(fpsNode, gl) {
	const fProgram = gl.createProgramShaders(fVertexShaderSource, fFragmentShaderSource);
	const bigFProgram = gl.createProgramShaders(bigFVertexShaderSource, bigFFragmentShaderSource);

	const numFs = 35;
	const radius  = 750;
	const rows = 47;
	const rotF = [Math.PI, 0, 0];
	const moveF = [-50, -75, -15];
	const scaleF = [1, 1, 1];
	const prim0 = Primitive.moveRotateScale(Primitive.createF(), moveF, rotF, scaleF);
	delete prim0.texcoord2;
	const prim1 = Primitive.moveRotateScale(Primitive.createF(), moveF, rotF, scaleF);
	prim1.texcoord = prim1.texcoord2;
	delete prim1.texcoord2;
	const prim = Primitive.createMany(numFs, rows, radius, [prim0, prim1], 'texcoord', 'position');
	const fvao = gl.createVAO(fProgram, {position: "a_position", texcoord: "a_texcoord", translation: "a_trans", speed: "a_speed"}, prim);
	const fTex = gl.loadTexture("../img/f-texture.png");

	const bigF = Primitive.moveRotateScale(Primitive.createF(), moveF, rotF, [7, 7, 7]);
	const bvao = gl.createVAO(bigFProgram, {position: "a_position", color: "a_color"}, bigF);

	const fieldOfViewRadians = m3.deg2rad(60);
	let camAngle = 0;

	const zNear = 1;
	const zFar = 7000;
	const fPosition = [radius, 0, 0];
	const up = [0, 1, 0];

	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame();

		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);

		camAngle += 60 * deltaTime;
		camAngle = camAngle % 360;

		const cameraPosition = [radius * 1.5 * Math.cos(m3.deg2rad(camAngle)), 1000, radius * 2 * Math.sin(m3.deg2rad(camAngle))];
		const camerMatrix = m4.lookAt(cameraPosition, fPosition, up);
		const viewMatrix = m4.inverse(camerMatrix);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);
		gl.drawVAO(fvao, {u_matrix: viewProjectionMatrix, u_texture: fTex, u_time: now});
		gl.drawVAO(bvao, {u_matrix: viewProjectionMatrix});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
