// Mipmap 2
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_matrix;
out vec2 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = a_texcoord;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;
void main() {
	outColor = texture(u_texture, v_texcoord);
}
`;

function draw(fpsNode, gl) {
	const zDepth = 50;
	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const qvao = gl.createVAO(program, {position: "a_position", texcoord: "a_texcoord"}, Primitive.createPlane(1, zDepth));
	const texture = gl.loadTexture("../img/mip-low-res-example.png", undefined, null);
	const c = document.createElement("canvas");
	const ctx = c.getContext("2d");
	const mips = [
		{size: 64, color: "rgb(128,0,255)"},
		{size: 32, color: "rgb(0,0,255)"},
		{size: 16, color: "rgb(255,0,0)"},
		{size: 8, color: "rgb(255,255,0)"},
		{size: 4, color: "rgb(0,255,0)"},
		{size: 2, color: "rgb(0,255,255)"},
		{size: 1, color: "rgb(255,0,255)"},
	];
	const texture2 = gl.createTextureMipLevelsFromCanvas(function(s) {
		const size = s.size;
		c.width = size;
		c.height = size;
		ctx.fillStyle = "rgb(255,255,255)";
		ctx.fillRect(0, 0, size, size);
		ctx.fillStyle = s.color;
		ctx.fillRect(0, 0, size / 2, size / 2);
		ctx.fillRect(size / 2, size / 2, size / 2, size / 2);
		return c;
	}, mips, undefined, null);
	const textures = [texture, texture2];
	let textureIndex = 0;
	document.querySelector("body").addEventListener("click", function() {
		textureIndex = (textureIndex + 1) % textures.length;
	});

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, 2];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas, window.devicePixelRatio);
		gl.initFrame(null, [0, 0, 0, 1]);
		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const settings = [
			{x: -1, y:  1, zRot: 0, magFilter: gl.MipMap.NEAREST, minFilter: gl.MipMap.NEAREST,               },
			{x:  0, y:  1, zRot: 0, magFilter: gl.MipMap.LINEAR,  minFilter: gl.MipMap.LINEAR,                },
			{x:  1, y:  1, zRot: 0, magFilter: gl.MipMap.LINEAR,  minFilter: gl.MipMap.NEAREST_MIPMAP_NEAREST,},
			{x: -1, y: -1, zRot: 1, magFilter: gl.MipMap.LINEAR,  minFilter: gl.MipMap.LINEAR_MIPMAP_NEAREST, },
			{x:  0, y: -1, zRot: 1, magFilter: gl.MipMap.LINEAR,  minFilter: gl.MipMap.NEAREST_MIPMAP_LINEAR, },
			{x:  1, y: -1, zRot: 1, magFilter: gl.MipMap.LINEAR,  minFilter: gl.MipMap.LINEAR_MIPMAP_LINEAR,  },
		];
		const xSpacing = 1.2;
		const ySpacing = 0.7;
		settings.forEach(function(s) {
			gl.changeTexMipMapFilter(textures[textureIndex], gl.MipMapFilter.TEXTURE_MIN_FILTER, s.minFilter);
			gl.changeTexMipMapFilter(textures[textureIndex], gl.MipMapFilter.TEXTURE_MAG_FILTER, s.magFilter);
			const matrix = m4.translate(viewProjectionMatrix, s.x * xSpacing, s.y * ySpacing, -zDepth * 0.5);
			m4.zRotate(matrix, s.zRot * Math.PI, matrix);
			m4.scale(matrix, 1, 1, zDepth, matrix);
			gl.drawVAO(qvao, {u_matrix: matrix, u_texture: textures[textureIndex]});
		});

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2", {antialias: false}));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
