// Matrix stack 2
"use strict";
const vertexShaderSource = `#version 300 es
in vec4 a_position;
in vec2 a_texcoord;
uniform mat4 u_matrix;
uniform mat4 u_textureMatrix;
out vec2 v_texcoord;

void main() {
	gl_Position = u_matrix * a_position;
	v_texcoord = (u_textureMatrix * vec4(a_texcoord, 0, 1)).xy;
}
`;

const fragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;

void main() {
	if (v_texcoord.x < 0.0 || v_texcoord.y < 0.0 || v_texcoord.x > 1.0 || v_texcoord.y > 1.0) {
		discard;
	}
	outColor = texture(u_texture, v_texcoord);
}
`;

function draw(fpsNode, gl) {
	class MatrixStack {
		constructor() {
			this.stack = [];
			this.restore();
		}

		restore() {
			this.stack.pop();
			if (this.stack.length < 1) {
				this.stack[0] = m4.identity();
			}
		}

		save() {
			this.stack.push(this.getCurrentMatrix());
		}

		getCurrentMatrix() {
			return this.stack[this.stack.length - 1].slice();
		}

		setCurrentMatrix(m) {
			return this.stack[this.stack.length - 1] = m;
		}

		translate(x, y, z) {
			if (z === undefined) {
				z = 0;
			}
			const m = this.getCurrentMatrix();
			this.setCurrentMatrix(m4.translate(m, x, y, z));
		}

		rotateZ(angleInRadians) {
			const m = this.getCurrentMatrix();
			this.setCurrentMatrix(m4.zRotate(m, angleInRadians));
		}

		scale(x, y, z) {
			if (z === undefined) {
				z = 1;
			}
			const m = this.getCurrentMatrix();
			this.setCurrentMatrix(m4.scale(m, x, y, z));
		}
	}

	const matrixStack = new MatrixStack();

	function drawImage(gl, vao, tex, texWidth, texHeigh, srcX, srcY, srcWidth, srcHeight, dstX, dstY, dstWidth, dstHeight, srcRotation) {
		if (dstX === undefined) {
			dstX = srcX;
			srcX = 0;
		}
		if (dstY === undefined) {
			dstY = srcY;
			srcY = 0;
		}
		if (srcWidth === undefined) {
			srcWidth = texWidth;
		}
		if (srcHeight === undefined) {
			srcHeight = texHeigh;
		}
		if (dstWidth === undefined) {
			dstWidth = srcWidth;
			srcWidth = texWidth;
		}
		if (dstHeight === undefined) {
			dstHeight = srcHeight;
			srcHeight = texHeigh;
		}
		if (srcRotation === undefined) {
			srcRotation = 0;
		}
		const matrix = m4.orthographic(0, gl.canvas.clientWidth, gl.canvas.clientHeight, 0, -1, 1);
		m4.multiply(matrix, matrixStack.getCurrentMatrix(), matrix);
		m4.translate(matrix, dstX, dstY, 0, matrix);
		m4.scale(matrix, dstWidth, dstHeight, 1, matrix);

		const texMatrix = m4.scaling(1 / texWidth, 1 / texHeigh, 1);
		m4.translate(texMatrix, texWidth * 0.5, texHeigh * 0.5, 0, texMatrix);
		m4.zRotate(texMatrix, srcRotation, texMatrix);
		m4.translate(texMatrix, texWidth * -0.5, texHeigh * -0.5, 0, texMatrix);
		m4.translate(texMatrix, srcX, srcY, 0, texMatrix);
		m4.scale(texMatrix, srcWidth, srcHeight, 1, texMatrix);

		gl.drawVAO(vao, {u_matrix: matrix, u_texture: tex, u_textureMatrix: texMatrix});
	}

	const program = gl.createProgramShaders(vertexShaderSource, fragmentShaderSource);
	const vao = gl.createVAO(program, {position: "a_position", texcoord: "a_texcoord"}, Primitive.moveRotateScale(Primitive.createXYQuad(1), [0.5, 0.5, 0], [0, 0, 0], [1, 1, 1]));
	const tex = gl.loadTexture("../img/pic2.jpg", undefined, undefined, true);
	let then = 0;

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);
		gl.initFrame(undefined, undefined, undefined, undefined, false);
		matrixStack.save();
		matrixStack.translate(gl.canvas.width / 2, gl.canvas.height / 2);
		matrixStack.rotateZ(now);

		matrixStack.save();
		matrixStack.translate(tex.w / -2, tex.h / -2);
		drawImage(gl, vao, tex.texture, tex.w, tex.h, 0, 0);
		matrixStack.restore();

		matrixStack.save();
		matrixStack.translate(tex.w / -2, tex.h / -2);
		matrixStack.rotateZ(Math.sin(now * 2.2));
		matrixStack.scale(0.2, 0.2);
		matrixStack.translate(-tex.w, -tex.h);
		drawImage(gl, vao, tex.texture, tex.w, tex.h, 0, 0);
		matrixStack.restore();

		matrixStack.save();
		matrixStack.translate(tex.w / 2, tex.h / -2);
		matrixStack.rotateZ(Math.sin(now * 2.3));
		matrixStack.scale(0.2, 0.2);
		matrixStack.translate(0, -tex.h);
		drawImage(gl, vao, tex.texture, tex.w, tex.h, 0, 0);
		matrixStack.restore();

		matrixStack.save();
		matrixStack.translate(tex.w / -2, tex.h / 2);
		matrixStack.rotateZ(Math.sin(now * 2.4));
		matrixStack.scale(0.2, 0.2);
		matrixStack.translate(-tex.w, 0);
		drawImage(gl, vao, tex.texture, tex.w, tex.h, 0, 0);
		matrixStack.restore();

		matrixStack.save();
		matrixStack.translate(tex.w / 2, tex.h / 2);
		matrixStack.rotateZ(Math.sin(now * 2.5));
		matrixStack.scale(0.2, 0.2);
		matrixStack.translate(0, 0);
		drawImage(gl, vao, tex.texture, tex.w, tex.h, 0, 0);
		drawImage(gl, vao, tex.texture, tex.w, tex.h, 0, 0);
		matrixStack.restore();

		matrixStack.restore();

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
