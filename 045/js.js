// Mesh from JSON
"use strict";
const specularVertexShader = `#version 300 es
in vec4 a_position;
in vec3 a_normal;
uniform vec3 u_lightWorldPosition;
uniform vec3 u_viewWorldPosition;
uniform mat4 u_world;
uniform mat4 u_worldViewProjection;
uniform mat4 u_worldInverseTranspose;
out vec3 v_normal;
out vec3 v_surfaceToLight;
out vec3 v_surfaceToView;

void main() {
	gl_Position = u_worldViewProjection * a_position;
	v_normal = mat3(u_worldInverseTranspose) * a_normal;
	vec3 surfaceWorldPosition = (u_world * a_position).xyz;
	v_surfaceToLight = u_lightWorldPosition - surfaceWorldPosition;
	v_surfaceToView = u_viewWorldPosition - surfaceWorldPosition;
}
`;

const specularShader = `#version 300 es
#if GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
in vec3 v_normal;
in vec3 v_surfaceToLight;
in vec3 v_surfaceToView;
uniform vec4 u_color;
uniform float u_shininess;
out vec4 outColor;

void main() {
	vec3 normal = normalize(v_normal);
	vec3 surfaceToLightDirection = normalize(v_surfaceToLight);
	vec3 surfaceToViewDirection = normalize(v_surfaceToView);
	vec3 halfVector = normalize(surfaceToLightDirection + surfaceToViewDirection);

	float light = dot(normal, surfaceToLightDirection);
	float specular = 0.0;
	if (light > 0.0) {
		specular = pow(dot(normal, halfVector), u_shininess);
	}
	outColor = u_color;
	outColor.rgb *= light;
	outColor.rgb += specular;
}
`;

function draw(fpsNode, gl, canvas) {
	const program = gl.createProgramShaders(specularVertexShader, specularShader);
	let vao;
	Primitive.loadJSON('../mesh/guitar_body.json', function(prim) {
		vao = gl.createVAO(program, {position: "a_position", normal: "a_normal"}, prim, 'indices');
	});

	const fieldOfViewRadians = m3.deg2rad(60);
	const zNear = 1;
	const zFar = 2000;
	const cameraPosition = [0, 0, 2];
	const target = [0, 0, 0];
	const up = [0, 1, 0];
	const cameraMatrix = m4.lookAt(cameraPosition, target, up);
	const viewMatrix = m4.inverse(cameraMatrix);
	let modelXRotationRadians = 0;
	let modelYRotationRadians = 0;
	let then = 0;
	let autoAnim = true;
	let xmod = 0;
	let ymod = 0;
	let xpmod = 0;
	let ypmod = 0;
	let lb = null;

	const controls = new Controls(canvas, {
		pb0: function(v, i) {if (v == 0 && lb === i) {autoAnim = !autoAnim; lb = null} if (v) {lb = i;}},
		ksp: function(v) {if (v == 0) {autoAnim = !autoAnim;}},
		mlc: function() {autoAnim = !autoAnim;},
		ply: function(y) {ypmod = y;},
		plx: function(x) {xpmod = x;},
		my: function(y) {ymod = y;},
		mx: function(x) {xmod = x;},
		tly: function(y) {ymod = y;},
		tlx: function(x) {xmod = x;},
		kup: function(v) {ymod = -v;},
		kdo: function(v) {ymod = v;},
		kri: function(v) {xmod = v;},
		kle: function(v) {xmod = -v;},
		preventdeftouch: function() {return !autoAnim;},
	}, true);

	requestAnimationFrame(drawScene);

	function drawScene(now) {
		now *= 0.001;
		const deltaTime = now - then;
		then = now;
		fpsNode.nodeValue = (1 / deltaTime).toFixed(2);

		glUtils.resizeCanvas(gl.canvas);

		controls.updatePads();

		if (autoAnim) {
			modelXRotationRadians += deltaTime * 0.5;
			modelYRotationRadians += deltaTime * 0.7;
		} else {
			modelXRotationRadians += deltaTime * 0.5 * (ypmod ? ypmod : ymod);
			modelYRotationRadians += deltaTime * 0.7 * (xpmod ? xpmod : xmod);
		}
		modelXRotationRadians %= 2 * Math.PI;
		modelYRotationRadians %= 2 * Math.PI;

		gl.initFrame();

		const aspect = gl.clientWidth / gl.clientHeight;
		const projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
		const viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		const matrix = m4.xRotation(modelXRotationRadians);
		m4.yRotate(matrix, modelYRotationRadians, matrix);
		const worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, matrix);

		const uniforms = {u_worldViewProjection: worldViewProjectionMatrix, u_world: matrix, u_worldInverseTranspose: m4.transpose(m4.inverse(matrix)), u_lightWorldPosition: [0, 0, cameraPosition[2] * 1.5], u_viewWorldPosition: cameraMatrix.slice(12, 15), u_color: [1, 0.8, 0.2, 1], u_shininess: 50};
		gl.drawVAO(vao, uniforms);

		requestAnimationFrame(drawScene);
	}
}

function start() {
	const canvas = document.getElementById("c");
	const gl = new glUtils(canvas.getContext("webgl2"));
	if (gl.gl) {
		const err = document.getElementById("err");
		while (err.firstChild) {err.removeChild(err.firstChild);}
		const fpsElement = document.getElementById("fps");
		const fpsNode = document.createTextNode("");
		fpsElement.appendChild(fpsNode);
		draw(fpsNode, gl, canvas);
	} else {
		let cc = document.getElementById("cc");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc = document.getElementById("overlay");
		while (cc.firstChild) {cc.removeChild(cc.firstChild);}
		cc.style.width = '0';
		cc.style.height = '0';
		cc.style.padding = '0';
		cc.style.border = '0';
	}
}

document.addEventListener('DOMContentLoaded', start);
