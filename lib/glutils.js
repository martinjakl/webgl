/*
 * Copyright 2018 Martin Jakl (martin.jakl@macsnet.cz).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
"use strict";

class glUtils {
	constructor(gl) {
		this.gl = gl;
		if (gl != undefined) {
			this.DrawType = {
				STATIC_DRAW: gl.STATIC_DRAW,
				DYNAMIC_DRAW: gl.DYNAMIC_DRAW,
			}
			this.TextureWrap = {
				REPEAT: gl.REPEAT,
				CLAMP_TO_EDGE: gl.CLAMP_TO_EDGE,
				MIRRORED_REPEAT: gl.MIRRORED_REPEAT,
			}
			this.ArrayType = {
				Float32Array: gl.FLOAT,
				Uint8Array: gl.UNSIGNED_BYTE,
				Uint32Array: gl.UNSIGNED_INT,
				Uint16Array: gl.UNSIGNED_SHORT,
				Int8Array: gl.BYTE,
			};
			this.MipMap = {
				NEAREST: gl.NEAREST,
				LINEAR: gl.LINEAR,
				NEAREST_MIPMAP_LINEAR: gl.NEAREST_MIPMAP_LINEAR,
				LINEAR_MIPMAP_NEAREST: gl.LINEAR_MIPMAP_NEAREST,
				NEAREST_MIPMAP_NEAREST: gl.NEAREST_MIPMAP_NEAREST,
				LINEAR_MIPMAP_LINEAR: gl.LINEAR_MIPMAP_LINEAR,
			};
			this.MipMapFilter = {
				TEXTURE_MIN_FILTER: gl.TEXTURE_MIN_FILTER,
				TEXTURE_MAG_FILTER: gl.TEXTURE_MAG_FILTER,
			};
			this.PrimType = {
				TRIANGLES: gl.TRIANGLES,
				POINTS: gl.POINTS,
				LINE_STRIP: gl.LINE_STRIP,
				LINE_LOOP: gl.LINE_LOOP,
				LINES: gl.LINES,
				TRIANGLE_STRIP: gl.TRIANGLE_STRIP,
				TRIANGLE_FAN: gl.TRIANGLE_FAN,
			};
			this.PixelFormat = {
				RGBA: gl.RGBA,
				RGB: gl.RGB,
				RG: gl.RG,
				LUMINANCE: gl.LUMINANCE,
				LUMINANCE_ALPHA: gl.LUMINANCE_ALPHA,
				ALPHA: gl.ALPHA,
				RED: gl.RED,
				R8: gl.R8,
			};
		}
	}

	createShader(type, source) {
		const shader = this.gl.createShader(type);
		this.gl.shaderSource(shader, source);
		this.gl.compileShader(shader);
		const success = this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS);
		if (success) {
			return shader;
		}
		console.log(this.gl.getShaderInfoLog(shader));
		this.gl.deleteShader(shader);
	}

	createProgram(vertexShader, fragmentShader) {
		const program = this.gl.createProgram();
		this.gl.attachShader(program, vertexShader);
		this.gl.attachShader(program, fragmentShader);
		this.gl.linkProgram(program);
		const success = this.gl.getProgramParameter(program, this.gl.LINK_STATUS);
		if (success) {
			return program;
		}
		console.log(this.gl.getProgramInfoLog(program));
		this.gl.deleteProgram(program);
	}

	createProgramShaders(vs, fs) {
		const vertexShader = this.createShader(this.gl.VERTEX_SHADER, vs);
		const fragmentShader = this.createShader(this.gl.FRAGMENT_SHADER, fs);
		const program = this.createProgram(vertexShader, fragmentShader);
		return new ProgramInfo(program, this.getAttribs(program), this.getUniforms(program));
	}

	getAttribs(program) {
		const result = {};
		const activeAttributes = this.gl.getProgramParameter(program, this.gl.ACTIVE_ATTRIBUTES);
		for (let i = 0; i < activeAttributes; ++i) {
			const att = this.gl.getActiveAttrib(program, i);
			result[att.name] = this.gl.getAttribLocation(program, att.name);
		}
		return result;
	}

	getUniforms(program) {
		const result = {};
		const activeUniforms = this.gl.getProgramParameter(program, this.gl.ACTIVE_UNIFORMS);
		for (let i = 0; i < activeUniforms; ++i) {
			const uni = this.gl.getActiveUniform(program, i);
			const item = {};
			item.loc = this.gl.getUniformLocation(program, uni.name);
			item.type = uni.type;
			result[uni.name] = item;
		}
		return result;
	}

	get canvas() {
		return this.gl.canvas;
	}

	useProgram(pi) {
		this.gl.useProgram(pi.program);
	}

	useVAO(vao) {
		this.gl.bindVertexArray(vao.vao);
	}

	createVAO(pi, attrmap, primitive, ind = undefined, drawType = this.DrawType.STATIC_DRAW, instanced = null, instancecountfrom) {
		this.gl.useProgram(pi.program);
		const vao = this.gl.createVertexArray();
		this.gl.bindVertexArray(vao);
		for (let i in primitive) {
			const p = primitive[i];
			const a = attrmap[i];
			if (a !== undefined) {
				const ai = pi.attribs[a];
				if (ai !== undefined) {
					const buf = this.gl.createBuffer();
					this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buf);
					this.gl.bufferData(this.gl.ARRAY_BUFFER, p.data, drawType);

					this.gl.enableVertexAttribArray(ai);
					this.gl.vertexAttribPointer(ai, p.size, this.ArrayType[p.data.constructor.name], p.normalize, p.stride, p.offset);
					if (instanced && instanced[i] != undefined) {
						this.gl.vertexAttribDivisor(ai, instanced[i]);
					}
				}
			}
		}
		let indices = null;
		let count = primitive['position'].count;
		let indtype = null;
		if (ind != undefined) {
			count = primitive[ind].count;
			indices = this.gl.createBuffer();
			indtype = this.ArrayType[primitive[ind].data.constructor.name];
			this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, indices);
			this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, primitive[ind].data, drawType);
		}
		return new VAOInfo(vao, 0, count, pi, indices, indtype, instanced != null ? primitive[instancecountfrom].count : 0);
	}

	initFrame(fb = null, color = [0, 0, 0, 0], w = this.gl.canvas.width, h = this.gl.canvas.height, cull = true) {
		this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, fb);
		this.gl.viewport(0, 0, w, h);
		if (cull) {
			this.gl.enable(this.gl.CULL_FACE);
		}
		this.gl.enable(this.gl.DEPTH_TEST);
		this.gl.clearColor(color[0], color[1], color[2], color[3]);
		this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
	}

	blendOn() {
		this.gl.enable(this.gl.BLEND);
		this.gl.blendFunc(this.gl.ONE, this.gl.ONE_MINUS_SRC_ALPHA);
		this.gl.depthMask(false);
	}

	blendOff() {
		this.gl.disable(this.gl.BLEND);
		this.gl.depthMask(true);
	}

	get clientWidth() {
		return this.gl.canvas.clientWidth;
	}

	get clientHeight() {
		return this.gl.canvas.clientHeight;
	}

	drawVAO(vao, uniforms, mode = this.PrimType.TRIANGLES) {
		this.useProgram(vao.pi);
		this.useVAO(vao);
		let texi = 0;
		for (let name in uniforms) {
			const ul = vao.pi.uniforms[name];
			const v = uniforms[name];
			if (ul !== undefined) {
				switch (ul.type) {
					case this.gl.FLOAT_MAT4:
						this.gl.uniformMatrix4fv(ul.loc, false, v);
						break;
					case this.gl.FLOAT_MAT3:
						this.gl.uniformMatrix3fv(ul.loc, false, v);
						break;
					case this.gl.FLOAT_MAT2:
						this.gl.uniformMatrix2fv(ul.loc, false, v);
						break;
					case this.gl.INT:
						this.gl.uniform1i(ul.loc, v);
						break;
					case this.gl.SAMPLER_2D:
						this.gl.activeTexture(this.gl.TEXTURE0 + texi);
						this.gl.bindTexture(this.gl.TEXTURE_2D, v);
						this.gl.uniform1i(ul.loc, texi++);
						break;
					case this.gl.FLOAT_VEC4:
						this.gl.uniform4fv(ul.loc, v);
						break;
					case this.gl.FLOAT:
						this.gl.uniform1f(ul.loc, v);
						break;
					case this.gl.SAMPLER_CUBE:
						this.gl.activeTexture(this.gl.TEXTURE0 + texi);
						this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, v);
						this.gl.uniform1i(ul.loc, texi++);
						break;
					case this.gl.FLOAT_VEC2:
						this.gl.uniform2fv(ul.loc, v);
						break;
					case this.gl.FLOAT_VEC3:
						this.gl.uniform3fv(ul.loc, v);
						break;
					case this.gl.INT_VEC2:
						this.gl.uniform2iv(ul.loc, v);
						break;
					case this.gl.INT_VEC3:
						this.gl.uniform3iv(ul.loc, v);
						break;
					case this.gl.INT_VEC4:
						this.gl.uniform4iv(ul.loc, v);
						break;
					case this.gl.UNSIGNED_INT:
						this.gl.uniform1ui(ul.loc, v);
						break;
					case this.gl.UNSIGNED_INT_VEC2:
						this.gl.uniform2uiv(ul.loc, v);
						break;
					case this.gl.UNSIGNED_INT_VEC3:
						this.gl.uniform3uiv(ul.loc, v);
						break;
					case this.gl.UNSIGNED_INT_VEC4:
						this.gl.uniform4uiv(ul.loc, v);
						break;
					case this.gl.FLOAT_MAT2x3:
						this.gl.uniformMatrix2x3fv(ul.loc, false, v);
						break;
					case this.gl.FLOAT_MAT2x4:
						this.gl.uniformMatrix2x4fv(ul.loc, false, v);
						break;
					case this.gl.FLOAT_MAT3x2:
						this.gl.uniformMatrix3x2fv(ul.loc, false, v);
						break;
					case this.gl.FLOAT_MAT3x4:
						this.gl.uniformMatrix3x4fv(ul.loc, false, v);
						break;
					case this.gl.FLOAT_MAT4x2:
						this.gl.uniformMatrix4x2fv(ul.loc, false, v);
						break;
					case this.gl.FLOAT_MAT4x3:
						this.gl.uniformMatrix4x3fv(ul.loc, false, v);
						break;
					case this.gl.BOOL:
						this.gl.uniform1i(ul.loc, v);
						break;
					case this.gl.BOOL_VEC2:
						this.gl.uniform2iv(ul.loc, v);
						break;
					case this.gl.BOOL_VEC3:
						this.gl.uniform3iv(ul.loc, v);
						break;
					case this.gl.BOOL_VEC4:
						this.gl.uniform4iv(ul.loc, v);
						break;
					case this.gl.BYTE:
					case this.gl.UNSIGNED_BYTE:
					case this.gl.SHORT:
					case this.gl.UNSIGNED_SHORT:
					case this.gl.SAMPLER_3D:
					case this.gl.SAMPLER_2D_SHADOW:
					case this.gl.SAMPLER_2D_ARRAY:
					case this.gl.SAMPLER_2D_ARRAY_SHADOW:
					case this.gl.SAMPLER_CUBE_SHADOW:
					case this.gl.INT_SAMPLER_2D:
					case this.gl.INT_SAMPLER_3D:
					case this.gl.INT_SAMPLER_CUBE:
					case this.gl.INT_SAMPLER_2D_ARRAY:
					case this.gl.UNSIGNED_INT_SAMPLER_2D:
					case this.gl.UNSIGNED_INT_SAMPLER_3D:
					case this.gl.UNSIGNED_INT_SAMPLER_CUBE:
					case this.gl.UNSIGNED_INT_SAMPLER_2D_ARRAY:
					default:
						throw "Unknown uniform type";
				}
			}
		}
		if (vao.instances != 0)  {
			if (vao.indices == null) {
				this.gl.drawArraysInstanced(mode, vao.first, vao.count, vao.instances);
			} else {
				this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, vao.indices);
				this.gl.drawElementsInstanced(mode, vao.count, vao.indtype, vao.first, vao.instances);
			}
		} else {
			if (vao.indices == null) {
				this.gl.drawArrays(mode, vao.first, vao.count);
			} else {
				this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, vao.indices);
				this.gl.drawElements(mode, vao.count, vao.indtype, vao.first);
			}
		}
	}

	createTextureFromCanvas(canvas) {
		const texture = this.gl.createTexture();
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
		this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
		this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
		this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, canvas);
		this.gl.generateMipmap(this.gl.TEXTURE_2D);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
		return texture;
	}

	createTextureFromData(width, height, format, data, internalFormat) {
		const texture = this.gl.createTexture();
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
		this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, false);
		this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
		const border = 0;
		this.gl.pixelStorei(this.gl.UNPACK_ALIGNMENT, 1);
		this.gl.texImage2D(this.gl.TEXTURE_2D, 0, internalFormat === undefined ? format : internalFormat, width, height, border, format, this.ArrayType[data.constructor.name], data);
		this.gl.generateMipmap(this.gl.TEXTURE_2D);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
		return texture;
	}

	createTextureMipLevelsFromCanvas(func, params, wrapS = this.TextureWrap.CLAMP_TO_EDGE, wrapT = this.TextureWrap.CLAMP_TO_EDGE) {
		const texture = this.gl.createTexture();
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
		this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, false);
		this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
		const gl = this.gl;
		params.forEach(function(s, level) {
			gl.texImage2D(gl.TEXTURE_2D, level, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, func(s));
		});
		if (wrapS != null) {
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, wrapS);
		}
		if (wrapT != null) {
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, wrapT);
		}
		return texture;
	}

	loadTexture(imgsrc, wrapS = this.TextureWrap.CLAMP_TO_EDGE, wrapT = this.TextureWrap.CLAMP_TO_EDGE, extendedInfo = false) {
		const texture = this.gl.createTexture();
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
		this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, 1, 1, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));
		const image = new Image();
		this.requestCORSIfNotSameOrigin(image, imgsrc);
		image.src = imgsrc;
		const gl = this.gl;
		const retInfo = {texture: texture, w: 1, h: 1};
		image.addEventListener('load', function() {
			retInfo.w = image.width;
			retInfo.h = image.height;
			gl.activeTexture(gl.TEXTURE0);
			gl.bindTexture(gl.TEXTURE_2D, texture);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
			gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
			gl.generateMipmap(gl.TEXTURE_2D);
			if (wrapS != null) {
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrapS);
			}
			if (wrapT != null) {
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrapT);
			}
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		});
		if (extendedInfo) {
			return retInfo;
		} else {
			return texture;
		}
	}


	loadSamplerCube(imgsrc, wrapS = this.TextureWrap.CLAMP_TO_EDGE, wrapT = this.TextureWrap.CLAMP_TO_EDGE) {
		if (imgsrc.length != 6) {
			throw "Not enough images for Cube Map.";
		}
		const cubesides = [this.gl.TEXTURE_CUBE_MAP_POSITIVE_X, this.gl.TEXTURE_CUBE_MAP_NEGATIVE_X, this.gl.TEXTURE_CUBE_MAP_POSITIVE_Y, this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, this.gl.TEXTURE_CUBE_MAP_POSITIVE_Z, this.gl.TEXTURE_CUBE_MAP_NEGATIVE_Z];
		const texture = this.gl.createTexture();
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, texture);
		const gl = this.gl;
		let count = imgsrc.length;
		function loadImage() {
			gl.activeTexture(gl.TEXTURE0);
			gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);
			gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
			gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
			gl.texImage2D(this.cubeside, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this);
			if (--count == 0) {
				gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
				if (wrapS != null) {
					gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_S, wrapS);
				}
				if (wrapT != null) {
					gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_WRAP_T, wrapT);
				}
				gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
				gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			}
		}
		for (let i = 0; i < imgsrc.length; ++i) {
			this.gl.texImage2D(cubesides[i], 0, this.gl.RGBA, 1, 1, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));
			const image = new Image();
			this.requestCORSIfNotSameOrigin(image, imgsrc[i]);
			image.src = imgsrc[i];
			image.cubeside = cubesides[i];
			image.addEventListener('load', loadImage);
		}
		return texture;
	}

	createFBTexture(width, height, mimMap, depth) {
		const framebuffer = this.gl.createFramebuffer();
		const fbTexture = this.gl.createTexture();
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, fbTexture);
		this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, false);
		this.gl.pixelStorei(this.gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
		if (width !== undefined && height !== undefined) {
			this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, width, height, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);
		}
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, mimMap === undefined ? this.gl.NEAREST : mimMap);
		this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, mimMap === undefined ? this.gl.NEAREST : mimMap);
		this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, framebuffer);
		this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, fbTexture, 0);
		let depthTexture = null;
		if (depth) {
			depthTexture = this.gl.createTexture();
			this.gl.bindTexture(this.gl.TEXTURE_2D, depthTexture);
			if (width !== undefined && height !== undefined) {
				this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.DEPTH_COMPONENT24, width, height, 0, this.gl.DEPTH_COMPONENT, this.gl.UNSIGNED_INT, null);
			}
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
			this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
			this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.DEPTH_ATTACHMENT, this.gl.TEXTURE_2D, depthTexture, 0);
		}
		return {fb: framebuffer, tex: fbTexture, dtex: depthTexture};
	}

	resizeFBTexture(tex, w, h) {
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, tex);
		this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, w, h, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);
	}

	changeTexMipMapFilter(texture, filter, value) {
		this.gl.activeTexture(this.gl.TEXTURE0);
		this.gl.bindTexture(this.gl.TEXTURE_2D, texture);
		this.gl.texParameteri(this.gl.TEXTURE_2D, filter, value);
	}

	static resizeCanvas(canvas, mulitplier = 1) {
		const displayWidth = canvas.clientWidth * mulitplier;
		const displayHeight = canvas.clientHeight * mulitplier;
		if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
			canvas.width = displayWidth;
			canvas.height = displayHeight;
			return true;
		}
		return false;
	}

	requestCORSIfNotSameOrigin(img, url) {
		const r = new RegExp('^(?:[a-z]+:)?//', 'i');
		if (r.test(url)) {
			if ((new URL(url)).origin !== window.location.origin) {
				img.crossOrigin = "";
			}
		}
	}

	 deleteTexture(texture) {
		 this.gl.deleteTexture(texture);
	 }
}

class ProgramInfo {
	constructor(program, attribs, uniforms) {
		this.program = program;
		this.attribs = attribs;
		this.uniforms = uniforms;
	}
}

class VAOInfo {
	constructor(vao, first, count, pi, indices, indtype, instances) {
		this.vao = vao;
		this.first = first;
		this.count = count;
		this.pi = pi;
		this.indices = indices;
		this.indtype = indtype;
		this.instances = instances;
	}
}

