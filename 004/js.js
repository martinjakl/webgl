//Texture effect
function createShader(gl, type, source) {
	const shader = gl.createShader(type);
	gl.shaderSource(shader, source);
	gl.compileShader(shader);
	const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
	if (success) {
		return shader;
	}
	console.log(gl.getShaderInfoLog(shader));
	gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
	const program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	const success = gl.getProgramParameter(program, gl.LINK_STATUS);
	if (success) {
		return program;
	}
	console.log(gl.getProgramInfoLog(program));
	gl.deleteProgram(program);
}

function resize(canvas) {
	const displayWidth = canvas.clientWidth;
	const displayHeight = canvas.clientHeight;
	if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
		canvas.width = displayWidth;
		canvas.height = displayHeight;
	}
}

function randomInt(range) {
	return Math.floor(Math.random() * range);
}

function setRectangle(gl, x, y, width, height) {
	const x1 = x;
	const x2 = x + width;
	const y1 = y;
	const y2 = y + height;
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
		x1, y1,
		x2, y1,
		x1, y2,
		x1, y2,
		x2, y1,
		x2, y2]), gl.STATIC_DRAW);
}

function computeKernelWeight(kernel) {
	const weight = kernel.reduce(function(prev, curr) {
		return prev + curr;
	});
	return weight <= 0 ? 1 : weight;
}

function render(image) {
	const canvas = document.getElementById("c");
	const gl = canvas.getContext("webgl2");
	if (gl) {
		const vertexShaderSource = `#version 300 es
		in vec2 a_position;
		in vec2 a_textCoord;
		uniform vec2 u_resolution;
		out vec2 v_textCoord;
		void main() {
			vec2 zeroToOne = a_position / u_resolution;
			vec2 zeroToTwo = zeroToOne * 2.0;
			vec2 clipSpace = zeroToTwo - 1.0;
			gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);
			v_textCoord = a_textCoord;
		}
		`;
		const fragmentShaderSource = `#version 300 es
		precision mediump float;
		uniform sampler2D u_image;
		uniform float u_kernel[9];
		uniform float u_kernelWeight;
		in vec2 v_textCoord;
		out vec4 outColor;
		void main() {
			vec2 onePixel = vec2(1) / vec2(textureSize(u_image, 0));
			vec4 colorSum = 
				texture(u_image, v_textCoord + onePixel * vec2(-1, -1)) * u_kernel[0] +
				texture(u_image, v_textCoord + onePixel * vec2(0, -1)) * u_kernel[1] +
				texture(u_image, v_textCoord + onePixel * vec2(1, -1)) * u_kernel[2] +
				texture(u_image, v_textCoord + onePixel * vec2(-1, 0)) * u_kernel[3] +
				texture(u_image, v_textCoord + onePixel * vec2(0, 0)) * u_kernel[4] +
				texture(u_image, v_textCoord + onePixel * vec2(1, 0)) * u_kernel[5] +
				texture(u_image, v_textCoord + onePixel * vec2(-1, 1)) * u_kernel[6] +
				texture(u_image, v_textCoord + onePixel * vec2(0, 1)) * u_kernel[7] +
				texture(u_image, v_textCoord + onePixel * vec2(1, 1)) * u_kernel[8];
			outColor = vec4((colorSum / u_kernelWeight).rgb, 1);
		}
		`;

		const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
		const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
		const program = createProgram(gl, vertexShader, fragmentShader);
		const positionAttributeLocation = gl.getAttribLocation(program, "a_position");
		const textCoordAttributeLocation = gl.getAttribLocation(program, "a_textCoord");
		const resolutionUniformLocation = gl.getUniformLocation(program, "u_resolution");
		const imageUniformLocation = gl.getUniformLocation(program, "u_image");
		const kernelUniformLocation = gl.getUniformLocation(program, "u_kernel[0]");
		const kernelWeightUniformLocation = gl.getUniformLocation(program, "u_kernelWeight");
		const positionBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

		const vao = gl.createVertexArray();
		gl.bindVertexArray(vao);
		gl.enableVertexAttribArray(positionAttributeLocation);
		let size = 2;
		let type = gl.FLOAT;
		let normalize = false;
		let stride = 0;
		let offset = 0;
		gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset);

		const textCoordBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, textCoordBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
			0.0, 0.0,
			1.0, 0.0,
			0.0, 1.0,
			0.0, 1.0,
			1.0, 0.0,
			1.0, 1.0,
		]), gl.STATIC_DRAW);
		gl.enableVertexAttribArray(textCoordAttributeLocation);
		size = 2;
		type = gl.FLOAT;
		normalize = false;
		stride = 0;
		offset = 0;
		gl.vertexAttribPointer(textCoordAttributeLocation, size, type, normalize, stride, offset);
		const texture = gl.createTexture();
		gl.activeTexture(gl.TEXTURE0 + 0);
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		const mipLevel = 0;
		const internalFormat = gl.RGBA;
		const srcFormat = gl.RGBA;
		const srcType = gl.UNSIGNED_BYTE;
		gl.texImage2D(gl.TEXTURE_2D, mipLevel, internalFormat, srcFormat, srcType, image);

		const edgeDetectKernel = [
			-1, -1, -1,
			-1, 8, -1,
			-1, -1, -1,
		];

		resize(gl.canvas);
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
		gl.clearColor(0, 0, 0, 0);
		gl.clear(gl.COLOR_BUFFER_BIT);
		gl.useProgram(program);
		gl.uniform2f(resolutionUniformLocation, gl.canvas.width, gl.canvas.height);
		gl.uniform1i(imageUniformLocation, 0);
		gl.uniform1fv(kernelUniformLocation, edgeDetectKernel);
		gl.uniform1f(kernelWeightUniformLocation, computeKernelWeight(edgeDetectKernel));
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

		setRectangle(gl, 0, 0, image.width, image.height);
		const primitiveType = gl.TRIANGLES;
		offset = 0;
		const count = 6;
		gl.drawArrays(primitiveType, offset, count);
	} else {
		console.log("No GL");
	}
}

function start() {
	const image = new Image();
	image.src = "../img/pic.jpg"
	image.onload = function() {
		render(image);
	}
}

document.addEventListener('DOMContentLoaded', start);
